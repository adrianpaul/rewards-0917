<%-- 
    Document   : verificationCode
    Created on : 05 31, 19, 2:11:55 PM
    Author     : Adrian Paul
--%>

<%@page import="java.net.URISyntaxException"%>
<%@page import="HTTPManagement.HTTPManagerDirectory"%>
<%@page import="Others.Constants"%>
<%@page import="java.net.URI"%>
<!DOCTYPE html>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
     <%
         
            if(session.getAttribute("isExisted") == null || session.getAttribute("isExisted").toString().equalsIgnoreCase("false") || session.getAttribute("isExisted").toString().equalsIgnoreCase("default") 
                 || session.getAttribute("companyName").toString().equalsIgnoreCase("")){
                response.sendRedirect("index.jsp");
            }
            
            
            
         
     %>
      
         <META HTTP-EQUIV="refresh" CONTENT="<%=session.getMaxInactiveInterval() %>; URL=/0917/sessionexpired.jsp"/>
      
        <title><%=Constants.appName%> - Serve</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="assets/globeIcon.ico" />
         
        <link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.3/css/bootstrap.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.1.1/css/mdb.min.css">
	<link rel="stylesheet"  href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="application/javascript"  href="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/material.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/ripples.min.js">
	<link rel="text/html"  href="https://codepen.io/triss90/pen/VWWMWN">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.2.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <!-- Global site tag (gtag.js) - Google Analytics --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-HN82HHLXQ9"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-HN82HHLXQ9'); </script>
        
<style>

input:focus {
  border-bottom: 1px solid royalblue !important;
  box-shadow: 0 1px 0 0 royalblue !important;
}    

.checkboxDiv{
    background-color:#f7f7f7;
    padding-left: 15px;
    padding-bottom: 15px; 
    padding-right: 15px; 
    padding-top: 23px;
}
.checkbox-changed{
    padding-top: 10px;
}

.checkbox-changed[type="checkbox"]:checked + label:before
{
    top: -4px;
  left: -3px;
  width: 12px;
  height: 22px;
  border-top: 2px solid transparent;
  border-left: 2px solid transparent;
  border-right: 2px solid white; /* You need to change the colour here */
  border-bottom: 2px solid white; /* And here */
  -webkit-transform: rotate(40deg);
  -moz-transform: rotate(40deg);
  -ms-transform: rotate(40deg);
  -o-transform: rotate(40deg);
  transform: rotate(40deg);
  -webkit-backface-visibility: hidden;
  -webkit-transform-origin: 100% 100%;
  -moz-transform-origin: 100% 100%;
  -ms-transform-origin: 100% 100%;
  -o-transform-origin: 100% 100%;
  transform-origin: 100% 100%;

}


.collapsible-header {
    position: relative;
}

.iconadd{
    display:inline-block ;
    position: absolute;
    right: 0;
}
.iconremove{
  display:none !important;
}

li.active .collapsible-header .material-icons.iconadd{
  display: none;
}

li.active .collapsible-header .material-icons.iconremove{
  display: inline-block !important;
   position: absolute;
    right: 0;
}
.no-shadows {
    box-shadow: none!important;
     border: none;
}

.iconadds{
    display:inline-block ;
    position: absolute;
    right: 0;
}

 </style>   

<script>
    function callSuccess(){
    if(!document.querySelector('input[name="improve"]:checked')){
        document.getElementById("errorImprove").style.display="block";
        document.getElementById("divImprove").scrollIntoView();
        return false;

    }else{
        document.getElementById("errorImprove").style.display="none";
       
    }
        
    }

    function enabledButton(){
        if($('#improve1').is(':checked')){
            document.getElementById("p1").style.backgroundColor="#00c5e8";
            document.getElementById("span1").style.color="white";
        }else{
            document.getElementById("p1").style.backgroundColor="#f7f7f7";
            document.getElementById("span1").style.color="black";
            document.getElementById("improve1").style.color="black";
        }
        
        if($('#improve2').is(':checked')){
            document.getElementById("p2").style.backgroundColor="#00c5e8";
            document.getElementById("span2").style.color="white";
            document.getElementById("improve2").style.color="white";
        }else{
            document.getElementById("p2").style.backgroundColor="#f7f7f7";
            document.getElementById("span2").style.color="black";
            document.getElementById("improve2").style.color="black";
        }
        if($('#improve3').is(':checked')){
            document.getElementById("p3").style.backgroundColor="#00c5e8";
            document.getElementById("span3").style.color="white";
            document.getElementById("improve3").style.color="white";
        }else{
            document.getElementById("p3").style.backgroundColor="#f7f7f7";
            document.getElementById("span3").style.color="black";
            document.getElementById("improve3").style.color="black";
        }
        
        if($('#improve4').is(':checked')){
            document.getElementById("p4").style.backgroundColor="#00c5e8";
            document.getElementById("span4").style.color="white";
            document.getElementById("improve4").style.color="white";
        }else{
            document.getElementById("p4").style.backgroundColor="#f7f7f7";
            document.getElementById("span4").style.color="black";
            document.getElementById("improve4").style.color="black";
        }
   
        if($('#improve5').is(':checked')){
            document.getElementById("p5").style.backgroundColor="#00c5e8";
            document.getElementById("span5").style.color="white";
            document.getElementById("improve5").style.color="white";
        }else{
            document.getElementById("p5").style.backgroundColor="#f7f7f7";
            document.getElementById("span5").style.color="black";
            document.getElementById("improve5").style.color="black";
        }
   
        if($('#improve6').is(':checked')){
            document.getElementById("p6").style.backgroundColor="#00c5e8";
            document.getElementById("span6").style.color="white";
            document.getElementById("improve6").style.color="white";
            
        }else{
            document.getElementById("p6").style.backgroundColor="#f7f7f7";
            document.getElementById("span6").style.color="black";
            document.getElementById("improve6").style.color="black";
            
        }
   
        if($('#improve7').is(':checked')){
            document.getElementById("p7").style.backgroundColor="#00c5e8";
            document.getElementById("span7").style.color="white";
            document.getElementById("improve7").style.color="white";
        }else{
            document.getElementById("p7").style.backgroundColor="#f7f7f7";
            document.getElementById("span7").style.color="black";
            document.getElementById("improve7").style.color="black";
        }
   
        if($('#improve8').is(':checked')){
            document.getElementById("p8").style.backgroundColor="#00c5e8";
            document.getElementById("span8").style.color="white";
            document.getElementById("improve8").style.color="white";
        }else{
            document.getElementById("p8").style.backgroundColor="#f7f7f7";
            document.getElementById("span8").style.color="black";
            document.getElementById("improve8").style.color="black";
        }
        
    if(document.querySelector('input[name="improve"]:checked')){
        document.getElementById("btnSubmit").disabled = false;
       
    }else{
        document.getElementById("btnSubmit").disabled = true;
       
    }
        
        
        
    
    }


</script>
</head>



<body onLoad="enabledButton()">


        
        <div class="row" >
            <div class="col s12 col m12" style="text-align: center;padding-top: 20px;">
                <img class="responsive-img"  src="assets/illustrations/1x/helpmdpi1.png"/> 
                <h4 class="h4-responsive" style="color:#678c96"> How else may we serve you?</h4>

            </div>
 
        </div>
        
        <div class="row" >
            <div class="col m12 col s12">
                <form action="Serve" method="POST"> 
                    <div class="col s12 offset-m3  col m6">   
                         
                        <label style="font-size:18px;color:#4d4d4d;">Which part of the business would you like to improve the most?<br>
                            <p style="color:#000080;">(Choose a maximum of 3 choices)</p></label>

                        <br><br><p style="background-color:#f7f7f7;padding: 15px;" id="p1" class="checkboxDiv">
                            <input  class="checkbox-changed" type="checkbox" name="improve" id="improve1" value="Ensuring our business is safe from online threats" />
                            <label style="color:black;" id="span1" for="improve1">Ensuring our business is safe from online threats</label>
                        </p>
                        <p  style="background-color:#f7f7f7; padding: 15px;" id="p2" class="checkboxDiv" >
                            <input class="checkbox-changed" type="checkbox" name="improve"  id="improve2" value="Generating new Customer" />
                            <label style="color:black;" id="span2"   for="improve2">Generating new Customer</label>
                        </p>
                        <p  style="background-color:#f7f7f7; padding: 15px;" id="p3" class="checkboxDiv">
                            <input class="checkbox-changed" type="checkbox" name="improve"  id="improve3" value="Keeping my customer happy and loyal" />
                            <label style="color:black;" id="span3"  for="improve3">Keeping my customer happy and loyal</label>
                        </p>

                        <p  style="background-color:#f7f7f7; padding: 15px;" id="p4" class="checkboxDiv">
                            <input class="checkbox-changed" type="checkbox" name="improve"  id="improve4" value="Limiting skills and productivity of my employees" />
                            <label style="color:black;" id="span4"  for="improve4">Limiting skills and productivity of my employees</label>
                        </p>

                        <p style="background-color:#f7f7f7; padding: 15px;" id="p5" class="checkboxDiv">
                            <input class="checkbox-changed" type="checkbox" name="improve"  id="improve5" value="Making sure our files are backed-up when disaster strikes" />
                            <label style="color:black;" id="span5"  for="improve5">Making sure our files are backed-up when disaster strikes</label>
                        </p>


                        <p  style="background-color:#f7f7f7; padding: 15px;" id="p6" class="checkboxDiv">
                            <input class="checkbox-changed" type="checkbox" name="improve"  id="improve6" value="Managing our business finances" />
                            <label style="color:black;" id="span6"  for="improve6">Managing our business finances</label>
                        </p>

                        <p style="background-color:#f7f7f7; padding: 15px;" id="p7" class="checkboxDiv">
                            <input class="checkbox-changed" type="checkbox" name="improve"  id="improve7" value="Managing our telco subscriptions (mobile, internet, and WiFi) to meet our growing needs" />
                            <label style="color:black;" id="span7"  for="improve7">Managing our telco subscriptions (mobile, internet, and WiFi) to meet our growing needs</label>
                        </p>

                        <p style="background-color:#f7f7f7; padding: 15px;" id="p8" class="checkboxDiv">
                            <input class="checkbox-changed" type="checkbox" name="improve"  id="improve8" value="Maximizing our online presence to engage (e.g social media, advertisements, etc" />
                            <label style="color:black;" id="span8"  for="improve8">Maximizing our online presence to engage (e.g social media, advertisements, etc)</label>
                        </p>
                        <span id="errorImprove" style="color:red;display:none;font-size:15px;">Select something</span><br>
                        
                        <input  type="hidden" class="form-control" name="improves" id="improves"/>
                     </div>     


                        <div class="col s12 offset-m3 m6 offset-l5 l2">
                       
                            <button id="btnSubmit" class="waves-light btn"  onClick="return callSuccess();"   style="width: 100%;color:white;background-color: #3385ff;" disabled="">CONTINUE</button> 
                        </div>
                </form>

               
            </div>

        </div>   
  
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=cea87e7d-969e-47a5-b787-61b8ff7dadfa"> </script>
    <!-- End of globemybusiness Zendesk Widget script -->   
</body>
<script>
 
   
    $('input[name=improve]').change(function(){
        if($('#improve1').is(':checked')){
            document.getElementById("p1").style.backgroundColor="#00c5e8";
            document.getElementById("span1").style.color="white";
          
            
            
        }else{
            document.getElementById("p1").style.backgroundColor="#f7f7f7";
            document.getElementById("span1").style.color="black";
            document.getElementById("improve1").style.color="black";
            
        }
        
        if($('#improve2').is(':checked')){
            document.getElementById("p2").style.backgroundColor="#00c5e8";
            document.getElementById("span2").style.color="white";
            document.getElementById("improve2").style.color="white";
            
        }else{
            document.getElementById("p2").style.backgroundColor="#f7f7f7";
            document.getElementById("span2").style.color="black";
            document.getElementById("improve2").style.color="black";
            
        }
        if($('#improve3').is(':checked')){
            document.getElementById("p3").style.backgroundColor="#00c5e8";
            document.getElementById("span3").style.color="white";
            document.getElementById("improve3").style.color="white";
            
        }else{
            document.getElementById("p3").style.backgroundColor="#f7f7f7";
            document.getElementById("span3").style.color="black";
            document.getElementById("improve3").style.color="black";
            
        }
        
        if($('#improve4').is(':checked')){
            document.getElementById("p4").style.backgroundColor="#00c5e8";
            document.getElementById("span4").style.color="white";
            document.getElementById("improve4").style.color="white";
            
        }else{
            document.getElementById("p4").style.backgroundColor="#f7f7f7";
            document.getElementById("span4").style.color="black";
            document.getElementById("improve4").style.color="black";
            
        }
   
        if($('#improve5').is(':checked')){
            document.getElementById("p5").style.backgroundColor="#00c5e8";
            document.getElementById("span5").style.color="white";
            document.getElementById("improve5").style.color="white";
            
        }else{
            document.getElementById("p5").style.backgroundColor="#f7f7f7";
            document.getElementById("span5").style.color="black";
            document.getElementById("improve5").style.color="black";
            
        }
   
        if($('#improve6').is(':checked')){
            document.getElementById("p6").style.backgroundColor="#00c5e8";
            document.getElementById("span6").style.color="white";
            document.getElementById("improve6").style.color="white";
            
        }else{
            document.getElementById("p6").style.backgroundColor="#f7f7f7";
            document.getElementById("span6").style.color="black";
            document.getElementById("improve6").style.color="black";
            
        }
   
        if($('#improve7').is(':checked')){
            document.getElementById("p7").style.backgroundColor="#00c5e8";
            document.getElementById("span7").style.color="white";
            document.getElementById("improve7").style.color="white";
            
        }else{
            document.getElementById("p7").style.backgroundColor="#f7f7f7";
            document.getElementById("span7").style.color="black";
            document.getElementById("improve7").style.color="black";
            
        }
   
        if($('#improve8').is(':checked')){
            document.getElementById("p8").style.backgroundColor="#00c5e8";
            document.getElementById("span8").style.color="white";
            document.getElementById("improve8").style.color="white";
            
        }else{
            document.getElementById("p8").style.backgroundColor="#f7f7f7";
            document.getElementById("span8").style.color="black";
            document.getElementById("improve8").style.color="black";
            
        }
   
    });
    $('input[name=improve]').change(function(){
        var improveArray = [];
        var improveArrayCheckboxes = document.querySelectorAll('input[name=improve]:checked')

        for (var i = 0; i < improveArrayCheckboxes.length; i++) {
            improveArray.push(improveArrayCheckboxes[i].value)
        }
        if ( $('input[name="improve"]:checked').length>3 || $('input[name="improve"]:checked').length === 0) {

            document.getElementById("errorImprove").style.display="block";
            document.getElementById("errorImprove").innerHTML ="Minimum    of 1 item and Maximum of 3 items only";
            $('#btnSubmit').prop('disabled', true);
           
        }else{
            document.getElementById("errorImprove").style.display="none";
            $('#btnSubmit').removeAttr('disabled');
            document.getElementById("improves").value = improveArray;
            
        }
    });

</script>    
</html>
