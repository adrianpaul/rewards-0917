<%-- 
    Document   : accountNumber
    Created on : 05 31, 19, 2:11:55 PM
    Author     : Adrian Paul
--%>

<%@page import="Others.Constants"%>
<!DOCTYPE html>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
    <%
        session.setAttribute("isCheckedPrivacy", "");
        session.setAttribute("isCheckedTerms", "");
        session.setAttribute("mobile", "");
        session.setAttribute("financialAccountIdFk", "0");
        session.setAttribute("type", "");
        session.setAttribute("errorType", "");
        session.setAttribute("codeMatches", "");
        session.setAttribute("emailAddress", "");
        session.setAttribute("mobileNumber", "");
        session.setAttribute("isVerified", "");  
        session.setAttribute("isExisted", "");
        session.setAttribute("successAdd", "");
        session.setAttribute("firstName", "");
        session.setAttribute("formType", "");
        session.setAttribute("improves", "");
        session.setAttribute("inCharge", "");
        session.setAttribute("companyName", "");
        session.setAttribute("sex", "");
        session.setAttribute("branch", "");
        session.setAttribute("kycCampaignIdFk", "1");
        

        
        
    %>
        <title><%=Constants.appName%> - Home</title>
        <META HTTP-EQUIV="refresh" CONTENT="<%=session.getMaxInactiveInterval() %>; URL=/0917/sessionexpired.jsp"/>
      
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="assets/globeIcon.ico" />
        <link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.3/css/bootstrap.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.1.1/css/mdb.min.css">
	<link rel="stylesheet"  href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="application/javascript"  href="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/material.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/ripples.min.js">
	<link rel="text/html"  href="https://codepen.io/triss90/pen/VWWMWN">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.2.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <!-- Global site tag (gtag.js) - Google Analytics --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-HN82HHLXQ9"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-HN82HHLXQ9'); </script>

<style>
    
.checkbox-changed[type="checkbox"]:checked + label:after
{
    border: 2px solid #00c5e8;  
    background-color: #00c5e8;
}
</style>
<script>
    function enabledButton(){      
        if(document.querySelector('input[name="privacy"]:checked')){
            $('#btnSubmit').removeAttr('disabled');
        
        }else{
            document.getElementById("btnSubmit").disabled= true;
          }

  
       
    }
    
</script>   
<script type="text/javascript">
piAId = '577043';
piCId = '63415';
piHostname = 'pi.pardot.com';

(function() {
	function async_load(){
		var s = document.createElement('script'); s.type = 'text/javascript';
		s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
		var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
	}
	if(window.attachEvent) { window.attachEvent('onload', async_load); }
	else { window.addEventListener('load', async_load, false); }
})();
</script>
</head>
<body onLoad="enabledButton()">
        <div class="row">
        
            <div class="col s12 col m12" style="text-align: center;">
                <div class="row"></div>
                <img class="responsive-img" src="assets/front-banner.png"/>
                  <h4 style="color:#63acd9;text-align: center;"> Globe myBusiness has a treat for you!</h4>
<!--               
<!--                <h4 style="color:#63acd9;text-align: center;"> Globe myBusiness has a treat for you!</h4>-->
                    
            </div>
        </div>    
            
        <div class="row">
            <div class="col s12  m12">
                <form action="Benefits" method="POST">
                   <div class="col s12 offset-m2 col m8">
                        <p style="color: black;font-size: 20px;text-align: center;">
                           We want to make sure we have the right information about you to ensure you receive your treat.
                           Approximate completion time: 5 minutes.

                        </p>
                        <p style="color: black;font-size: 20px;text-align: center;">
                            <input  class="filled-in checkbox-changed"  type="checkbox"  name="privacy" id="privacy"/><label style="color: black" for="privacy">I agree with Globe's <a href="https://www.globe.com.ph/business/sme/terms-conditions.html" target="_blank"> Terms and Conditions </a> and <a href="http://www.globe.com.ph/privacy-policy" target="_blank"> Data Privacy Policy </a></label>
                            <span id="errorPrivacy" style="color:red;display:none;margin-left:12px;font-size:15px;">Please indicate that you read and agree the Privacy Policy.</span>
                        </p>
                   </div>
                    <div class="col s12 offset-m3 m6 offset-l5 l2">
                        <br><button id="btnSubmit" class="waves-light btn" style="width: 100%;color:white;background-color: #009fdf;" >CONTINUE</button> 
                    </div>   
                </form>    


            </div>    

        </div>  
    <!-- Start of globemybusiness Zendesk Widget script -->
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=cea87e7d-969e-47a5-b787-61b8ff7dadfa"> </script>
    <!-- End of globemybusiness Zendesk Widget script -->
</body>


                            
<script>
    
    $('#privacy').on('change', function () {
        if($('#privacy').prop('checked')){
            $('#btnSubmit').removeAttr('disabled');
        
        } else { 
            $('#btnSubmit').prop('disabled', true);
        }
    });
</script>    
 
</html>
