<%-- 
    Document   : registrationForm
    Created on : 04 12, 19, 10:20:25 AM
    Author     : Adrian Paul
--%>

<%@page import="java.net.URISyntaxException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Parser.IndustryParser"%>
<%@page import="Bean.Industry"%>
<%@page import="HTTPManagement.HTTPManagerDirectory"%>
<%@page import="Others.Constants"%>
<%@page import="java.net.URI"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <%
            String folderName = Constants.folderName;
           String improves = (String) session.getAttribute("improves");
           System.out.println("isExisted: "+ session.getAttribute("isExisted").toString());
           
          if(session.getAttribute("isExisted") == null  || session.getAttribute("isExisted").toString().equalsIgnoreCase("false") || session.getAttribute("improves").toString().equalsIgnoreCase("")){
                response.sendRedirect("index.jsp");
            }else{
            String firstName = (String) session.getAttribute("firstName");
            String lastName = (String) session.getAttribute("lastName");
            String accountContactIdFk = (String) session.getAttribute("accountContactIdFk");
            String billingContactEmail = (String) session.getAttribute("billingContactEmail");
            String mobileNum = (String) session.getAttribute("mobileNum");
            String sex = (String) session.getAttribute("sex");
            String positionInTheCompany = (String) session.getAttribute("positionInTheCompany").toString().replaceFirst(" ","");
            String financialAccountIdFk = (String) session.getAttribute("financialAccountIdFk");
            String birthday = (String) session.getAttribute("birthday");
            String accountContactRoleIdFk = (String) session.getAttribute("accountContactRoleIdFk");
            String department = (String) session.getAttribute("department");
            String inCharge = (String) session.getAttribute("inCharge");
            String gppChecked = (String) session.getAttribute("isCheckedPrivacy");
            String interest = (String) session.getAttribute("interest");
            String accountId = (String) session.getAttribute("accountId");
       
          
            
//            System.out.println("accountId:  " + accountId);
//            System.out.println("accountContactIdFk: "+accountContactIdFk);
//            System.out.println("accountContactRoleIdFk: " +accountContactRoleIdFk);
//            System.out.println("financialAccountIdFk:  " + financialAccountIdFk);
//            System.out.println("firstName:  " + firstName);
//            System.out.println("lastName:  " + lastName);
//            System.out.println("billingContactEmail:  " + billingContactEmail);
//            System.out.println("mobileNum:  " + mobileNum);
//            System.out.println("sex:  " + sex);
//            System.out.println("positionInTheCompany:  " + positionInTheCompany);
//            System.out.println("department:  " + department);
//            System.out.println("birthdate:  " + birthday);
//            System.out.println("interest: "+interest);
            System.out.println("gppChecked: "+gppChecked);
            System.out.println("inCharge: "+inCharge);
          
            }
        %>
        <title><%=Constants.appName%> - Claim Rewards</title>
	<link rel="icon" type="image/png" href="../assets/globeIcon.ico" />
        <META HTTP-EQUIV="refresh" CONTENT="<%=session.getMaxInactiveInterval() %>; URL=sessionexpired.jsp"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
       
         
        <link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.3/css/bootstrap.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.1.1/css/mdb.min.css">
	<link rel="stylesheet"  href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="application/javascript"  href="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/material.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/ripples.min.js">
	<link rel="text/html"  href="https://codepen.io/triss90/pen/VWWMWN">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.2.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <script type="text/javascript" src="assets/javascript/rewards.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-HN82HHLXQ9"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-HN82HHLXQ9'); </script>
 <script>
        if (location.protocol != 'https:') { 
            location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
        }
    </script>          
        
        

 
</head>


<script>
    function callSuccess(){
    if(document.getElementById("reward").value === ""){
        document.getElementById("errorReward").style.display="block";
        document.getElementById("divButton").scrollIntoView();
        return false;

    }else{
        document.getElementById("errorReward").style.display="none";
       
    }
        
    }

</script>

<body>
    
   
    <div class="row" >
        <div class="col s12 col m12" style="text-align: center;padding-top: 20px;">
            <div class="col s12 offset-m2 col m8" style="text-align: center;">
                <img class="responsive-img"  src="../assets/illustrations/1x/almostmdpi1.png"/>  
                <h4 style="color:#63acd9;"> Almost Done!</h4>
                <p style="color: black;font-size: 20px;">
                    Because you've been so awesome throughout
                    the whole time, we want to give you a prize!
                    Choose from any one of the options below 
                    and we'll email you ASAP for instructions
                    on how to get your rewards
                </p>

                 <label style="color: black;font-size: 20px;">Choose one of the following rewards </label>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col m12 col s12">
            <div class="col s12  offset-m2 col m8" >
                <form action="../AddTempMobile2" method="POST">

                    <div class="row">
                        <div class="col s6 col m4" id="divRewards1">
                            <div class="card"  id="divReward1" >
                                <div class="card-image">
                                    <img  src="../assets/ayala.jpg"  class="responsive-img">
                                </div>
                                <div class="card-content right-align">
                                    <p style="text-align: left;">Get a P200 voucher discount.</p>

                                </div>

                            </div>
                        </div>

                        <div class="col s6 col m4"  id="divRewards2">
                            <div class="card" id="divReward2">
                                <div class="card-image">
                                    <img  src="../assets/krispykreme.jpg"  class="responsive-img">
                                </div>
                                <div class="card-content right-align">
                                    <p style="text-align: left;">Get a P200 voucher discount.</p>

                                </div>

                            </div>
                        </div>

                        <div class="col s6 col m4" id="divRewards3">
                            <div class="card" id="divReward3"  >
                                <div class="card-image">
                                    <img src="../assets/puregold.png" class="responsive-img">
                                </div>
                                <div class="card-content right-align">
                                    <p style="text-align: left;">Get a P200 voucher discount.</p>

                                </div>

                            </div>
                        </div>

                        <div class="col s6 col m4" id="divRewards4">
                            <div class="card" id="divReward4"  >
                                <div class="card-image">
                                    <img  src="../assets/coffeebeanandtealeaf.png"  class="responsive-img">
                                </div>
                                <div class="card-content right-align">
                                    <p style="text-align: left;">Get a P200 voucher discount.</p>
                                </div>

                            </div>
                        </div>


                    </div> 
                    <div class="row">

                        <div class="col s6 col m4" id="divRewards5">
                            <div class="card" id="divReward5" >
                                <div class="card-image">
                                    <img  src="../assets/nationaBookStore.jpg"  class="responsive-img">
                                </div>
                                <div class="card-content right-align">
                                    <p style="text-align: left;">Get a P200 voucher discount.</p>
                                </div>

                            </div>
                        </div>

                        <div class="col s6 col m4"  id="divRewards6">
                            <div class="card" id="divReward6" >
                                <div class="card-image">
                                    <img  src="../assets/sm.png"  class="responsive-img">
                                </div>
                                <div class="card-content right-align">
                                    <p style="text-align: left;">Get a P200 voucher discount.</p>
                                </div>

                            </div>
                        </div>

                        <div class="col s6 col m4"  id="divRewards7">
                            <div class="card" id="divReward7">
                                <div class="card-image">
                                    <img src="../assets/rob.jpg" class="responsive-img">
                                </div>
                                <div class="card-content right-align">
                                    <p style="text-align: left;">Get a P200 voucher discount.</p>
                                </div>

                            </div>
                        </div>
                        <div class="col s6 col m4" >
                        </div>

                    </div>    


                    <span id="errorReward" style="color:red;display:none;font-size:15px;text-align: center;">Select a reward</span><br>
                    <input type="hidden" id="reward" name="reward" class="form-control" style="text-transform:capitalize;">


                    <div class="col s12 offset-m2 col m4" id="divButton">

                        <a href="serve.jsp" class="waves-light btn  blue-text text-darken-4" style="width: 100%;background-color: white; border: 2px solid #3385ff;">GO BACK</a> 
                    </div>  
                    <div class="col s12  col m4">
                        <button onClick="return callSuccess();" id="divRewardSubmit" class="waves-light btn" style="width: 100%;color:white;background-color: #009fdf;" >CONTINUE</button> 
                    </div>


                </form>   
            </div> 




        </div>    

    </div>     
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=cea87e7d-969e-47a5-b787-61b8ff7dadfa"> </script>
    <!-- End of globemybusiness Zendesk Widget script -->   
</body>
<script>
    $('#divRewards1').click(function(){
        document.getElementById("divReward1").style.fontWeight ="bold";
        document.getElementById("divReward2").style.fontWeight ="normal";
        document.getElementById("divReward3").style.fontWeight ="normal";
        document.getElementById("divReward4").style.fontWeight ="normal";
        document.getElementById("divReward5").style.fontWeight ="normal";
        document.getElementById("divReward6").style.fontWeight ="normal";
        document.getElementById("divReward7").style.fontWeight ="normal";
         
        document.getElementById("divReward1").style.backgroundColor ="#63acd9";
        document.getElementById("divReward1").style.color ="white";
        document.getElementById("divReward2").style.backgroundColor ="white";
        document.getElementById("divReward2").style.color ="black";
        document.getElementById("divReward3").style.backgroundColor ="white";
        document.getElementById("divReward3").style.color ="black";
        document.getElementById("divReward4").style.backgroundColor ="white";
        document.getElementById("divReward4").style.color ="black";
        document.getElementById("divReward5").style.backgroundColor ="white";
        document.getElementById("divReward5").style.color ="black";
        document.getElementById("divReward6").style.backgroundColor ="white";
        document.getElementById("divReward6").style.color ="black";
        document.getElementById("divReward7").style.backgroundColor ="white";
        document.getElementById("divReward7").style.color ="black";
        document.getElementById("reward").value ="8875";
        document.getElementById("errorReward").style.display ="none";
    });
    
    
    $('#divRewards2').click(function(){
        document.getElementById("divReward1").style.fontWeight ="normal";
        document.getElementById("divReward2").style.fontWeight ="bold";
        document.getElementById("divReward3").style.fontWeight ="normal";
        document.getElementById("divReward4").style.fontWeight ="normal";
        document.getElementById("divReward5").style.fontWeight ="normal";
        document.getElementById("divReward6").style.fontWeight ="normal";
        document.getElementById("divReward7").style.fontWeight ="normal";
        
        document.getElementById("divReward1").style.backgroundColor ="white";
        document.getElementById("divReward1").style.color ="black";
        document.getElementById("divReward2").style.backgroundColor ="#63acd9";
        document.getElementById("divReward2").style.color ="white";
        document.getElementById("divReward3").style.backgroundColor ="white";
        document.getElementById("divReward3").style.color ="black";
        document.getElementById("divReward4").style.backgroundColor ="white";
        document.getElementById("divReward4").style.color ="black";
        document.getElementById("divReward5").style.backgroundColor ="white";
        document.getElementById("divReward5").style.color ="black";
        document.getElementById("divReward6").style.backgroundColor ="white";
        document.getElementById("divReward6").style.color ="black";
        document.getElementById("divReward7").style.backgroundColor ="white";
        document.getElementById("divReward7").style.color ="black";
        document.getElementById("reward").value ="7843";
        document.getElementById("errorReward").style.display ="none";
    });
    
    $('#divRewards3').click(function(){
        document.getElementById("divReward1").style.fontWeight ="normal";
        document.getElementById("divReward2").style.fontWeight ="normal";
        document.getElementById("divReward3").style.fontWeight ="bold";
        document.getElementById("divReward4").style.fontWeight ="normal";
        document.getElementById("divReward5").style.fontWeight ="normal";
        document.getElementById("divReward6").style.fontWeight ="normal";
        document.getElementById("divReward7").style.fontWeight ="normal";
        
        document.getElementById("divReward1").style.backgroundColor ="white";
        document.getElementById("divReward1").style.color ="black";
        document.getElementById("divReward2").style.backgroundColor ="white";
        document.getElementById("divReward2").style.color ="black";
        document.getElementById("divReward3").style.backgroundColor ="#63acd9";
        document.getElementById("divReward3").style.color ="white";
        document.getElementById("divReward4").style.backgroundColor ="white";
        document.getElementById("divReward4").style.color ="black";
        document.getElementById("divReward5").style.backgroundColor ="white";
        document.getElementById("divReward5").style.color ="black";
        document.getElementById("divReward6").style.backgroundColor ="white";
        document.getElementById("divReward6").style.color ="black";
        document.getElementById("divReward7").style.backgroundColor ="white";
        document.getElementById("divReward7").style.color ="black";
        document.getElementById("reward").value ="8876";
        document.getElementById("errorReward").style.display ="none";
      
    });
    
    
    $('#divRewards4').click(function(){
        document.getElementById("divReward1").style.fontWeight ="normal";
        document.getElementById("divReward2").style.fontWeight ="normal";
        document.getElementById("divReward3").style.fontWeight ="normal";
        document.getElementById("divReward4").style.fontWeight ="bold";
        document.getElementById("divReward5").style.fontWeight ="normal";
        document.getElementById("divReward6").style.fontWeight ="normal";
        document.getElementById("divReward7").style.fontWeight ="normal";
        
        
       document.getElementById("divReward1").style.backgroundColor ="white";
       document.getElementById("divReward1").style.color ="black";
       document.getElementById("divReward2").style.backgroundColor ="white";
       document.getElementById("divReward2").style.color ="black";
       document.getElementById("divReward3").style.backgroundColor ="white";
       document.getElementById("divReward3").style.color ="black";
       document.getElementById("divReward4").style.backgroundColor ="#63acd9";
       document.getElementById("divReward4").style.color ="white";
       document.getElementById("divReward5").style.backgroundColor ="white";
       document.getElementById("divReward5").style.color ="black";
       document.getElementById("divReward6").style.backgroundColor ="white";
       document.getElementById("divReward6").style.color ="black";
       document.getElementById("divReward7").style.backgroundColor ="white";
       document.getElementById("divReward7").style.color ="black";
       document.getElementById("reward").value ="8877";
       document.getElementById("errorReward").style.display ="none";
      
    });
    
     $('#divRewards5').click(function(){
         
        document.getElementById("divReward1").style.fontWeight ="normal";
        document.getElementById("divReward2").style.fontWeight ="normal";
        document.getElementById("divReward3").style.fontWeight ="normal";
        document.getElementById("divReward4").style.fontWeight ="normal";
        document.getElementById("divReward5").style.fontWeight ="bold";
        document.getElementById("divReward6").style.fontWeight ="normal";
        document.getElementById("divReward7").style.fontWeight ="normal";

       document.getElementById("divReward1").style.backgroundColor ="white";
       document.getElementById("divReward1").style.color ="black";
       document.getElementById("divReward2").style.backgroundColor ="white";
       document.getElementById("divReward2").style.color ="black";
       document.getElementById("divReward3").style.backgroundColor ="white";
       document.getElementById("divReward3").style.color ="black";
       document.getElementById("divReward4").style.backgroundColor ="white";
       document.getElementById("divReward4").style.color ="black";
       document.getElementById("divReward4").style.color ="blue";
       document.getElementById("divReward5").style.backgroundColor ="#63acd9";
       document.getElementById("divReward5").style.color ="white";
       document.getElementById("divReward6").style.backgroundColor ="white";
       document.getElementById("divReward6").style.color ="black";
       document.getElementById("divReward7").style.backgroundColor ="white";
       document.getElementById("divReward7").style.color ="black";
       
      document.getElementById("reward").value ="8878";
      document.getElementById("errorReward").style.display ="none";
      
    });
    
    $('#divRewards6').click(function(){
        document.getElementById("divReward1").style.fontWeight ="normal";
        document.getElementById("divReward2").style.fontWeight ="normal";
        document.getElementById("divReward3").style.fontWeight ="normal";
        document.getElementById("divReward4").style.fontWeight ="normal";
        document.getElementById("divReward5").style.fontWeight ="normal";
        document.getElementById("divReward6").style.fontWeight ="bold";
        document.getElementById("divReward7").style.fontWeight ="normal";
        
       document.getElementById("divReward1").style.backgroundColor ="white";
       document.getElementById("divReward1").style.color ="black";
       document.getElementById("divReward2").style.backgroundColor ="white";
       document.getElementById("divReward2").style.color ="black";
       document.getElementById("divReward3").style.backgroundColor ="white";
       document.getElementById("divReward3").style.color ="black";
       document.getElementById("divReward4").style.backgroundColor ="white";
       document.getElementById("divReward4").style.color ="black";
       document.getElementById("divReward5").style.backgroundColor ="white";
       document.getElementById("divReward5").style.color ="black";
       document.getElementById("divReward6").style.backgroundColor ="#63acd9";
       document.getElementById("divReward6").style.color ="white";
       document.getElementById("divReward7").style.backgroundColor ="white";
       document.getElementById("divReward7").style.color ="black";
       
       document.getElementById("reward").value ="8724";
      document.getElementById("errorReward").style.display ="none";
      
    });
    
    $('#divRewards7').click(function(){
        document.getElementById("divReward1").style.fontWeight ="normal";
        document.getElementById("divReward2").style.fontWeight ="normal";
        document.getElementById("divReward3").style.fontWeight ="normal";
        document.getElementById("divReward4").style.fontWeight ="normal";
        document.getElementById("divReward5").style.fontWeight ="normal";
        document.getElementById("divReward6").style.fontWeight ="normal";
        document.getElementById("divReward7").style.fontWeight ="bold";
        
       document.getElementById("divReward1").style.backgroundColor ="white";
       document.getElementById("divReward1").style.color ="black";
       document.getElementById("divReward2").style.backgroundColor ="white";
       document.getElementById("divReward2").style.color ="black";
       document.getElementById("divReward3").style.backgroundColor ="white";
       document.getElementById("divReward3").style.color ="black";
       document.getElementById("divReward4").style.backgroundColor ="white";
       document.getElementById("divReward4").style.color ="black";
       document.getElementById("divReward5").style.backgroundColor ="white";
       document.getElementById("divReward5").style.color ="black";
       document.getElementById("divReward6").style.backgroundColor ="white";
       document.getElementById("divReward6").style.color ="black";
       document.getElementById("divReward7").style.backgroundColor ="#63acd9";
       document.getElementById("divReward7").style.color ="white";
       
       document.getElementById("reward").value ="8383";
       document.getElementById("errorReward").style.display ="none";
      
    });
    

</script>    
</html>
