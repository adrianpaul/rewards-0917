



<%@page import="Bean.Account"%>
<%@page import="Parser.AccountParser"%>
<%@page import="Bean.FinancialAccount"%>
<%@page import="Parser.FinancialAccountParser"%>
<%@page import="Bean.AccountContact"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.Collections"%>
<%@page import="HTTPManagement.HTTPManagerDirectory"%>
<%@page import="java.net.URISyntaxException"%>
<%@page import="java.net.URI"%>
<%@page import="Others.Constants"%>
<%@page import="Bean.Department"%>
<%@page import="Bean.AccountContactRole"%>
<%@page import="Parser.AccountContactRoleParser"%>
<%@page import="Parser.AccountContactParser"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Parser.DepartmentParser"%>
<!DOCTYPE html>


<html>
<head>
        <%
            String firstName ="";
            String lastName ="";
            String middleName ="";
            String nickName ="";
            String mobileNum ="";
            String telNum ="";
            String emailAdd ="";
            String sex ="";
            String civilStatus ="";
            String spouseName ="";
            String positionInTheCompany ="";
            String tin ="";
            String mothersMaidenName ="";
            String spouseContactNum ="";
            String numberOfChildren ="";
            String education ="";
            String lastSchoolAttended ="";
            String accountContactRoleIdFk ="";
            String status ="";
            String accountIdFk ="";
            String birthdate ="";
            String imageLocation ="";
            String imageName ="";
            String accountContactId ="";
            String financialAccountIdFk ="";
           
      
            URI uriSample = null;
            String urlGetInfo ="";
            String urlAccountContactRole ="";
            String endPoint = Constants.serverURL;
            String email = Constants.email;
            String password = Constants.password;
            HTTPManagerDirectory tp = new HTTPManagerDirectory();

            AccountContactRoleParser accountContactRoleParser = new AccountContactRoleParser();
            ArrayList< AccountContactRole> allAccountContactRole = new ArrayList<AccountContactRole>();
            AccountContactParser accountContactParser = new AccountContactParser();
            
            ArrayList<Department> allDepartments = new ArrayList<Department>();
            DepartmentParser allDepartmentParser = new DepartmentParser();
            
            ArrayList<AccountContact> allAccount = new ArrayList<AccountContact>();
            AccountParser accountParser = new AccountParser();
            ArrayList<Account> account = new ArrayList<Account>();
            
            FinancialAccountParser financialAccountParser = new FinancialAccountParser();
            ArrayList<FinancialAccount> allFinancialAccount = new ArrayList<FinancialAccount>();
            String mobileNumber = (String) session.getAttribute("mobileNumber");
            String formType = (String) session.getAttribute("formType");       
            financialAccountIdFk = (String) session.getAttribute("financialAccountIdFk");
            accountIdFk = (String) session.getAttribute("accountId");
            String gender = (String) session.getAttribute("sex");
            String folderName = Constants.folderName;
           
           if(session.getAttribute("isExisted") == null || session.getAttribute("isExisted").toString().equalsIgnoreCase("false")
                 || session.getAttribute("formType").toString().equalsIgnoreCase("")){
                response.sendRedirect("index.jsp");
            }else{
                 
               
                if(!formType.equalsIgnoreCase("") && formType.equalsIgnoreCase("2")){
                    
                    String getInfo = endPoint + "account/getAccountContacts/"+accountIdFk;
                    System.out.println("getInfo url: " +getInfo);
                     try {
                         uriSample = new URI(getInfo);
                         urlGetInfo = uriSample.toURL().toString();

                     } catch (URISyntaxException ex) {
                          urlGetInfo = endPoint + "account/getAccountContacts/"+accountIdFk;
                     }

                     String allAccountResponse = tp.executeGetRequest(urlGetInfo, "GET", email, password);

                     allAccount = accountContactParser.parseAllAccountContact(allAccountResponse);

                     for(int i=0;i<allAccount.size();i++){
                        mobileNum = allAccount.get(i).getMobileNumber();
                        if(mobileNum.equalsIgnoreCase(mobileNumber)){
                            accountContactId =  allAccount.get(i).getId();
                            firstName = allAccount.get(i).getFirstName();
                            lastName = allAccount.get(i).getLastName();
                            middleName = allAccount.get(i).getMiddleName();
                            nickName = allAccount.get(i).getNickname();
                            emailAdd = allAccount.get(i).getEmail();
                            telNum = allAccount.get(i).getTelNumber();
                            sex = allAccount.get(i).getSex();
                            civilStatus = allAccount.get(i).getCivilStatus();
                            spouseName = allAccount.get(i).getSpouseName();
                            positionInTheCompany = allAccount.get(i).getPositionInTheCompany();
                            tin =  allAccount.get(i).getTin();
                            mothersMaidenName =  allAccount.get(i).getMothersMaidenName();
                            spouseContactNum =  allAccount.get(i).getSpouseContactNum();
                            numberOfChildren =  allAccount.get(i).getNumberOfChildren();
                            education =  allAccount.get(i).getEducation();
                            lastSchoolAttended = allAccount.get(i).getLastSchoolAttended();
                            accountContactRoleIdFk = allAccount.get(i).getAccountContactRoleIdFk();
                            status  =  allAccount.get(i).getStatus();
                            accountIdFk =  allAccount.get(i).getAccountIdFk();
                            birthdate = allAccount.get(i).getBirthdate();

                        }
                        

                     }
                   
                }
                if(!formType.equalsIgnoreCase("") && formType.equalsIgnoreCase("1")){
                     String getAccount = endPoint + "account/getAccountContactByFinancialAccId/?financialAccId="+financialAccountIdFk;
                    System.out.println("getAccount url: " +getAccount);
                     try {
                         uriSample = new URI(getAccount);
                         urlGetInfo = uriSample.toURL().toString();

                     } catch (URISyntaxException ex) {
                          getAccount = endPoint + "account/getAccountContactByFinancialAccId/?financialAccId="+financialAccountIdFk;
                     }

                     String accountResponse = tp.executeGetRequest(getAccount, "GET", email, password);

                     account = accountParser.parseAllAccount(accountResponse);

                    for(int i=0;i<account.size();i++){
                        mobileNum = account.get(i).getMobileNumber();
                        accountIdFk = account.get(i).getAccountIdFk();

                    }
                }
                
                String getAllAccountContactRole = endPoint + "lookup/getPriorityAccountContactRoles";
                try {
                    uriSample = new URI(getAllAccountContactRole);
                    urlAccountContactRole = uriSample.toURL().toString();

                } catch (URISyntaxException ex) {
                     urlAccountContactRole = endPoint + "lookup/getPriorityAccountContactRoles";
                }
                System.out.println("accountIdFk: "+accountIdFk);
                String allAccountContactRoleResponse = tp.executeGetRequest(urlAccountContactRole, "GET", email, password);

                allAccountContactRole = accountContactRoleParser.parseAllAccountContactRole(allAccountContactRoleResponse);
                
                String getAllDepartments = endPoint + "lookup/departmentV2/getAllDepartments";
                String urlDepartments ="";
                try {
                    uriSample = new URI(getAllDepartments);
                    urlDepartments = uriSample.toURL().toString();

                } catch (URISyntaxException ex) {
                     urlDepartments = endPoint + "lookup/departmentV2/getAllDepartments";
                }

                String allDepartmentsResponse = tp.executeGetRequest(urlDepartments, "GET", email, password);

                allDepartments = allDepartmentParser.parseAllDepartment(allDepartmentsResponse);
                
                Collections.sort(allAccountContactRole, new Comparator<AccountContactRole>() {
                        @Override
                        public int compare(AccountContactRole o1, AccountContactRole o2) {
                            return o1.getID().compareTo(o2.getID()); //To change body of generated methods, choose Tools | Templates.
                        }
                });
                
                
                Collections.sort(allDepartments, new Comparator<Department>() {
                        @Override
                        public int compare(Department o1, Department o2) {
                            return o1.getName().compareTo(o2.getName()); //To change body of generated methods, choose Tools | Templates.
                        }
                });
                 
               
           
           }
            
            
            
            
        %>
        <title><%=Constants.appName%> - About Yourself</title>
	<META HTTP-EQUIV="refresh" CONTENT="1800; URL=sessionexpired.jsp"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="../assets/globeIcon.ico" />
         
        <link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.3/css/bootstrap.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.1.1/css/mdb.min.css">
	<link rel="stylesheet"  href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="application/javascript"  href="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/material.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/ripples.min.js">
	<link rel="text/html"  href="https://codepen.io/triss90/pen/VWWMWN">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.2.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
       <!-- Global site tag (gtag.js) - Google Analytics --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-HN82HHLXQ9"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-HN82HHLXQ9'); </script>
      <script>
        if (location.protocol != 'https:') { 
            location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
        }
    </script>          
        
    <script>
    function callSuccess(){
        var regexEmail =/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        var mobileNumberRegex = /^0[0-9-]*$/;
        var errorMobileNumber  = "false";
        var errorEmailAdd ="false";
        var errorLastName  ="false";
        var errorBirthday  ="false";
        var errorPositionInTheCompany  ="false";
        var errorDepartment ="false";
        var errorFirstName ="false";
        var errorSex ="false";
        var errorInterest ="false";
        var errorInCharge ="false";
        var inChargeArray = [];
        var interestArray = [];
        var inChargeCheckboxes = document.querySelectorAll('input[name=inCharge]:checked');
        var interestCheckboxes = document.querySelectorAll('input[name=interest]:checked')
        
        for (var i = 0; i < inChargeCheckboxes.length; i++) {
            inChargeArray.push(inChargeCheckboxes[i].value);
        }
      
        for (var i = 0; i < interestCheckboxes.length; i++) {
            interestArray.push(interestCheckboxes[i].value);
        }
        
        if(!document.querySelector('input[name="inCharge"]:checked')){
            document.getElementById("errorInCharge").style.display="block";
            document.getElementById("divInCharge").scrollIntoView();
            errorInCharge = "true";
        }else{
            document.getElementById("errorInCharge").style.display="none";
            document.getElementById("inCharges").value = inChargeArray;
            errorInCharge = "false";
        }

        
        if(document.getElementById("department").value ===""){
            document.getElementById("errorDepartment").style.display="block";
            document.getElementById("errorDepartment").innerHTML="Enter Department";
            document.getElementById("department").style.border="5px solid red";
            document.getElementById("divDepartment").scrollIntoView();
            errorDepartment ="true";
        }else{
            document.getElementById("errorDepartment").style.display="none";
            document.getElementById("department").style.border="1px solid LightGray";
            errorDepartment ="false"; 
        }
        
        if(document.getElementById("positionInTheCompany").value === ""){
            document.getElementById("errorPositionInTheCompany").style.display="block";
            document.getElementById("errorPositionInTheCompany").innerHTML="Enter Position In The Company";
            document.getElementById("positionInTheCompany").style.border="5px solid red";
            document.getElementById("divPositionInTheCompany").scrollIntoView();
            errorPositionInTheCompany ="true";
        }else{
            document.getElementById("errorPositionInTheCompany").style.display="none";
            document.getElementById("positionInTheCompany").style.border="1px solid LightGray";
            errorPositionInTheCompany ="false"; 
        }
        
        if(!document.querySelector('input[name="interest"]:checked')){
            document.getElementById("errorInterest").style.display="block";
            document.getElementById("divInterest").scrollIntoView();
            errorInterest = "true";
        }else{
            document.getElementById("errorInterest").style.display="none";
            document.getElementById("interests").value = interestArray;
            errorInterest = "false";
        }

        
        if(document.getElementById("emailAdd").value ===""){
            document.getElementById("errorEmailAdd").style.display="block";
            document.getElementById("emailAdd").style.borderColor="red";
            document.getElementById("errorEmailAdd").innerHTML ="Enter Email";
            document.getElementById("emailAdd").focus();
            errorEmailAdd ="true";
        }else if(!regexEmail.test(document.getElementById("emailAdd").value)){
            document.getElementById("errorEmailAdd").style.display="block";
            document.getElementById("errorEmailAdd").innerHTML ="Invalid Email Format";
            document.getElementById("emailAdd").style.borderColor="red";
            document.getElementById("emailAdd").focus();
            errorEmailAdd ="true";
        }else{
            document.getElementById("errorEmailAdd").style.display="none";
            document.getElementById("emailAdd").style.borderColor="LightGray";
            errorEmailAdd ="false";
        }
        
        
        if(document.getElementById("mobileNumber").value ===""){
            document.getElementById("errorMobileNumber").style.display="block";
            document.getElementById("errorMobileNumber").innerHTML="Enter Mobile Number Code";
            document.getElementById("mobileNumber").style.borderColor="red";
            document.getElementById("mobileNumber").focus();
            errorMobileNumber ="true";
        }else if(!mobileNumberRegex.test(document.getElementById("mobileNumber").value)){
            document.getElementById("errorMobileNumber").style.display="block";
            document.getElementById("errorMobileNumber").innerHTML="Invalid Mobile Number Format";
            document.getElementById("mobileNumber").style.borderColor="red";
            document.getElementById("mobileNumber").focus();
            errorMobileNumber ="true";
        }else if(document.getElementById("mobileNumber").value.length !== 11){
            document.getElementById("errorMobileNumber").style.display="block";
            document.getElementById("errorMobileNumber").innerHTML="Mobile number should be 11 characters only";
            document.getElementById("mobileNumber").style.borderColor="red";
            document.getElementById("mobileNumber").focus();
            errorMobileNumber ="true";

        }else{
            document.getElementById("errorMobileNumber").style.display="none";
            document.getElementById("mobileNumber").style.borderColor="LightGray";
            errorMobileNumber ="false";
        }
        
        var regex = /^[+0-9]*$/;
        var yearRegex = /^(19|20)\d{2}$/;
        
      
        
        if(document.getElementById("year").value ===""){
            document.getElementById("errorBirthday").style.display="block";
            document.getElementById("errorBirthday").innerHTML="Enter Year";
            document.getElementById("year").style.borderColor="red";
            document.getElementById("year").focus();
            errorBirthday ="true";
        }else if(!regex.test($('#year').val()){
            document.getElementById("errorBirthday").style.display="block";
            document.getElementById("errorBirthday").innerHTML="Year should be a number.";
            document.getElementById("year").style.borderColor="red";
            document.getElementById("year").focus();
            errorBirthday ="true";
        }else if(!yearRegex.test($('#year').val()){
            document.getElementById("errorBirthday").style.display="block";
            document.getElementById("errorBirthday").innerHTML="Year should be a number.";
            document.getElementById("year").style.borderColor="red";
            document.getElementById("year").focus();
            errorBirthday ="true";
        }else{
            document.getElementById("errorBirthday").style.display="none";
            document.getElementById("year").style.borderColor="LightGray";
            errorBirthday ="false"; 
        }
        
        if(document.getElementById("gender").value ===""){
            document.getElementById("errorSex").style.display="block";
            document.getElementById("divSex").scrollIntoView();
            errorSex ="true";
        }else{
            document.getElementById("errorSex").style.display="none";
            errorSex ="false";
        }
        
   
        
        if(document.getElementById("lastName").value ===""){
            document.getElementById("errorLastName").style.display="block";
            document.getElementById("errorLastName").innerHTML="Enter Last Name";
            document.getElementById("lastName").style.borderColor="red";
            document.getElementById("lastName").focus();
            errorLastName ="true";
        }else{
            document.getElementById("errorLastName").style.display="none";
            document.getElementById("lastName").style.borderColor="LightGray";
            errorLastName ="false"; 
        }
        
        if(document.getElementById("firstName").value ===""){
            document.getElementById("errorFirstName").style.display="block";
            document.getElementById("errorFirstName").innerHTML="Enter First Name";
            document.getElementById("firstName").style.borderColor="red";
            document.getElementById("firstName").focus();
            errorFirstName ="true";
        }else{
            document.getElementById("errorFirstName").style.display="none";
            document.getElementById("firstName").style.borderColor="LightGray";
            errorFirstName ="false"; 
        }
        
        if( errorFirstName === "true" || errorLastName === "true" || errorSex === "true"
            || errorBirthday === "true"  || errorMobileNumber === "true"  || errorEmailAdd === "true"
            || errorInterest === "true" || errorDepartment === "true" || errorPositionInTheCompany === "true"
            || errorInCharge === "true"){
            return false;
        }
    }
    
  function enabledButton(){
     
        if($('#inCharge1').is(':checked')){
            document.getElementById("p1").style.backgroundColor="#00c5e8";
            document.getElementById("span1").style.color="white";

            document.getElementById("inCharge1").style.color="white";

        }

        if($('#inCharge2').is(':checked')){
            document.getElementById("p2").style.backgroundColor="#00c5e8";
            document.getElementById("span2").style.color="white";
            document.getElementById("inCharge2").style.color="white";

        }

        if($('#inCharge3').is(':checked')){
            document.getElementById("p3").style.backgroundColor="#00c5e8";
            document.getElementById("span3").style.color="white";
            document.getElementById("inCharge3").style.color="white";

        }
        
        if($('#inCharge1').is(':checked')){
            document.getElementById("p1").style.backgroundColor="#00c5e8";
            document.getElementById("span1").style.color="white";
            document.getElementById("inCharge1").style.color="white";

        }

        if($('#inCharge4').is(':checked')){
            document.getElementById("p4").style.backgroundColor="#00c5e8";
            document.getElementById("span4").style.color="white";
            document.getElementById("inCharge4").style.color="white";

        }

        if($('#inCharge5').is(':checked')){
            document.getElementById("p5").style.backgroundColor="#00c5e8";
            document.getElementById("span5").style.color="white";
            document.getElementById("inCharge5").style.color="white";

        }

        if($('#inCharge6').is(':checked')){
             document.getElementById("p6").style.backgroundColor="#00c5e8";
             document.getElementById("span6").style.color="white";
             document.getElementById("inCharge6").style.color="white";

        }

        if($('#inCharge7').is(':checked')){
            document.getElementById("p7").style.backgroundColor="#00c5e8";
            document.getElementById("span7").style.color="white";
            document.getElementById("inCharge7").style.color="white";

        }


        if($('#interest1').is(':checked')){
            document.getElementById("interestImage1").src="assets/interestIcons/withboxes/active/activeBusiness.png";
        }else{
            document.getElementById("interestImage1").src="assets/interestIcons/withboxes/inactive/inactiveBusiness.png";
        }

        if($('#interest2').is(':checked')){
            document.getElementById("interestImage2").src="assets/interestIcons/withboxes/active/activeTech.png";

        }else{
            document.getElementById("interestImage2").src="assets/interestIcons/withboxes/inactive/inactiveTech.png";

        }

       
        if($('#interest3').is(':checked')){
            document.getElementById("interestImage3").src="assets/interestIcons/withboxes/active/activeProductivity.png";

        }else{
            document.getElementById("interestImage3").src="assets/interestIcons/withboxes/inactive/inactiveProductivity.png";

        }
        
        if($('#interest4').is(':checked')){
            document.getElementById("interestImage4").src="assets/interestIcons/withboxes/active/activeGlobe.png";

        }else{
            document.getElementById("interestImage4").src="assets/interestIcons/withboxes/inactive/inactiveGlobe.png";

        }

        if($('#interest5').is(':checked')){
            document.getElementById("interestImage5").src="assets/interestIcons/withboxes/active/activeSuccess.png";
        }else{
            document.getElementById("interestImage5").src="assets/interestIcons/withboxes/inactive/inactiveSuccess.png";
        }
        if($('#interest6').is(':checked')){
            document.getElementById("interestImage6").src="assets/interestIcons/withboxes/active/activeExpert.png";
        }else{
            document.getElementById("interestImage6").src="assets/interestIcons/withboxes/inactive/inactiveExpert.png";
       }
    



        if(document.getElementById("gender").value === "Male"){
           document.getElementById("gender1").src ="assets/guyActive.png";
           document.getElementById("gender2").src ="assets/girlInactive.png";
           document.getElementById("gender3").src ="assets/neutralInactive.png";
           document.getElementById("gender").value ="Male";
           document.getElementById("gender3").style.backgroundColor ="white";

           document.getElementById("errorSex").style.display ="none";


        };


        if(document.getElementById("gender").value === "Female"){
           document.getElementById("gender1").src ="assets/guyInactive.png";
           document.getElementById("gender2").src ="assets/girlActive.png";
           document.getElementById("gender3").src ="assets/neutralInactive.png";
           document.getElementById("gender").value ="Female";
           document.getElementById("gender3").style.backgroundColor ="white";
           document.getElementById("errorSex").style.display ="none";
        }

        if(document.getElementById("gender").value === "Prefer Not To Say"){
       
           document.getElementById("gender1").src ="assets/guyInactive.png";
           document.getElementById("gender2").src ="assets/girlInactive.png";
           document.getElementById("gender3").src ="assets/neutralActive.png";
           document.getElementById("gender3").style.backgroundColor ="#63acd9";
           document.getElementById("gender").value ="Prefer Not To Say";
          document.getElementById("errorSex").style.display ="none";

        }
  
     
    }
  
</script> 
<style>

input:focus {
  border-bottom: 1px solid royalblue !important;
  box-shadow: 0 1px 0 0 royalblue !important;
}    
.checkboxDiv{
    background-color:#f7f7f7;
    padding-left: 15px;
    padding-bottom: 15px; 
    padding-right: 15px; 
    padding-top: 23px;
}
.checkbox-changed{
    padding-top: 10px;
}

.checkbox-changed[type="checkbox"]:checked + label:before
{
    top: -4px;
  left: -3px;
  width: 12px;
  height: 22px;
  border-top: 2px solid transparent;
  border-left: 2px solid transparent;
  border-right: 2px solid white; /* You need to change the colour here */
  border-bottom: 2px solid white; /* And here */
  -webkit-transform: rotate(40deg);
  -moz-transform: rotate(40deg);
  -ms-transform: rotate(40deg);
  -o-transform: rotate(40deg);
  transform: rotate(40deg);
  -webkit-backface-visibility: hidden;
  -webkit-transform-origin: 100% 100%;
  -moz-transform-origin: 100% 100%;
  -ms-transform-origin: 100% 100%;
  -o-transform-origin: 100% 100%;
  transform-origin: 100% 100%;

}

.collapsible-header {
    position: relative;
}

.iconadd{
    display: block ;
    position: absolute;
    right: 0;
}
.iconremove{
  display:none !important;
}

li.active .collapsible-header .material-icons.iconadd{
  display: none;
}

li.active .collapsible-header .material-icons.iconremove{
  display:  block !important;
   position: absolute;
    right: 0;
}
.no-shadows {
    box-shadow: none!important;
     border: none;
}

.iconadds{
    display: block ;
    position: absolute;
    right: 0;
}

.collapsible-header, .collapsible-body, .collapsible, ul.collapsible>li 
{
  margin: 0!important;;
  padding: 0!important;
  border: 0!important;
  box-shadow: none!important;
  background: #fff;
}






.interestCheckbox input[type="checkbox"] {
    display: none;
  
}
 </style>   
</head>



<body onLoad="enabledButton()">
      
    <div class="row" >
        <div class="col s12 col m12" style="text-align: center;padding-top: 20px;">
            <img class="responsive-img" src="../assets/illustrations/1x/aboutyourselfmdpi1.png"/> 
            <h4 class="h4-responsive" style="color:#678c96"> Tell us about yourself</h4>

        </div>

    </div>
    <div class="row" >
        <div class="col s12 col m12">
            <div class="col s12 offset-m3 col m6">
                <form action="../AboutYourself2" method="POST" eenctype="multipart/form-data">
                    <ul class="collapsible no-shadows ">
                        <li>
                            <div class="collapsible-header active" style="color:#678c96"> <b>PERSONAL DETAILS</b> <i class="material-icons  iconadd">keyboard_arrow_down</i> <i class="material-icons iconremove">keyboard_arrow_up</i></div>
                            <br><div class="collapsible-body ">
                                    <div class="md-form">
                                        <input type="text"  id="firstName" name="firstName" class="form-control"  style="text-transform:capitalize;" >
                                        <label for="firstName" class="">First Name <font style="color: red;">*</font></label>
                                        <span id="errorFirstName" style="color:red;display:none;font-size:15px;">Enter First Name </span>

                                    </div>  

                                    <div class="md-form">
                                        <input type="text"  id="lastName" name="lastName" class="form-control"  style="text-transform:capitalize;" >
                                        <label for="lastName" class="">Last Name <font style="color: red;">*</font></label>
                                        <span id="errorLastName" style="color:red;display:none;font-size:15px;">Enter Last Name</span>

                                    </div>  
                                <span id="errorSex" style="color:red;display:none;font-size:15px;text-align: center;">Select a Gender</span>

                                <div class="row" id="divSex">
                                    <label style="font-size:15px;">Gender <font style="color: red;">*</font></label>
                                    <div class="col s4" style="text-align:center; font-size: 15px;" id ="divGender1">
                                       <img  name="male" src="../assets/guyInactive.png" alt="" class="circle responsive-img" id="gender1">
                                        <label for="male" >Male  </label>

                                    </div> 
                                    <div class="col s4" style="text-align:center; font-size: 15px;" id ="divGender2">
                                       <img  name="female" src="../assets/girlInactive.png" alt="" class="circle responsive-img" id="gender2">
                                       <label for="female" >Female  </label>
                                    </div>    
                                    <div class="col s4" style="text-align:center; font-size: 10px;" id ="divGender3">
                                        <img  name="neutral" src="../assets/neutralInactive.png" alt="" class="circle responsive-img" id="gender3">
                                       <br><label for="neutral" >Prefer Not To Say  </label>
                                    </div> 

                                </div>
                                <input type="hidden" id="gender" name="gender" class="form-control"  style="text-transform:capitalize;" value="<%=gender%>">

                                <label for="" >Birthday <font style="color: red;">*</font> </label>
                                  
                                <div class="md-form">
                                     
                                    <div class="row">
                                         
                                        <div clas="col m12 col s12">
                                            <div class="col s4 col m4 ">
                                                <select id="month" name="month" class="input-field col m12 col s12 browser-default" >
                                                    <option style="color:#000;" value=""  selected disabled>Select a Month</option>
                                                    <option value="01">January</option>
                                                    <option value="02">February</option>
                                                    <option value="03">March</option>
                                                    <option value="04">April</option>
                                                    <option value="05">May</option>
                                                    <option value="06">June</option>
                                                    <option value="07">July</option>
                                                    <option value="08">August</option>
                                                    <option value="09">September</option>
                                                    <option value="10">October</option>
                                                    <option value="11">November</option>
                                                    <option value="12">December</option>
                                                </select>
                                            </div>
                                            <div class="col s4 col m4 ">
                                                <select id="day" name="day" class="input-field col m12 col s12 browser-default" >
                                                <option style="color:#000;" value=""  selected disabled>Select a Day</option>
                                                </select>
                                            </div>  
                                            <div class="col s4 col m4 " style="margin-top:13px;">
                                                <input type="text"  name="year" id="year" maxLength="4">
                                                <label for="year"> Year</label>
                                            </div>  
                                        </div>    
                                        
                                    </div>    
                                    <span id="errorBirthday" style="color:red;font-size:15px;margin-top:-25px;display:none;">Enter Birthday</span>
                                </div>


                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header active" style="color:#678c96"> <b>CONTACT INFORMATION</b> <i class="material-icons  iconadd">keyboard_arrow_down</i> <i class="material-icons iconremove">keyboard_arrow_up</i></div>
                            <br><div class="collapsible-body">
                                <label style="color:#000000;font-size:15px;">Note: This is where we will send the rewards</label><br>
                                <div class="input-field">
                                    <input type="text"  id="mobileNumber" name="mobileNumber" maxLength="11">
                                    <label  for="mobileNumber">Mobile Number <font style="color: red;">*</font> (format: 09xxxxxxxx)</label>
                                    <span id="errorMobileNumber" style="color:red;display:none;font-size:15px;">Enter Mobile Number</span>
                                </div>
                                
                                <div class="input-field">
                                    <input type="text"  id="contactNumber" name="contactNumber" maxLength="11">
                                    <label  for="contactNumber" style="margin-top: -15px;">Alternate Contact Number<br>(Mobile  Number or Landline Number)</label>
                                    <span id="errorContactNumber" style="color:red;display:none;font-size:15px;">Enter Alternate Contact Number</span>
                                </div><br>


                                
                                <div class="input-field">
                                    <input type="text"  id="emailAdd" name="emailAdd" class="">
                                    <label for="emailAdd" class="">Email Address <font style="color: red;">*</font> </label>
                                    <span id="errorEmailAdd" style="color:red;display:none;font-size:15px;">Enter Email Address.</span>
                                </div><br>
                                
                               

                            </div>


                        </li>
                        <li>
                            <div class="collapsible-header active" style="color:#678c96"> <b>YOUR INTEREST</b> <i class="material-icons  iconadd">keyboard_arrow_down</i> <i class="material-icons iconremove">keyboard_arrow_up</i></div>
                            <br><div class="collapsible-body">
                                <label style="color:#000000;font-size:15px;">Which topic interests you the most? <font style="color:red;">*</font></label><br>
                             
                                <span style="color: red;text-align: center;font-size:15px;" id="errorInterest"></span>
                                <div class="row">
                                    <div class="col s12 col m12 col l12" > 
                                        <div class="col s4 col m4 col l4">
                                            <input type="checkbox" name="interest" id="interest1" value="Business News" />
                                            <br><label  for="interest1"  style="color:black;text-align:center;" >
                                                <img src="../assets/interestIcons/withboxes/inactive/inactiveBusiness.png" class="responsive-img" id="interestImage1">
                                                <label style="color:black;" for="interestImage1">Business News</label>
                                            </label>
                                        </div>
                                        <div  class="col s4 col m4 col l4" style="text-align:center;">
                                            <input  type="checkbox" name="interest"  id="interest2" value="Technology Tips and Trends" />
                                            <br><label  for="interest2"  style="color:black;">
                                                <img src="../assets/interestIcons/withboxes/inactive/inactiveTech.png" class="responsive-img" id="interestImage2">
                                                <label style="color:black;" for="interestImage2">Technology Tips and Trends</label>
                                            </label>
                                        </div>
                                        <div  class="col s4 col m4 col l4" style="text-align:center;">
                                            <input  type="checkbox" name="interest"  id="interest3" value="Productivity Hacks" />
                                            <br><label  for="interest3"  style="color:black;">
                                                <img src="../assets/interestIcons/withboxes/inactive/inactiveProductivity.png" class="responsive-img" id="interestImage3">
                                                <label style="color:black;" for="interestImage3">Productivity Hacks</label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col s12 col m12" > 
                                        <div class="col s4">
                                            <input type="checkbox" name="interest" id="interest4" value="Globe Tricks and Tips" />
                                            <br><label  for="interest4"  style="color:black;text-align:center;" >
                                                <img src="../assets/interestIcons/withboxes/inactive/inactiveGlobe.png" class="responsive-img" id="interestImage4">
                                                <label style="color:black;" for="interestImage4">Globe Tricks and Tips</label>
                                            </label>
                                        </div>
                                        <div  class="col s4" style="text-align:center;">
                                            <input  type="checkbox" name="interest"  id="interest5" value="Success Stories" />
                                            <br><label  for="interest5"  style="color:black;">
                                                <img src="../assets/interestIcons/withboxes/inactive/inactiveSuccess.png" class="responsive-img" id="interestImage5">
                                                <label style="color:black;" for="interestImage5">Success Stories</label>
                                            </label>
                                        </div>
                                        <div  class="col s4 " style="text-align:center;">
                                            <input  type="checkbox" name="interest"  id="interest6" value="Expert Advice" />
                                            <br><label  for="interest6"  style="color:black;">
                                                <img src="../assets/interestIcons/withboxes/inactive/inactiveExpert.png" class="responsive-img" id="interestImage6">
                                                <label style="color:black;" for="interestImage6">Expert Advice</label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <input  type="hidden" class="form-control" name="interests" id="interests"/>
      
                            </div>               

                        </li>

                        <li>
                            <div class="collapsible-header" style="color:#678c96"> <b>YOU AND YOUR COMPANY</b> <i class="material-icons  iconadd">keyboard_arrow_down</i> <i class="material-icons iconremove">keyboard_arrow_up</i></div>
                             <br><div class="collapsible-body">
                                <label class="">Position <font style="color: red;">*</font></label>

                                <div id="divPositionInTheCompany"  >

                                    <select id="positionInTheCompany" name="positionInTheCompany" class="input-field col m12 col s12 browser-default">
                                        <option style="color:#000;" value=""  selected disabled >Select an answer</option>
                                        <%for(int i=0;i<allAccountContactRole.size();i++){
                                                String ids = allAccountContactRole.get(i).getID();
                                                String names = allAccountContactRole.get(i).getName();

                                         %> 
                                                <option value="<%=ids%>" > <%=names%></option>

                                        <%}%>
                                        <input type="text" id="accountContactRole" name="accountContactRole" class="form-control" style="display:none;">

                                    </select>
                                    <span id="errorPositionInTheCompany" style="color:red;display:none;font-size:15px;">Select a Position In The Company</span>
                                </div>


                                <label class="">Department <font style="color: red;">*</font></label>

                                <div id="divDepartment">

                                    <select id="department" name="department"  class="input-field col m12 col s12 browser-default">
                                        <option value=""  selected disabled>Select an answer</option>
                                        <%for(int i=0;i<allDepartments.size();i++){

                                            String ids = allDepartments.get(i).getID();
                                            String names = allDepartments.get(i).getName();
                                            String isActive = allDepartments.get(i).getIsActive();

                                            if(isActive.equalsIgnoreCase("true")){
                                        %> 
                                                <option value="<%=names%>" > <%=names%></option>

                                        <%}}%>

                                    </select>

                                    <span id="errorDepartment" style="color:red;display:none;font-size:15px;">Select a Department</span>
                                </div>

                                <div class="" id="divInCharge" >
                                        <label style="font-size:15px;margin-top: 30px;">When it comes to your telco needs, which of the
                                         following are your responsible for? <font style="color: red;">*</font></label>

                                            <p  class="checkboxDiv" id="p1"  >
                                                <input class="checkbox-changed"  type="checkbox" name="inCharge" id="inCharge1" value="I refer / recommend" />
                                                <label style="color:black;" id="span1" for="inCharge1" >I refer / recommend</label>

                                            </p>
                                            <p  class="checkboxDiv" id="p2" >
                                                <input class="checkbox-changed" type="checkbox" name="inCharge"  id="inCharge2" value="I assess / purchase" />
                                                <label style="color:black;" id="span2" for="inCharge2">I assess / purchase</label>
                                            </p>
                                            <p  class="checkboxDiv" id="p3" >
                                                <input class="checkbox-changed" type="checkbox" name="inCharge"  id="inCharge3" value="I give the final approval" />
                                                <label style="color:black;" id="span3" for="inCharge3">I give the final approval</label>

                                            </p>

                                            <p  class="checkboxDiv" id="p4">
                                                <input class="checkbox-changed" type="checkbox" name="inCharge"  id="inCharge4" value="I handle the technical concern" />
                                                <label style="color:black;" id="span4" for="inCharge4">I handle the technical concern</label>

                                            </p>

                                            <p class="checkboxDiv" id="p5" >

                                                <input class="checkbox-changed" type="checkbox" name="inCharge"  id="inCharge5" value="I reassess / renew" />
                                                <label style="color:black;" id="span5" for="inCharge5">I reassess / renew</label>

                                            </p>


                                            <p  class="checkboxDiv" id="p6" >
                                                <input class="checkbox-changed" type="checkbox" name="inCharge"  id="inCharge6" value="I handle the account concerns like payment and billing" />
                                                <label style="color:black;" id="span6" for="inCharge6">I handle the account concerns like payment and billing</label>
                                            </p>

                                            <p class="checkboxDiv" id="p7" >
                                                <input class="checkbox-changed" type="checkbox" name="inCharge"  id="inCharge7" value="I use the postpaid line" />
                                                <label style="color:black;" id="span7" for="inCharge7">I use the postpaid line</label>
                                            </p> 
                                            <span id="errorInCharge" style="color:red;display:none;font-size:15px;">Check something</span><br>


                                            <input  type="hidden" class="form-control" name="inCharges" id="inCharges"/>

                                    </div>



                            </div>
                        </li>
                    </ul>
                </div>                      
                    <input type="text"  id="accountContactRoleIdFk" name="accountContactRoleIdFk" class="form-control" value="0" style="display:none;" >
                    <input type="text"  id="accountContactId" name="accountContactId" class="form-control" value="<%=accountContactId%>" style="display:none;">
                    <input type="text"  id="accountId" name="accountId" class="form-control" value="<%=accountIdFk%>" style="display:none;">
                    <input type="text"  id="financialAccountIdFk" name="financialAccountIdFk" class="form-control" value="<%=financialAccountIdFk%>"  style="display:none;">
                    <div class="col s12 offset-m3 m6 offset-l5 l2">
                        <button disabled id="btnSubmit" class="waves-light btn"  onClick="return callSuccess();" style="width: 100%;color:white;background-color: #3385ff;" >CONTINUE</button> 
                    </div>    
                </form>   
              
        </div>    

    </div>  
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=cea87e7d-969e-47a5-b787-61b8ff7dadfa"> </script>
    <!-- End of globemybusiness Zendesk Widget script -->   
</body>

<script>
    
    $('#month').change(function(){
        if($('#month').val() ==="01" || $('#month').val() ==="03" || $('#month').val() ==="05" 
                || $('#month').val() ==="07" || $('#month').val() ==="08" || $('#month').val() ==="10" 
                || $('#month').val() ==="12" ){
            for(var i=1;i<=31;i++){
                   
                $("#day").append($("<option></option>").attr("value", i).text(i)); 
                    
              
            }
        } 
        
        if($('#month').val() ==="04" || $('#month').val() ==="06" || $('#month').val() ==="09" 
                || $('#month').val() ==="11"){
            for(var i=1;i<=30;i++){
                $("#day").append($("<option></option>").attr("value", i).text(i)); 
            }
        } 
         
        if($('#month').val() ==="02"){
            for(var i=1;i<=28;i++){
           
                $("#day").append($("<option></option>").attr("value", i).text(i)); 
      
              
            }
        }  
    });
    $('#year').on('keyup', function () {
        var regex = /^[+0-9]*$/;
        var yearRegex = /^(19|20)\d{2}$/;
             
        if($('#year').val() === ""){
		 
            $('#year').css('border-color', 'red');
            $('#year').focus();
            $('#errorBirthday').css('display', 'block');
            $('#errorBirthday').html("Enter Year");
        }else if($('#year').val().length == 4 && !regex.test($('#year').val())){
            $('#year').css('border-color', 'red');
            $('#year').focus();
            $('#errorBirthday').css('display', 'block');
            $('#errorBirthday').html("Year should be a number.");
        }else if($('#year').val().length == 4 && !yearRegex.test($('#year').val())){
            $('#year').css('border-color', 'red');
            $('#year').focus();
            $('#errorBirthday').css('display', 'block');
            $('#errorBirthday').html("Invalid Year Range.");
        }else { 
            $('#year').css('border-color', '#d2d6de');
            $('#errorBirthday').css('display', 'none');
        }
    });
    $('input[name=interest]').change(function(){
    
      var interestArray = [];
        var interestArrayCheckboxes = document.querySelectorAll('input[name=interest]:checked')

        for (var i = 0; i < interestArrayCheckboxes.length; i++) {
            interestArray.push(interestArrayCheckboxes[i].value)
        }
        if($('input[name=interest]').is(':checked')){
            document.getElementById("errorInterest").innerHTML="";
            document.getElementById("interests").value = interestArray;
          
        } else {
            document.getElementById("errorInterest").innerHTML="Please choose at least 1";
          
        }
    });
    
     
    $('input[name=inCharge]').change(function(){
          var inChargeArray = [];
        var inChargeCheckboxes = document.querySelectorAll('input[name=inCharge]:checked')

        for (var i = 0; i < inChargeCheckboxes.length; i++) {
            inChargeArray.push(inChargeCheckboxes[i].value)
        }
        if($('input[name=inCharge]').is(':checked')){
            document.getElementById("errorInCharge").style.display="none";
            document.getElementById("inCharges").value = inChargeArray;
          
            
        } else {
            document.getElementById("errorInCharge").style.display="block";
          
        }
    
    });
    $('#contactNumber').on('keyup', function () {
        var regex = /^[+0-9]*$/;
        var mpobileNumberRegex = /^0[0-9]*$/;
        
       if(!regex.test($('#contactNumber').val())){
            $('#contactNumber').css('border-color', 'red');
              $('#errorContactNumber').css('color', 'red');
            $('#contactNumber').focus();
            $('#errorContactNumber').css('display', 'block');
            $('#errorContactNumber').html("Only numeric characters is allowed.");
        }else if($('#contactNumber').val().length <=11 && $('#contactNumber').val() !==""){
            $('#errorContactNumber').css('color', 'blue');
            $('#contactNumber').focus();
            $('#errorContactNumber').css('display', 'block');
            $('#errorContactNumber').html("Format (if landline: Area Code + 8 digit landline number, if mobile: 09XXXXXXXXX)");
        }else { 
            $('#contactNumber').css('border-color', '#d2d6de');
            $('#errorContactNumber').css('display', 'none');
        }
    });
    
    $('#inCharge1').change(function(){
        if($('#inCharge1').is(':checked')){
            document.getElementById("p1").style.backgroundColor="#00c5e8";
            document.getElementById("span1").style.color="white";
            
            document.getElementById("inCharge1").style.color="white";
            
        }else{
            document.getElementById("p1").style.backgroundColor="#f7f7f7";
            document.getElementById("span1").style.color="black";
            document.getElementById("inCharge1").style.color="black";
            
        }
   
    });
    
    $('#inCharge2').change(function(){
        if($('#inCharge2').is(':checked')){
            document.getElementById("p2").style.backgroundColor="#00c5e8";
            document.getElementById("span2").style.color="white";
            document.getElementById("inCharge2").style.color="white";
            
        }else{
            document.getElementById("p2").style.backgroundColor="#f7f7f7";
            document.getElementById("span2").style.color="black";
            document.getElementById("inCharge2").style.color="black";
            
        }
   
    });
    
    
    $('#inCharge3').change(function(){
        if($('#inCharge3').is(':checked')){
            document.getElementById("p3").style.backgroundColor="#00c5e8";
            document.getElementById("span3").style.color="white";
            document.getElementById("inCharge3").style.color="white";
            
        }else{
            document.getElementById("p3").style.backgroundColor="#f7f7f7";
            document.getElementById("span3").style.color="black";
            document.getElementById("inCharge3").style.color="black";
            
        }
   
    });
    
    $('#inCharge1').change(function(){
        if($('#inCharge1').is(':checked')){
            document.getElementById("p1").style.backgroundColor="#00c5e8";
            document.getElementById("span1").style.color="white";
            document.getElementById("inCharge1").style.color="white";
            
        }else{
            document.getElementById("p1").style.backgroundColor="#f7f7f7";
            document.getElementById("span1").style.color="black";
            document.getElementById("inCharge1").style.color="black";
            
        }
   
    });
    $('#inCharge4').change(function(){
        if($('#inCharge4').is(':checked')){
            document.getElementById("p4").style.backgroundColor="#00c5e8";
            document.getElementById("span4").style.color="white";
            document.getElementById("inCharge4").style.color="white";
            
        }else{
            document.getElementById("p4").style.backgroundColor="#f7f7f7";
            document.getElementById("span4").style.color="black";
            document.getElementById("inCharge4").style.color="black";
            
        }
   
    });

    $('#inCharge5').change(function(){
        if($('#inCharge5').is(':checked')){
            document.getElementById("p5").style.backgroundColor="#00c5e8";
            document.getElementById("span5").style.color="white";
            document.getElementById("inCharge5").style.color="white";
            
        }else{
            document.getElementById("p5").style.backgroundColor="#f7f7f7";
            document.getElementById("span5").style.color="black";
            document.getElementById("inCharge5").style.color="black";
            
        }
   
    });
    
     $('#inCharge6').change(function(){
        if($('#inCharge6').is(':checked')){
            document.getElementById("p6").style.backgroundColor="#00c5e8";
            document.getElementById("span6").style.color="white";
            document.getElementById("inCharge6").style.color="white";
            
        }else{
            document.getElementById("p6").style.backgroundColor="#f7f7f7";
            document.getElementById("span6").style.color="black";
            document.getElementById("inCharge6").style.color="black";
            
        }
   
    });
    
     $('#inCharge7').change(function(){
        if($('#inCharge7').is(':checked')){
            document.getElementById("p7").style.backgroundColor="#00c5e8";
            document.getElementById("span7").style.color="white";
            document.getElementById("inCharge7").style.color="white";
            
        }else{
            document.getElementById("p7").style.backgroundColor="#f7f7f7";
            document.getElementById("span7").style.color="black";
            document.getElementById("inCharge7").style.color="black";
            
        }
   
    });
    
    $('#interest1').change(function(){
        if($('#interest1').is(':checked')){
            document.getElementById("interestImage1").src="../assets/interestIcons/withboxes/active/activeBusiness.png";
        }else{
            document.getElementById("interestImage1").src="../assets/interestIcons/withboxes/inactive/inactiveBusiness.png";
        }
   
    });
     $('#interest2').change(function(){
        if($('#interest2').is(':checked')){
            document.getElementById("interestImage2").src="../assets/interestIcons/withboxes/active/activeTech.png";
        
        }else{
            document.getElementById("interestImage2").src="../assets/interestIcons/withboxes/inactive/inactiveTech.png";
     
        }
   
    });
     $('#interest3').change(function(){
        if($('#interest3').is(':checked')){
            document.getElementById("interestImage3").src="../assets/interestIcons/withboxes/active/activeProductivity.png";
        
        }else{
            document.getElementById("interestImage3").src="../assets/interestIcons/withboxes/inactive/inactiveProductivity.png";
     
        }
   
    });
     $('#interest4').change(function(){
        if($('#interest4').is(':checked')){
            document.getElementById("interestImage4").src="../assets/interestIcons/withboxes/active/activeGlobe.png";
        
        }else{
            document.getElementById("interestImage4").src="../assets/interestIcons/withboxes/inactive/inactiveGlobe.png";
     
        }
   
    });
     $('#interest5').change(function(){
        if($('#interest5').is(':checked')){
            document.getElementById("interestImage5").src="../assets/interestIcons/withboxes/active/activeSuccess.png";
        }else{
            document.getElementById("interestImage5").src="../assets/interestIcons/withboxes/inactive/inactiveSuccess.png";
        }
    });
     $('#interest6').change(function(){
        if($('#interest6').is(':checked')){
            document.getElementById("interestImage6").src="../assets/interestIcons/withboxes/active/activeExpert.png";
        }else{
            document.getElementById("interestImage6").src="../assets/interestIcons/withboxes/inactive/inactiveExpert.png";
       }
    });
    
    
  
    $('#inCharge1').change(function(){
        if($('#inCharge1').is(':checked')){
            document.getElementById("p1").style.backgroundColor="#00c5e8";
            document.getElementById("span1").style.color="white";
            
            document.getElementById("inCharge1").style.color="white";
            
        }else{
            document.getElementById("p1").style.backgroundColor="#f7f7f7";
            document.getElementById("span1").style.color="black";
            document.getElementById("inCharge1").style.color="black";
            
        }
   
    });
    
    $('#inCharge2').change(function(){
        if($('#inCharge2').is(':checked')){
            document.getElementById("p2").style.backgroundColor="#00c5e8";
            document.getElementById("span2").style.color="white";
            document.getElementById("inCharge2").style.color="white";
            
        }else{
            document.getElementById("p2").style.backgroundColor="#f7f7f7";
            document.getElementById("span2").style.color="black";
            document.getElementById("inCharge2").style.color="black";
            
        }
   
    });
    
    
    $('#inCharge3').change(function(){
        if($('#inCharge3').is(':checked')){
            document.getElementById("p3").style.backgroundColor="#00c5e8";
            document.getElementById("span3").style.color="white";
            document.getElementById("inCharge3").style.color="white";
            
        }else{
            document.getElementById("p3").style.backgroundColor="#f7f7f7";
            document.getElementById("span3").style.color="black";
            document.getElementById("inCharge3").style.color="black";
            
        }
   
    });
    
    $('#inCharge1').change(function(){
        if($('#inCharge1').is(':checked')){
            document.getElementById("p1").style.backgroundColor="#00c5e8";
            document.getElementById("span1").style.color="white";
            document.getElementById("inCharge1").style.color="white";
            
        }else{
            document.getElementById("p1").style.backgroundColor="#f7f7f7";
            document.getElementById("span1").style.color="black";
            document.getElementById("inCharge1").style.color="black";
            
        }
   
    });
    $('#inCharge4').change(function(){
        if($('#inCharge4').is(':checked')){
            document.getElementById("p4").style.backgroundColor="#00c5e8";
            document.getElementById("span4").style.color="white";
            document.getElementById("inCharge4").style.color="white";
            
        }else{
            document.getElementById("p4").style.backgroundColor="#f7f7f7";
            document.getElementById("span4").style.color="black";
            document.getElementById("inCharge4").style.color="black";
            
        }
   
    });

    $('#inCharge5').change(function(){
        if($('#inCharge5').is(':checked')){
            document.getElementById("p5").style.backgroundColor="#00c5e8";
            document.getElementById("span5").style.color="white";
            document.getElementById("inCharge5").style.color="white";
            
        }else{
            document.getElementById("p5").style.backgroundColor="#f7f7f7";
            document.getElementById("span5").style.color="black";
            document.getElementById("inCharge5").style.color="black";
            
        }
   
    });
    
     $('#inCharge6').change(function(){
        if($('#inCharge6').is(':checked')){
            document.getElementById("p6").style.backgroundColor="#00c5e8";
            document.getElementById("span6").style.color="white";
            document.getElementById("inCharge6").style.color="white";
            
        }else{
            document.getElementById("p6").style.backgroundColor="#f7f7f7";
            document.getElementById("span6").style.color="black";
            document.getElementById("inCharge6").style.color="black";
            
        }
   
    });
    
     $('#inCharge7').change(function(){
        if($('#inCharge7').is(':checked')){
            document.getElementById("p7").style.backgroundColor="#00c5e8";
            document.getElementById("span7").style.color="white";
            document.getElementById("inCharge7").style.color="white";
            
        }else{
            document.getElementById("p7").style.backgroundColor="#f7f7f7";
            document.getElementById("span7").style.color="black";
            document.getElementById("inCharge7").style.color="black";
            
        }
   
    });
    
    $('#interest1').change(function(){
        if($('#interest1').is(':checked')){
            document.getElementById("interestImage1").src="../assets/interestIcons/withboxes/active/activeBusiness.png";
        }else{
            document.getElementById("interestImage1").src="../assets/interestIcons/withboxes/inactive/inactiveBusiness.png";
        }
   
    });
     $('#interest2').change(function(){
        if($('#interest2').is(':checked')){
            document.getElementById("interestImage2").src="../assets/interestIcons/withboxes/active/activeTech.png";
        
        }else{
            document.getElementById("interestImage2").src="../assets/interestIcons/withboxes/inactive/inactiveTech.png";
     
        }
   
    });
     $('#interest3').change(function(){
        if($('#interest3').is(':checked')){
            document.getElementById("interestImage3").src="../assets/interestIcons/withboxes/active/activeProductivity.png";
        
        }else{
            document.getElementById("interestImage3").src="../assets/interestIcons/withboxes/inactive/inactiveProductivity.png";
     
        }
   
    });
     $('#interest4').change(function(){
        if($('#interest4').is(':checked')){
            document.getElementById("interestImage4").src="../assets/interestIcons/withboxes/active/activeGlobe.png";
        
        }else{
            document.getElementById("interestImage4").src="../assets/interestIcons/withboxes/inactive/inactiveGlobe.png";
     
        }
   
    });
     $('#interest5').change(function(){
        if($('#interest5').is(':checked')){
            document.getElementById("interestImage5").src="../assets/interestIcons/withboxes/active/activeSuccess.png";
        }else{
            document.getElementById("interestImage5").src="../assets/interestIcons/withboxes/inactive/inactiveSuccess.png";
        }
    });
     $('#interest6').change(function(){
        if($('#interest6').is(':checked')){
            document.getElementById("interestImage6").src="../assets/interestIcons/withboxes/active/activeExpert.png";
        }else{
            document.getElementById("interestImage6").src="../assets/interestIcons/withboxes/inactive/inactiveExpert.png";
       }
    });
    
    
  
    $('#inCharge1').change(function(){
        if($('#inCharge1').is(':checked')){
            document.getElementById("p1").style.backgroundColor="#00c5e8";
            document.getElementById("span1").style.color="white";
            
            document.getElementById("inCharge1").style.color="white";
            
        }else{
            document.getElementById("p1").style.backgroundColor="#f7f7f7";
            document.getElementById("span1").style.color="black";
            document.getElementById("inCharge1").style.color="black";
            
        }
   
    });
    
    $('#inCharge2').change(function(){
        if($('#inCharge2').is(':checked')){
            document.getElementById("p2").style.backgroundColor="#00c5e8";
            document.getElementById("span2").style.color="white";
            document.getElementById("inCharge2").style.color="white";
            
        }else{
            document.getElementById("p2").style.backgroundColor="#f7f7f7";
            document.getElementById("span2").style.color="black";
            document.getElementById("inCharge2").style.color="black";
            
        }
   
    });
    
    
    $('#inCharge3').change(function(){
        if($('#inCharge3').is(':checked')){
            document.getElementById("p3").style.backgroundColor="#00c5e8";
            document.getElementById("span3").style.color="white";
            document.getElementById("inCharge3").style.color="white";
            
        }else{
            document.getElementById("p3").style.backgroundColor="#f7f7f7";
            document.getElementById("span3").style.color="black";
            document.getElementById("inCharge3").style.color="black";
            
        }
   
    });
    
    $('#inCharge1').change(function(){
        if($('#inCharge1').is(':checked')){
            document.getElementById("p1").style.backgroundColor="#00c5e8";
            document.getElementById("span1").style.color="white";
            document.getElementById("inCharge1").style.color="white";
            
        }else{
            document.getElementById("p1").style.backgroundColor="#f7f7f7";
            document.getElementById("span1").style.color="black";
            document.getElementById("inCharge1").style.color="black";
            
        }
   
    });
    $('#inCharge4').change(function(){
        if($('#inCharge4').is(':checked')){
            document.getElementById("p4").style.backgroundColor="#00c5e8";
            document.getElementById("span4").style.color="white";
            document.getElementById("inCharge4").style.color="white";
            
        }else{
            document.getElementById("p4").style.backgroundColor="#f7f7f7";
            document.getElementById("span4").style.color="black";
            document.getElementById("inCharge4").style.color="black";
            
        }
   
    });

    $('#inCharge5').change(function(){
        if($('#inCharge5').is(':checked')){
            document.getElementById("p5").style.backgroundColor="#00c5e8";
            document.getElementById("span5").style.color="white";
            document.getElementById("inCharge5").style.color="white";
            
        }else{
            document.getElementById("p5").style.backgroundColor="#f7f7f7";
            document.getElementById("span5").style.color="black";
            document.getElementById("inCharge5").style.color="black";
            
        }
   
    });
    
     $('#inCharge6').change(function(){
        if($('#inCharge6').is(':checked')){
            document.getElementById("p6").style.backgroundColor="#00c5e8";
            document.getElementById("span6").style.color="white";
            document.getElementById("inCharge6").style.color="white";
            
        }else{
            document.getElementById("p6").style.backgroundColor="#f7f7f7";
            document.getElementById("span6").style.color="black";
            document.getElementById("inCharge6").style.color="black";
            
        }
   
    });
    
     $('#inCharge7').change(function(){
        if($('#inCharge7').is(':checked')){
            document.getElementById("p7").style.backgroundColor="#00c5e8";
            document.getElementById("span7").style.color="white";
            document.getElementById("inCharge7").style.color="white";
            
        }else{
            document.getElementById("p7").style.backgroundColor="#f7f7f7";
            document.getElementById("span7").style.color="black";
            document.getElementById("inCharge7").style.color="black";
            
        }
   
    });
    
    $('#interest1').change(function(){
        if($('#interest1').is(':checked')){
            document.getElementById("interestImage1").src="../assets/interestIcons/withboxes/active/activeBusiness.png";
        }else{
            document.getElementById("interestImage1").src="../assets/interestIcons/withboxes/inactive/inactiveBusiness.png";
        }
   
    });
     $('#interest2').change(function(){
        if($('#interest2').is(':checked')){
            document.getElementById("interestImage2").src="../assets/interestIcons/withboxes/active/activeTech.png";
        
        }else{
            document.getElementById("interestImage2").src="../assets/interestIcons/withboxes/inactive/inactiveTech.png";
     
        }
   
    });
     $('#interest3').change(function(){
        if($('#interest3').is(':checked')){
            document.getElementById("interestImage3").src="../assets/interestIcons/withboxes/active/activeProductivity.png";
        
        }else{
            document.getElementById("interestImage3").src="../assets/interestIcons/withboxes/inactive/inactiveProductivity.png";
     
        }
   
    });
     $('#interest4').change(function(){
        if($('#interest4').is(':checked')){
            document.getElementById("interestImage4").src="../assets/interestIcons/withboxes/active/activeGlobe.png";
        
        }else{
            document.getElementById("interestImage4").src="../assets/interestIcons/withboxes/inactive/inactiveGlobe.png";
     
        }
   
    });
     $('#interest5').change(function(){
        if($('#interest5').is(':checked')){
            document.getElementById("interestImage5").src="../assets/interestIcons/withboxes/active/activeSuccess.png";
        }else{
            document.getElementById("interestImage5").src="../assets/interestIcons/withboxes/inactive/inactiveSuccess.png";
        }
    });
     $('#interest6').change(function(){
        if($('#interest6').is(':checked')){
            document.getElementById("interestImage6").src="../assets/interestIcons/withboxes/active/activeExpert.png";
        }else{
            document.getElementById("interestImage6").src="../assets/interestIcons/withboxes/inactive/inactiveExpert.png";
       }
    });
    
    
  
    $('#inCharge1').change(function(){
        if($('#inCharge1').is(':checked')){
            document.getElementById("p1").style.backgroundColor="#00c5e8";
            document.getElementById("span1").style.color="white";
            
            document.getElementById("inCharge1").style.color="white";
            
        }else{
            document.getElementById("p1").style.backgroundColor="#f7f7f7";
            document.getElementById("span1").style.color="black";
            document.getElementById("inCharge1").style.color="black";
            
        }
   
    });
    
    $('#inCharge2').change(function(){
        if($('#inCharge2').is(':checked')){
            document.getElementById("p2").style.backgroundColor="#00c5e8";
            document.getElementById("span2").style.color="white";
            document.getElementById("inCharge2").style.color="white";
            
        }else{
            document.getElementById("p2").style.backgroundColor="#f7f7f7";
            document.getElementById("span2").style.color="black";
            document.getElementById("inCharge2").style.color="black";
            
        }
   
    });
    
    
    $('#inCharge3').change(function(){
        if($('#inCharge3').is(':checked')){
            document.getElementById("p3").style.backgroundColor="#00c5e8";
            document.getElementById("span3").style.color="white";
            document.getElementById("inCharge3").style.color="white";
            
        }else{
            document.getElementById("p3").style.backgroundColor="#f7f7f7";
            document.getElementById("span3").style.color="black";
            document.getElementById("inCharge3").style.color="black";
            
        }
   
    });
    
    $('#inCharge1').change(function(){
        if($('#inCharge1').is(':checked')){
            document.getElementById("p1").style.backgroundColor="#00c5e8";
            document.getElementById("span1").style.color="white";
            document.getElementById("inCharge1").style.color="white";
            
        }else{
            document.getElementById("p1").style.backgroundColor="#f7f7f7";
            document.getElementById("span1").style.color="black";
            document.getElementById("inCharge1").style.color="black";
            
        }
   
    });
    $('#inCharge4').change(function(){
        if($('#inCharge4').is(':checked')){
            document.getElementById("p4").style.backgroundColor="#00c5e8";
            document.getElementById("span4").style.color="white";
            document.getElementById("inCharge4").style.color="white";
            
        }else{
            document.getElementById("p4").style.backgroundColor="#f7f7f7";
            document.getElementById("span4").style.color="black";
            document.getElementById("inCharge4").style.color="black";
            
        }
   
    });

    $('#inCharge5').change(function(){
        if($('#inCharge5').is(':checked')){
            document.getElementById("p5").style.backgroundColor="#00c5e8";
            document.getElementById("span5").style.color="white";
            document.getElementById("inCharge5").style.color="white";
            
        }else{
            document.getElementById("p5").style.backgroundColor="#f7f7f7";
            document.getElementById("span5").style.color="black";
            document.getElementById("inCharge5").style.color="black";
            
        }
   
    });
    
     $('#inCharge6').change(function(){
        if($('#inCharge6').is(':checked')){
            document.getElementById("p6").style.backgroundColor="#00c5e8";
            document.getElementById("span6").style.color="white";
            document.getElementById("inCharge6").style.color="white";
            
        }else{
            document.getElementById("p6").style.backgroundColor="#f7f7f7";
            document.getElementById("span6").style.color="black";
            document.getElementById("inCharge6").style.color="black";
            
        }
   
    });
    
     $('#inCharge7').change(function(){
        if($('#inCharge7').is(':checked')){
            document.getElementById("p7").style.backgroundColor="#00c5e8";
            document.getElementById("span7").style.color="white";
            document.getElementById("inCharge7").style.color="white";
            
        }else{
            document.getElementById("p7").style.backgroundColor="#f7f7f7";
            document.getElementById("span7").style.color="black";
            document.getElementById("inCharge7").style.color="black";
            
        }
   
    });
    
    $('#interest1').change(function(){
        if($('#interest1').is(':checked')){
            document.getElementById("interestImage1").src="../assets/interestIcons/withboxes/active/activeBusiness.png";
        }else{
            document.getElementById("interestImage1").src="../assets/interestIcons/withboxes/inactive/inactiveBusiness.png";
        }
   
    });
     $('#interest2').change(function(){
        if($('#interest2').is(':checked')){
            document.getElementById("interestImage2").src="../assets/interestIcons/withboxes/active/activeTech.png";
        
        }else{
            document.getElementById("interestImage2").src="../assets/interestIcons/withboxes/inactive/inactiveTech.png";
     
        }
   
    });
     $('#interest3').change(function(){
        if($('#interest3').is(':checked')){
            document.getElementById("interestImage3").src="../assets/interestIcons/withboxes/active/activeProductivity.png";
        
        }else{
            document.getElementById("interestImage3").src="../assets/interestIcons/withboxes/inactive/inactiveProductivity.png";
     
        }
   
    });
     $('#interest4').change(function(){
        if($('#interest4').is(':checked')){
            document.getElementById("interestImage4").src="../assets/interestIcons/withboxes/active/activeGlobe.png";
        
        }else{
            document.getElementById("interestImage4").src="../assets/interestIcons/withboxes/inactive/inactiveGlobe.png";
     
        }
   
    });
     $('#interest5').change(function(){
        if($('#interest5').is(':checked')){
            document.getElementById("interestImage5").src="../assets/interestIcons/withboxes/active/activeSuccess.png";
        }else{
            document.getElementById("interestImage5").src="../assets/interestIcons/withboxes/inactive/inactiveSuccess.png";
        }
    });
     $('#interest6').change(function(){
        if($('#interest6').is(':checked')){
            document.getElementById("interestImage6").src="../assets/interestIcons/withboxes/active/activeExpert.png";
        }else{
            document.getElementById("interestImage6").src="../assets/interestIcons/withboxes/inactive/inactiveExpert.png";
       }
    });
    
    
  
    
    $('#divGender1').click(function(){
       document.getElementById("gender1").src ="../assets/guyActive.png";
       document.getElementById("gender2").src ="../assets/girlInactive.png";
       document.getElementById("gender3").src ="../assets/neutralInactive.png";
       document.getElementById("gender").value ="Male";
        document.getElementById("gender3").style.backgroundColor ="white";
     
       document.getElementById("errorSex").style.display ="none";
          
    
    });
    
    
    $('#divGender2').click(function(){
       document.getElementById("gender1").src ="../assets/guyInactive.png";
       document.getElementById("gender2").src ="../assets/girlActive.png";
       document.getElementById("gender3").src ="../assets/neutralInactive.png";
       document.getElementById("gender").value ="Female";
       document.getElementById("gender3").style.backgroundColor ="white";
       
       document.getElementById("errorSex").style.display ="none";
      
       
           
    
    });
    
    $('#divGender3').click(function(){
       document.getElementById("gender1").src ="../assets/guyInactive.png";
       document.getElementById("gender2").src ="../assets/girlInactive.png";
       document.getElementById("gender3").src ="../assets/neutralActive.png";
       document.getElementById("gender3").style.backgroundColor ="#63acd9";
       
       
       document.getElementById("gender").value ="Prefer Not To Say";
        document.getElementById("errorSex").style.display ="none";
      
    });
  
    $('#firstName').on('keyup', function () {
        if($('#firstName').val() === ""){

            $('#firstName').css('border-color', 'red');
            $('#errorFirstName').css('display', 'block');
        } else {
            $('#firstName').css('border-color', '#d2d6de');
            $('#errorFirstName').css('display', 'none');
        }
        
    });
  
  
    $('#lastName').on('keyup', function () {
        if($('#lastName').val() === ""){

            $('#lastName').css('border-color', 'red');
            $('#errorLastName').css('display', 'block');
        } else {
            $('#lastName').css('border-color', '#d2d6de');
            $('#errorLastName').css('display', 'none');
        }
    });
  
    
    $('#emailAdd').on('keyup', function () {
         var regexEmail =/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

        if($('#emailAdd').val() === ""){
            $('#emailAdd').css('border-color', 'red');
            $('#errorEmailAdd').css('display', 'block');
            $('#errorEmailAdd').text("Enter Email Address");
        } else if(!regexEmail.test($('#emailAdd').val())){
            $('#emailAdd').css('border-color', 'red');
            $('#errorEmailAdd').css('display', 'block');
            $('#errorEmailAdd').text("Invalid Email Format");
        }else { // does exist
            $('#emailAdd').css('border-color', '#d2d6de');
            $('#errorEmailAdd').css('display', 'none');
         }
    });
    
    
    $('#mobileNumber').on('keyup', function () {
        var regex = /^0[0-9]*$/;
             
        if($('#mobileNumber').val() === ""){
            $('#mobileNumber').css('border-color', 'red');
            $('#errorMobileNumber').css('display', 'block');
            $('#errorMobileNumber').html("Enter Mobile Number");
        }else if(!regex.test($('#mobileNumber').val())){
            $('#mobileNumber').css('border-color', 'red');
            $('#errorMobileNumber').css('display', 'block');
            $('#errorMobileNumber').html("Invalid Mobile Number Format.");
        }else if($('#mobileNumber').val().length !== 11){
            $('#mobileNumber').css('border-color', 'red');
            $('#errorMobileNumber').css('display', 'block');
            $('#errorMobileNumber').html("Mobile number should be 11 characters only.");
        }else { 
            $('#mobileNumber').css('border-color', '#d2d6de');
            $('#errorMobileNumber').css('display', 'none');
        }
    });
  
    var fields = "#firstName, #lastName,#month,#day, #year, #mobileNumber, #emailAdd, input[name=interest], #positionInTheCompany, #department, input[name=inCharge]";

    $(fields).on('change', function() {
        var flagFirstName = false;
        var flagLastName = false;
        var flagYear = false;
        var flagMonth = false;
        var flagDay = false;
        
        var flagMobileNumber = false;
        var flagEmailAddress = false;
        var flagInterest= false;
        var flagPositionInTheCompany = false;
        var flagDepartment = false;
        var flagInCharge = false;
        
      
        
        if($('#firstName').val() === ""){

            flagFirstName = false;
        } else {
             flagFirstName  = true;
        }
        
        if($('#lastName').val() === ""){
            flagLastName = false;
        } else {
            flagLastName  = true;
        }
        
        if($('#month  :selected').val() === ""){
          flagMonth = false;
        
        } else {
           flagMonth  = true;
        }
        
         if($('#day  :selected').val() === ""){
             flagDay = false;
        
        } else {
           flagDay  = true;
        }
        
        if($('#year').val() === ""){
             flagYear = false;
        
        } else {
           flagYear  = true;
        }
        
        var regex = /^[+0-9]*$/;
             
        if($('#mobileNumber').val() === ""){
             flagMobileNumber = false;
        
        }else if(!regex.test($('#mobileNumber').val())){
            flagMobileNumber = false;
        
        }else if($('#mobileNumber').val().length !== 11){
           flagMobileNumber = false;
        
        }else { 
             flagMobileNumber = true;
        
        }
        

        var regexEmail =/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

        if($('#emailAdd').val() === ""){
            flagEmailAddress = false;

        } else if(!regexEmail.test($('#emailAdd').val())){

            flagEmailAddress = false;

        }else { // does exist
             flagEmailAddress = true;
         }

        var interestArray = [];
        var interestArrayCheckboxes = document.querySelectorAll('input[name=interest]:checked')

        for (var i = 0; i < interestArrayCheckboxes.length; i++) {
            interestArray.push(interestArrayCheckboxes[i].value)
        }
        if($('input[name=interest]').is(':checked')){
            flagInterest = true;
 
        } else {
            flagInterest = false;

        }
        
     
        if($('#positionInTheCompany  :selected').val() === ""){
            flagPositionInTheCompany = false;
            
        } else {
            flagPositionInTheCompany = true;
            $('#accountContactRole').val($('#positionInTheCompany  :selected').text());
        }
  
        if($('#department  :selected').val() === ""){
            flagDepartment = false;

        } else {
            flagDepartment = true;


        }
        var inChargeArray = [];
        var inChargeCheckboxes = document.querySelectorAll('input[name=inCharge]:checked')

        for (var i = 0; i < inChargeCheckboxes.length; i++) {
            inChargeArray.push(inChargeCheckboxes[i].value)
        }
        if($('input[name=inCharge]').is(':checked')){
            flagInCharge = true;
 
            
        } else {
            flagInCharge = false;
 
        }
        
        
        if (flagFirstName === false || flagLastName === false
        || flagMonth === false || flagDay === false|| flagYear === false || flagMobileNumber === false || flagEmailAddress === false
        || flagInterest === false || flagPositionInTheCompany === false || flagDepartment === false
        || flagInCharge  === false) {
           $('#btnSubmit').prop('disabled', true);
        } else {
            $('#btnSubmit').removeAttr('disabled');
        }
    });

     $(document).ready(function(){
      $('.collapsible').collapsible();
    });
    
    $(document).ready(function(){
        $('.datepicker').on('mousedown',function(event){
            event.preventDefault();
        })
        $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 90, // Creates a dropdown of 15 years to control year,
        format: 'dd-mm-yyyy',
        today: "TODAY",
        clear:false,
        min: new Date(1930,1,1),
        max: new Date(2020,1,1)

      });
      
    });
    $(document).ready(function() {
        $('select').material_select();
    });
        
</script> 

 
</html>
