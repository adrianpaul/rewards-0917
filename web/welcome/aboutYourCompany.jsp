



<%@page import="Parser.AccountsParser"%>
<%@page import="Bean.Accounts"%>
<%@page import="Parser.IndustryParser"%>
<%@page import="Bean.Industry"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.Collections"%>
<%@page import="HTTPManagement.HTTPManagerDirectory"%>
<%@page import="java.net.URISyntaxException"%>
<%@page import="java.net.URI"%>
<%@page import="Others.Constants"%>
<%@page import="Bean.Department"%>
<%@page import="Bean.AccountContactRole"%>
<%@page import="Parser.AccountContactRoleParser"%>
<%@page import="Parser.AccountContactParser"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Parser.DepartmentParser"%>
<!DOCTYPE html>


<html>
<head>
        <%
            
            ArrayList<Industry> allIndustry = new ArrayList<Industry>();
            IndustryParser industryParser = new IndustryParser();
            
            ArrayList<Industry> othersIndustry = new ArrayList<Industry>();
            IndustryParser allIndustryParser = new IndustryParser();
            
            String companyName ="";
            String folderName = Constants.folderName;
           
           
            if(session.getAttribute("isExisted") == null || session.getAttribute("isExisted").toString().equalsIgnoreCase("false")
                 || session.getAttribute("inCharge").toString().equalsIgnoreCase("")){
                response.sendRedirect("index.jsp");
            }else{
            
            String firstName = (String) session.getAttribute("firstName");
            String lastName = (String) session.getAttribute("lastName");
            String accountContactIdFk = (String) session.getAttribute("accountContactIdFk");
            String billingContactEmail = (String) session.getAttribute("billingContactEmail");
            String mobileNum = (String) session.getAttribute("mobileNum");
            String sex = (String) session.getAttribute("sex");
            String positionInTheCompany = (String) session.getAttribute("positionInTheCompany").toString().replaceFirst(" ","");
            String financialAccountIdFk = (String) session.getAttribute("financialAccountIdFk");
            String birthday = (String) session.getAttribute("birthday");
            String accountContactRoleIdFk = (String) session.getAttribute("accountContactRoleIdFk");
            String department = (String) session.getAttribute("department");
            String inCharge = (String) session.getAttribute("inCharge");
            String gppChecked = (String) session.getAttribute("isCheckedPrivacy");
            String interest = (String) session.getAttribute("interest");
            String accountId = (String) session.getAttribute("accountId");
       
          
            
            System.out.println("accountId:  " + accountId);
            System.out.println("accountContactIdFk: "+accountContactIdFk);
            System.out.println("accountContactRoleIdFk: " +accountContactRoleIdFk);
            System.out.println("financialAccountIdFk:  " + financialAccountIdFk);
            System.out.println("firstName:  " + firstName);
            System.out.println("lastName:  " + lastName);
            System.out.println("billingContactEmail:  " + billingContactEmail);
            System.out.println("mobileNum:  " + mobileNum);
            System.out.println("sex:  " + sex);
            System.out.println("positionInTheCompany:  " + positionInTheCompany);
            System.out.println("department:  " + department);
            System.out.println("birthdate:  " + birthday);
            System.out.println("interest: "+interest);
            System.out.println("inCharge: "+inCharge);
            
            
         
            URI uriSample = null;
            String endPoint = Constants.serverURL;
            String email = Constants.email;
            String password = Constants.password;
            HTTPManagerDirectory tp = new HTTPManagerDirectory();
            
            Accounts account = new Accounts();
            AccountsParser accountsParser = new AccountsParser();
            
            
            String getAccounts = endPoint + "account/getAccount/"+accountId;
            
            System.out.println("getAccounts: "+getAccounts);
            String urlAccounts ="";
            try {
                uriSample = new URI(getAccounts);
                urlAccounts = uriSample.toURL().toString();

            } catch (URISyntaxException ex) {
                 urlAccounts = endPoint + "account/getAccount/"+accountId;
            }

            String accountResponse = tp.executeGetRequest(urlAccounts, "GET", email, password);
//            System.out.println("accountResponse: "+accountResponse);
            account = accountsParser.accountsParseFeed(accountResponse);
           
            if(!account.getCompanyName().toString().equals("")){
                companyName = account.getCompanyName();
            }else{
                companyName = "";
            }
            
            System.out.println("companyName:" +companyName);
            
           
            String getAllIndustry = endPoint + "lookup/industryType/getPriorityIndustyTypes";
            String urlIndustry ="";
            try {
                uriSample = new URI(getAllIndustry);
                urlIndustry = uriSample.toURL().toString();

            } catch (URISyntaxException ex) {
                 urlIndustry = endPoint + "lookup/industryType/getPriorityIndustyTypes";
            }

            String allIndustryResponse = tp.executeGetRequest(urlIndustry, "GET", email, password);
//            System.out.println("allIndustryResponse: "+allIndustryResponse);
            allIndustry = industryParser.parseAllIndustry(allIndustryResponse);
             String finalUrlEmail ="";
         
            String getOthersIndustry = endPoint + "lookup/industryType/getAllIndustryTypes";
//            System.out.println("getOthersIndustry "+getOthersIndustry);
            try {
                uriSample = new URI(getOthersIndustry);
                finalUrlEmail = uriSample.toURL().toString();

            } catch (URISyntaxException ex) {
                finalUrlEmail = endPoint + "lookup/industryType/getAllIndustryTypes";
            }

            String othersIndustryResponse = tp.executeGetRequest(finalUrlEmail, "GET", email, password);
            System.out.println("othersIndustryResponse "+othersIndustryResponse);
            othersIndustry = allIndustryParser.parseAllIndustry(othersIndustryResponse);


            Collections.sort(allIndustry, new Comparator<Industry>() {
                    @Override
                    public int compare(Industry o1, Industry o2) {
                        return o1.getName().compareTo(o2.getName()); //To change body of generated methods, choose Tools | Templates.
                    }
            });
            
            Collections.sort(othersIndustry, new Comparator<Industry>() {
                    @Override
                    public int compare(Industry o1, Industry o2) {
                        return o1.getName().compareTo(o2.getName()); //To change body of generated methods, choose Tools | Templates.
                    }
            });

            }
            
            
        %>
    <title><%=Constants.appName%> - About Your Company</title>
	
	<META HTTP-EQUIV="refresh" CONTENT="1800; URL=sessionexpired.jsp"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../assets/globeIcon.ico" />
        
    <link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.3/css/bootstrap.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.1.1/css/mdb.min.css">
	<link rel="stylesheet"  href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="application/javascript"  href="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/material.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/ripples.min.js">
	<link rel="text/html"  href="https://codepen.io/triss90/pen/VWWMWN">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.2.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<!-- Compiled and minified CSS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <!-- Global site tag (gtag.js) - Google Analytics --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-HN82HHLXQ9"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-HN82HHLXQ9'); </script>

    <script>
        if (location.protocol != 'https:') { 
            location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
        }
    </script>          
        
<style>

input:focus {
  border-bottom: 1px solid royalblue !important;
  box-shadow: 0 1px 0 0 royalblue !important;
}    


.checkbox-changed[type="checkbox"].filled-in:checked + span:not(.lever):after 
{
    border: 2px solid transparent;  
    background-color: transparent;
}


.collapsible-header {
    position: relative;
}

.iconadd{
    display:inline-block ;
    position: absolute;
    right: 0;
}
.iconremove{
  display:none !important;
}

li.active .collapsible-header .material-icons.iconadd{
  display: none;
}

li.active .collapsible-header .material-icons.iconremove{
  display: inline-block !important;
   position: absolute;
    right: 0;
}
.no-shadows {
    box-shadow: none!important;
     border: none;
}

.iconadds{
    display:inline-block ;
    position: absolute;
    right: 0;
}
.collapsible-header, .collapsible-body, .collapsible, ul.collapsible>li 
{
  margin: 0!important;;
  padding: 0!important;
  border: 0!important;
  box-shadow: none!important;
  background: #fff;
}


 </style>
 
 <script>
     function callSuccessed(){
        var regexEmail =/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
              
        var errorCompanyName ="false";	
        var errorNumberOfEmployees ="false";	
        var errorIndustry ="false";	
        var errorLocation ="false";
        var errorBillingContactPerson ="false";
        var errorBillingContactEmail ="false";
        
        
         if(document.getElementById("billingContactEmail").value ===""){
            document.getElementById("errorBillingContactEmail").style.display="block";
            document.getElementById("billingContactEmail").style.borderColor="red";
            document.getElementById("errorBillingContactEmail").innerHTML ="Enter Email";
            document.getElementById("billingContactEmail").focus();
            errorBillingContactEmail ="true";
        }else if(!regexEmail.test(document.getElementById("billingContactEmail").value)){
            document.getElementById("errorBillingContactEmail").style.display="block";
            document.getElementById("billingContactEmail").style.borderColor="red";
            document.getElementById("billingContactEmail").focus();
            errorBillingContactEmail ="true";
        }else{
            document.getElementById("errorBillingContactEmail").style.display="none";
            document.getElementById("billingEmail").style.borderColor="LightGray";
            errorBillingContactEmail ="false";
        }
       
        
        if(document.getElementById("billingContactPerson").value === ""){
                document.getElementById("errorBillingContactPerson").style.display="block";
                document.getElementById("billingContactPerson").focus();
                document.getElementById("billingContactPerson").style.borderColor="red";
                errorBillingContactPerson = "true";

        }else{
                document.getElementById("errorBillingContactPerson").style.display="none";
                document.getElementById("billingContactPerson").style.borderColor="LightGray";
                errorBillingContactPerson ="false";
        }

        if(document.getElementById("location").value === ""){
                document.getElementById("errorLocation").style.display="block";
                document.getElementById("divBranch1").scrollIntoView();
                errorLocation = "true";

        }else{
                document.getElementById("errorLocation").style.display="none";
               errorLocation ="false";
        }

        if(document.getElementById("industry").value === ""){
                document.getElementById("errorIndustry").style.display="block";
                document.getElementById("divIndustry").scrollIntoView();
                errorIndustry = "true";

        }else{
                document.getElementById("errorIndustry").style.display="none";
                errorIndustry ="false";
        }

        if(document.getElementById("numberOfEmployees").value ===""){
                document.getElementById("errorNumberOfEmployees").style.display="block";
                document.getElementById("divNumberOfEmployees").scrollIntoView();
                errorNumberOfEmployees ="true";  

        }else{
                document.getElementById("errorNumberOfEmployees").style.display="none";
                errorNumberOfEmployees ="false";
        }



        if(document.getElementById("companyName").value ===""){
                document.getElementById("errorCompanyName").style.display="block";
                document.getElementById("companyName").style.borderColor="red";
                document.getElementById("companyName").focus();
                errorCompanyName ="true";
        }else{
                document.getElementById("errorCompanyName").style.display="none";
                document.getElementById("companyName").style.borderColor="LightGray";
                errorCompanyName = "false";
        }




            if(errorCompanyName === "true"   || errorNumberOfEmployees === "true" 
                || errorIndustry === "true"  || errorLocation === "true" 
               ){
                return false;
            }
     
        
    }
    
     function enabledButton(){      
                   
        var errorCompanyName ="false";	
        var errorNumberOfEmployees ="false";	
        var errorIndustry ="false";	
        var errorLocation ="false";
       
        if(document.getElementById("branch").value === ""){
                errorLocation = "true";

        }else{
               errorLocation ="false";
        }

        if(document.getElementById("industry").value === ""){
               errorIndustry = "true";

        }else{
               errorIndustry ="false";
        }

        if(document.getElementById("numberOfEmployees").value ===""){
                errorNumberOfEmployees ="true";  

        }else{
                 errorNumberOfEmployees ="false";
        }



        if(document.getElementById("companyName").value ===""){
          
                errorCompanyName ="true";
        }else{
                errorCompanyName = "false";
        }
        
        if(errorCompanyName === "false"   || errorNumberOfEmployees === "false" 
                || errorIndustry === "false"  || errorLocation === "false"){
                $('#btnSubmit').removeAttr('disabled');
        }else{
            document.getElementById("btnSubmit").disabled= true;
        }
  
       
    }
    
 </script>    
 
</head>



<body onLoad="enabledButton()">
    <div class="row" >
         <div class="col s12 col m12" style="text-align: center;padding-top:20px;">
             <img class="responsive-img" src="../assets/illustrations/1x/aboutcompanymdpi1.png"/> 
             <br><h4 class="h4-responsive" style="color:#678c96"> How about your company?</h4>

         </div>

     </div>
     <div class="row" >
         <div class="col s12 col m12">

             <div class="col s12 offset-m3 col m6">
                 <form action="../AboutYourCompany2" method="POST" enctype="multipart/form-data">
                     <ul class="collapsible no-shadows" data-collapsible="accordion">
                         <li>
                             <div class="collapsible-header active" style="color:#678c96"> <b>COMPANY DETAILS</b> <i class="material-icons  iconadd">keyboard_arrow_down</i> <i class="material-icons iconremove">keyboard_arrow_up</i></div>
                             <br><div class="collapsible-body ">
                                 <div class="md-form">
                                     <input type="text"  id="companyName" name="companyName" class="form-control"  style="text-transform:capitalize;" value="<%=companyName%>">
                                     <label for="companyName" >Company Name <font style="color: red;">*</font></label>
                                     <p style="font-size:13px;">Please indicate your Corporate Registered Name</p>
                                     <span id="errorCompanyName" style="color:red;display:none;font-size:15px;">Enter Company Name</span>

                                 </div>  
                               <label for="" style="font-size: 14px;">Number of Employees <font style="color: red;">*</font></label>

                                <div id="divNumberOfEmployees">
                                   
                                     <select id="numberOfEmployees" name="numberOfEmployees" class="input-field col m12 col s12 browser-default">
                                         <option style="color:#000;" value=""  selected disabled>Select an answer</option>
                                         <option  value="0 to 9">0 to 9</option>
                                         <option  value="10 to 49">10 to 49</option>
                                         <option value="50 to 99">50 to 99</option>
                                         <option value="100 to 199">100 to 199</option>
                                         <option value="200 and up">200 and up</option>



                                     </select>
                                     <span id="errorNumberOfEmployees" style="color:red;display:none;font-size:15px;">Select Number of Employees</span>
                                 </div>
                                 
                             
                                 <div id="divIndustry"  >
                                    <label for="" style="margin-top:20px;font-size: 14px;" >Industry <font style="color: red;">*</font></label>

                                     <select id="industry" name="industry" class="input-field col m12 col s12 browser-default">
                                         <option value=""  selected disabled>Select an answer</option>
                                         <%for(int i=0; i<allIndustry.size(); i++){%>

                                          <option value="<%=allIndustry.get(i).getName()%>"><%=allIndustry.get(i).getName()%></option>

                                      <%}%>
                                         <option value="OTHERS">OTHERS</option>

                                     </select>
                                     <span id="errorIndustry" style="display:none;color: red;">Select Industry.</span>
                                 </div><br>

                                 <div id="divOthersIndustry" style="display:none;">
                                     <label for="othersIndustry" style="margin-top:20px;font-size: 14px;">Other Industry <font style="color: red;">*</font></label>

                                     <select id="othersIndustry" name="othersIndustry" class="input-field col m12 col s12 browser-default">
                                         <option value=""  selected disabled>Select an answer</option>
                                         <%for(int i=0; i<othersIndustry.size(); i++){%>

                                          <option value="<%=othersIndustry.get(i).getName()%>"><%=othersIndustry.get(i).getName()%></option>

                                      <%}%>


                                     </select>
                                     <span id="errorOtherIndustry" style="display:none;color: red;">Select Industry.</span>
                                 </div><br><br>


                                    
                                 
                                   
                                <div id="divBranch">
                                     <label style="margin-top:20px;font-size: 14px;" >Number of Office Locations/Branches<font style="color: red;">*</font></label>
                               
                                   
                                     <select id="branch" name="branch" class="input-field col m12 col s12 browser-default">
                                         <option style="color:#000;" value=""  selected disabled>Select an answer</option>
                                         <option  value="1">1</option>
                                         <option  value="2-5">2-5</option>
                                         <option value="5-10">5-10</option>
                                         <option value="More than 10">More than 10</option>
                                         
                                     </select>
                                     <span id="errorBranch" style="color:red;display:none;font-size:15px;">Select Number of Employees</span>
                                 </div><br><br>
                                
                                <%--<div class="row" >  
                                    <div class="col s12 col m12" > 
                                      
                                        <div id="divBranch1" class="offset-s2 col s4 offset-m2 col m4" style="text-align:center; font-size: 15px;" id ="divLocation1">
                                            <img style name="singleBranch" src="assets/234567.jpg" alt="" class="circle responsive-img" id="location1">
                                             <label for="singleBranch" >Single <br> Branch  </label>

                                         </div> 

                                         <div id="divBranch2" class="col s4 col m4" style="text-align:center; font-size: 15px;" id ="divLocation2">
                                            <img  name="multiBranch" src="assets/234567.jpg" alt="" class="circle responsive-img" id="location2">
                                            <label for="multiBranch" >Multiple <br> Branches  </label>
                                         </div> 


                                        <input  type="text" class="form-control" name="location" id="location" style="display:none;"/>
                                    </div>
                                      
                                 </div>--%>

                                <br><label style="margin-top:20px;font-size: 14px;" >Company Anniversary  </label>
                                 
                                <div class="row">

                                    <div class="col s12 col m12 md-form">
                                        <div class="col s6 col m6 ">
                                            
                                            <select id="month" name="month" class="input-field col m12 col s12 browser-default" >
                                                <option style="color:#000;" value=""  selected disabled>Select a Month</option>
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05">May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                        </div>
                                        <div class="col s6 col m6 " style="margin-top:13px;">
                                            <input type="text"  name="year" id="year" maxLength="4">
                                            <label for="year"> Year</label>
                                        </div>
                                    </div>
                                   
                                  
                                 </div>   
                                    <span id="errorCompanyAnniversary" style="color:red;font-size:15px;margin-top: -45px;display:none;">Enter Company Anniversary</span>
                                    <input type="text" name="companyAnniversary" id="companyAnniversary" style="display:none;">
     
                               
                             </div>
                                
                         </li><br>
                         <li style="display:none;">
                             <div class="collapsible-header" style="color:#678c96"> <b>BILLING  DETAILS</b> <i class="material-icons  iconadd">keyboard_arrow_down</i> <i class="material-icons iconremove">keyboard_arrow_up</i></div>
                             <div class="collapsible-body">
                                 <div class="input-field">
                                     <input type="text"  id="billingContactPerson" name="billingContactPerson" >
                                     <label  for="billingContactPerson">Billing Contact Person</label>
                                     <span id="errorBillingContactPerson" style="color:red;display:none;font-size:15px;">Enter Billing Contact Person</span>
                                 </div>
                                 <div class="input-field">
                                     <input type="text"  id="billingContactEmail" name="billingContactEmail" >
                                     <label for="billingContactEmail" >Billing Email Address</label>
                                     <span id="errorBillingContactEmail" style="color:red;display:none;font-size:15px;">Enter Billing Contact Email.</span>
                                 </div>
                                 <label for="" >Company Billing Address</label>

                                 <div class="input-field">
                                     <div id="divCity" class="browser-default">
                                         <label for="city" >City</label>

                                         <select id="city" name="city" >
                                             <option style="color:#000;" value=""  selected disabled>Select a City</option>

                                         </select>
                                         <span id="errorCity" style="color:red;display:none;font-size:15px;">Select a City</span>
                                     </div>

                                     <div id="divProvince" class="browser-default">
                                         <label for="province" >Province</label>

                                         <select id="province" name="province" >
                                             <option style="color:#000;" value=""  selected disabled>Select a Province</option>

                                         </select>
                                         <span id="errorProvince" style="color:red;display:none;font-size:15px;">Select a Province</span>
                                     </div>
                                 </div>



                             </div>


                         </li>
                     </ul>


                     <div class="col s12 offset-m2 col m4">
                         <a href="aboutYourself.jsp" class="waves-light btn  blue-text text-darken-4" style="width: 100%;background-color: white; border: 2px solid #3385ff;">GO BACK</a> 

                     </div>
                     <div class="col s12  col m4">
                         <button id="btnSubmit" class="waves-light btn"  onClick="return callSuccessed();" style="width: 100%;color:white;background-color: #3385ff;" disabled>CONTINUE</button> 
                     </div>


                 </form>                            
             </div>    
         </div>



     </div>  
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=cea87e7d-969e-47a5-b787-61b8ff7dadfa"> </script>
    <!-- End of globemybusiness Zendesk Widget script -->     
</body>

<script>
        $('#year').on('keyup', function () {
            var regex = /^[+0-9-]*$/;
            var yearRegex = /^(19|20)\d{2}$/;
       
            if($('#year').val() === ""){
                $('#year').css('border-color', 'red');
                $('#errorCompanyAnniversary').css('display', 'block');
                $('#errorCompanyAnniversary').html("Invalid Format");
            }else if($('#year').val().length === 4 && !regex.test($('#year').val())){
                $('#year').css('border-color', 'red');
                $('#errorCompanyAnniversary').css('display', 'block');
                $('#errorCompanyAnniversary').html("Year should be a number.");
            }else if($('#year').val().length === 4 && !yearRegex.test($('#year').val())){
                $('#year').css('border-color', 'red');
                $('#errorCompanyAnniversary').css('display', 'block');
                $('#errorCompanyAnniversary').html("Invalid Year Range");
            }else { 
                $('#year').css('border-color', '#d2d6de');
                $('#errorCompanyAnniversary').css('display', 'none');
              }
	});
        
     $('#month').change(function(){
        if($('#month :selected').val() === ""){
            $('#errorMonth').css('display', 'block');
            $('#errorMonth').html("Select a Month");
        }else { 
            $('#month').css('border-color', '#d2d6de');
            
        }
     });       
    
    

    $('#companyName').keyup(function(){
         if($('#companyName').val() === ""){
            $('#errorCompanyName').css('display', 'block');
            $('#errorCompanyName').html("Enter Company Name");
        }else { 
            $('#companyName').css('border-color', '#d2d6de');
            $('#errorCompanyName').css('display', 'none');
        }
    });
    
    $('#industry').change(function(){
        if($('#industry :selected').val() === ""){
            $('#industry').css('border-color', 'red');
            $('#industry').focus();
            $('#errorIndustry').css('display', 'block');
            $('#errorIndustry').html("Select Industry");
        }else { 
            $('#industry').css('border-color', '#d2d6de');
            $('#errorIndustry').css('display', 'none');
            if($('#industry :selected').val() === "OTHERS"){
                $('#divOthersIndustry').css('display', 'block');
            }
        }
    });
    
    $('#othersIndustry').change(function(){
        if($('#othersIndustry :selected').val() === "" && $('#industry :selected').val() === "OTHERS"){
            $('#othersIndustry').css('border-color', 'red');
            $('#errorOtherIndustry').css('display', 'block');
            $('#errorOtherIndustry').html("Select Other Industry");
        }else{
            $('#othersIndustry').css('border-color', '#d2d6de');
            $('#errorOtherIndustry').css('display', 'none');
        }
    });
    $('#numberOfEmployees').change(function(){
   
       if($('#numberOfEmployees  :selected').val() === ""){
            $('#numberOfEmployees').css('border-color', 'red');
            $('#errorNumberOfEmployees').css('display', 'block');
            $('#errorNumberOfEmployees').html("Select Number of Employees");
        }else { 
            $('#numberOfEmployees').css('border-color', '#d2d6de');
            $('#errorNumberOfEmployees').css('display', 'none');
        }
     });  
     
    $('#branch').change(function(){
   
       if($('#branch  :selected').val() === ""){
            $('#branch').css('border-color', 'red');
            $('#errorBranch').css('display', 'block');
            $('#errorBranch').html("Select a Branch");
        }else { 
            $('#branch').css('border-color', '#d2d6de');
            $('#errorBranch').css('display', 'none');
        }
     });  
     
     
     var fields = "#companyName, #numberOfEmployees, #industry, #othersIndustry, #branch";
    
    $(fields).on('change', function() {
        var flagCompanyName = false;
        var flagNumberOfEmployees = false;
        var flagIndustry = false;
        var flagOtherIndustry = true;
        var flagBranch = false;
        
        if($('#companyName').val() === ""){
            flagCompanyName  = false;
        }else { 
            flagCompanyName = true;
        }
        
        if($('#numberOfEmployees  :selected').val() === ""){
            flagNumberOfEmployees  = false;
        }else { 
            flagNumberOfEmployees = true;
        }
                
        if($('#industry :selected').val() === ""){
            flagIndustry = false;
        }else { 
            flagIndustry = true;
              if($('#industry :selected').val() === "OTHERS"){
                if($('#othersIndustry :selected').val() === "" && $('#industry :selected').val() === "OTHERS"){
                         flagOtherIndustry = false;
                    }else{
                        flagOtherIndustry = true;
                    }
            }else{
                  flagOtherIndustry = true;
            } 
        }
        
        if($('#branch  :selected').val() === ""){
            flagBranch  = false;
        }else { 
            flagBranch = true;
        }
    

       
        if (flagCompanyName === false || flagNumberOfEmployees === false || flagIndustry === false || 
            flagOtherIndustry === false || flagBranch === false) {
            $('#btnSubmit').prop('disabled', true);
            
        } else {
            $('#btnSubmit').removeAttr('disabled');
        }
        
         
    }); 
    
    $(document).ready(function(){
      $('.collapsible').collapsible();
    });
    
   
    $(document).ready(function() {
        $('select').material_select();
    });
              
</script> 

 
</html>
