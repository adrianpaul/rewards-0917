



<%@page import="Others.Constants"%>
<!DOCTYPE html>


<html>
<head>
        <% 
            
            if(session.getAttribute("isCheckedPrivacy") == null || session.getAttribute("isCheckedPrivacy").toString().equalsIgnoreCase("")){
                response.sendRedirect("index.jsp");
            }
            
        
            String isExisted = (String) session.getAttribute("isExisted");
            String errorType = (String) session.getAttribute("errorType");
            String gppChecked = (String) session.getAttribute("isCheckedPrivacy");
            System.out.println("gppChecked: "+gppChecked);
            System.out.println("isExisted: "+isExisted);
         
             
          
            
        %>
        <title><%=Constants.appName%> - Validate Identity</title>
	
	<META HTTP-EQUIV="refresh" CONTENT="1800; URL=sessionexpired.jsp"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="../assets/globeIcon.ico" />
         
        <link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.3/css/bootstrap.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.1.1/css/mdb.min.css">
	<link rel="stylesheet"  href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="application/javascript"  href="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/material.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/ripples.min.js">
	<link rel="text/html"  href="https://codepen.io/triss90/pen/VWWMWN">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.2.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <!-- Global site tag (gtag.js) - Google Analytics --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-HN82HHLXQ9"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-HN82HHLXQ9'); </script>
        <script src="https://www.google.com/recaptcha/api.js?render=6LeFZbwUAAAAANPgd5jL93oZUqWD9V379Hutamai"></script>
        <script>
//        grecaptcha.ready(function() {
//            grecaptcha.execute('6LeFZbwUAAAAAAQBznxuHsKBGuVDqCPT-yOg_-Tl', {action: 'homepage'}).then(function(token) {
//               
//            });
//        });
        </script>

   <script>
        if (location.protocol != 'https:') { 
            location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
        }
    </script>          
        
  
    <script>
    function callSuccess(){
        var mobileNumberRegex = /^[+0-9-]*$/;
			
        if(document.getElementById("accountNumber").value ===""){
                document.getElementById("errorAccountNumber").style.display="block";
                document.getElementById("errorAccountNumber").innerHTML="Enter Your Account Number";
                document.getElementById("accountNumber").style.borderColor="red";
                document.getElementById("accountNumber").focus();
                return false;
        }else if(document.getElementById("accountNumber").value.length<8){
                document.getElementById("errorAccountNumber").style.display="block";
                document.getElementById("errorAccountNumber").innerHTML="Number with 11 or 13 characters is allowed.";
                document.getElementById("accountNumber").style.borderColor="red";
                document.getElementById("accountNumber").focus();
                return false;
        }else{
                document.getElementById("errorAccountNumber").style.display="none";
                document.getElementById("accountNumber").style.borderColor="LightGray";
                
        }
          
        
        
    }
    
    function callSuccess2(){
        var mobileNumberRegex = /^[+0-9-]*$/;
			
        if(document.getElementById("mobile").value ===""){
                document.getElementById("errorMobileNumber").style.display="block";
                document.getElementById("errorMobileNumber").innerHTML="Enter Your Mobile Number";
                document.getElementById("mobile").style.borderColor="red";
                document.getElementById("mobile").focus();
                return false;
        }else if(!mobileNumberRegex.test(document.getElementById("accountNumber").value)){
                document.getElementById("errorMobileNumber").style.display="block";
                document.getElementById("errorMobileNumber").innerHTML="Only numeric characters allowed";
                document.getElementById("mobile").style.borderColor="red";
                document.getElementById("mobile").focus();
                return false;
        }else if(document.getElementById("mobile").value.length !== 11){
                document.getElementById("errorMobileNumber").style.display="block";
                document.getElementById("errorMobileNumber").innerHTML="Mobile number should be 11 characters only.";
                document.getElementById("mobile").style.borderColor="red";
                document.getElementById("mobile").focus();
                return false;
        }else{
                document.getElementById("errorMobileNumber").style.display="none";
                document.getElementById("mobile").style.borderColor="LightGray";
        }
    }
    
    
    function selectDiv1(){
        
        document.getElementById("title").innerHTML ="What's your Globe myBusiness number?"; 
        document.getElementById("btn1").style.color ="white"; 
        document.getElementById("btn2").style.color ="gray"; 
         document.getElementById("btn2").className="white chip"; 
        document.getElementById("btn1").className ="blue chip"; 
        
        
        document.getElementById("divMobile").style.display ="block"; 
        document.getElementById("divAccountNumber").style.display ="none"; 
        
    
    } 
    
    function selectDiv2(){
        
               
        document.getElementById("title").innerHTML ="What's your Account Number?"; 
        document.getElementById("btn2").style.color ="white"; 
        document.getElementById("btn1").style.color ="gray"; 
        document.getElementById("divMobile").style.display ="none"; 
        document.getElementById("btn1").className="white chip"; 
        document.getElementById("btn2").className ="blue chip"; 
        
        document.getElementById("divAccountNumber").style.display ="block"; 
    
    } 


    function enabledButton(){      
        if(document.getElementById("mobile").value === ""){
            document.getElementById("btnSubmit1").disabled= true;
        }else{
            document.getElementById("btnSubmit1").disabled= false;
        }

        if(document.getElementById("accountNumber").value === ""){
           document.getElementById("btnSubmit2").disabled = true;
        }else{
            document.getElementById("btnSubmit2").disabled= false;
        }

       
    }
 
</script> 

  


    <script src="https://www.google.com/recaptcha/api.js"></script>
    
    <style>
        .modal { 

                max-height: 100%;
                margin-top: -30px;
                overflow: auto;
        }
        .grecaptcha-badge { 
            top:15px !important;         
            right:-15px; 

        }
        

        
        .rc-anchor-light, .rc-anchor-light { 
            background: #ffffff00 !important; 
            color: #ffffff00 !important; 
            border: 1px solid #ffffff00 !important;
        }
        
        
        .rc-anchor {
            border-radius: 3px;
            box-shadow: 0 0 4px 1px rgba(15, 14, 14, 0.14);
            -webkit-box-shadow: 0 0 4px 1px rgba(0, 0, 0, 0.11);
            -moz-box-shadow: 0 0 4px 1px rgba(0,0,0,0.08);
        }
        
       

        
    </style>  
</head>



<body onLoad="enabledButton()">
    <div class="row" >
           
        <div class="col s12 col m12 col l12" style="text-align: center;padding-top: 20px;">
           <img class="responsive-img" src="../assets/illustrations/1x/phonenumbermdpi1.png"/> 
          <h4 class="h4-responsive" id="title"> What's your Globe myBusiness number?</h4>
           <% if (session.getAttribute("isExisted") != null) {
               if (isExisted.equals("false")){
                   if (session.getAttribute("errorType") != null && errorType.equalsIgnoreCase("accountNumber")){%>
                       <div  style="color:red;">
                          <!--<h5>Error!</h5>-->
                           <p style="text-align: center;font-size: 15px;">Account Number Doesn't exist.</p>
                       </div>
                   <% }else{%>
                       <div  style="color:red;">
                           <!--<h5>Error!</h5>-->
                           <p style="text-align: center;font-size: 15px;">Mobile Number Doesn't exist.</p>
                       </div>
                   <%}
                   }
                   session.removeAttribute("isExisted"); %>
           <% } %>
       </div>

   </div>
   <div class="row ">
        <div style="font-size: 20px;text-align: center;" class="col s12 col m12 col l12">  
            <a class="blue chip" onClick="selectDiv1();" id="btn1" style="color:white;">Mobile Number</a>
            <a  class="white chip" onClick="selectDiv2();" id="btn2" style="color:gray;">Account Number</a>  
        </div>

    </div>

   <div class="row">
       <div class="col s12 col m12">
           <div id="modal1" class="modal modal-fixed-footer">
                 <div class="modal-content">
                   <label style="color:black;font-size: 18px;">Account Number</label>
                   <img class="responsive-img" src="../assets/billscreenshot.png" style="max-width: 100%;height: auto;">

                 </div>
                   <div class="modal-footer">
                       <a href=../"assets/billscreenshot.png" class="modal-action modal-close waves-effect waves-green btn-flat " download><i class="material-icons">file_download</i></a>
                       <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Return</a>
                   </div>
           </div>

       </div>   
   </div>   
   <div class="row" >
     
    <div id="divMobile" class="col s12 col m12">
        <form action="../SendOTPMobile2" method="POST">  

            <div class="col s12 offset-l3 col l6" >
                <input type="text" required id="mobile" name="mobile" class="form-control" style="margin-top: 10px;">
                <label style="font-size: 15px;">Enter your 11-digit mobile number with this format (09xxxxxxxxx)</label>
                <span id="errorMobileNumber" style="color:red;display:none;font-size:15px;margin-top: -5px;">Enter Account Number</span>


            </div>
            <div class="col s12 offset-m3 m6 offset-l5 l2">
                <button id="btnSubmit1" class="waves-light btn"  onClick="return callSuccess2();"   style="width: 100%;color:white;background-color: #3385ff;" disabled>CONTINUE</button> 

            </div>        
        </form>
    </div>    

    <div id="divAccountNumber" style="display: none;" class="col s12 col m12" >
        <form action="../AccountNumber2" method="POST">   
            <div class="col s12 offset-l3 col l6" >
                <input type="text" required id="accountNumber" name="accountNumber" class="form-control" style="margin-top: 10px;">
                <label style="font-size: 15px;">Enter your Account Number</label>
                <a class="modal-trigger" href="#modal1">Where can I find this?</a>
                <span id="errorAccountNumber" style="color:red;display:none;font-size:15px;margin-top: -5px;">Enter Account Number</span>
                <br>
                
            </div>
            <div class="col s12 m12 l12">
                <div style="text-align: center; margin-top: 15px;">
                     <div class="g-recaptcha" style="display: inline-block;" data-callback="myCallback" data-expired-callback="expCallback" 
			data-sitekey="6Le3Z7wUAAAAACd68ltOvqSDOQ9YnJKRQAI2yb36"></div>
                </div>
            </div>

          
            <div class="col s12 offset-m3 m6 offset-l5 l2">
                <button id="btnSubmit2" class="waves-light btn"  onClick="return callSuccess();"   style="width: 100%;color:white;background-color: #3385ff;" disabled>CONTINUE</button> 
            </div>        
        </form>
        
        <div class="col s12 offset-l3 col l6" >
          
        </div>
    </div>  

  

   </div>  
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=cea87e7d-969e-47a5-b787-61b8ff7dadfa"> </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>
    <!-- End of globemybusiness Zendesk Widget script -->   
</body>

<script>
    
    var expCallback = function() {
        grecaptcha.reset();
        $('#accountNumber').css('border-color', 'red');
        $('#accountNumber').focus();
        $('#errorAccountNumber').css('display', 'block');
        $('#errorAccountNumber').html("reCaptcha expired");
        $('#btnSubmit2').prop('disabled', true);
        console.log("reCaptcha Inc: " + response.length);
   };
   
    var myCallback = function enableAccountNumberSubmit(){      
          var regex = /^[+0-9-]*$/;
                
                
             
        if($('#accountNumber').val() === ""){
		 
            $('#accountNumber').css('border-color', 'red');
            $('#accountNumber').focus();
            $('#errorAccountNumber').css('display', 'block');
            $('#errorAccountNumber').html("Invalid Format");
            $('#btnSubmit2').prop('disabled', true);
            console.log("no account number");
            
        }else if($('#accountNumber').val().length<8){
            
            $('#accountNumber').css('border-color', 'red');
            $('#accountNumber').focus();
            $('#errorAccountNumber').css('display', 'block');
            $('#errorAccountNumber').html("Minimum of  8 characters are allowed.");
            $('#btnSubmit2').prop('disabled', true);
            console.log("min 8");
            
        }else { 
            var response = grecaptcha.getResponse();

            if(response.length > 0){
                console.log("reponse len: " + response.length);
                $('#accountNumber').css('border-color', '#d2d6de');
                $('#errorAccountNumber').css('display', 'none');
                $('#btnSubmit2').removeAttr('disabled');
            }else{
                $('#accountNumber').css('border-color', 'red');
                $('#accountNumber').focus();
                $('#errorAccountNumber').css('display', 'block');
                $('#errorAccountNumber').html("Please finish reCaptcha");
                $('#btnSubmit2').prop('disabled', true);
                console.log("reCaptcha Inc: " + response.length);
            }

        }
      
    }
    
    window.myCallback = myCallback;
                        
    $('#accountNumber').on('keyup', function () {
      
        if($('#accountNumber').val() === ""){
		 
            $('#accountNumber').css('border-color', 'red');
            $('#accountNumber').focus();
            $('#errorAccountNumber').css('display', 'block');
            $('#errorAccountNumber').html("Invalid Format");
            $('#btnSubmit2').prop('disabled', true);
            console.log("no account number");
            
        }else if($('#accountNumber').val().length<8){
            
            $('#accountNumber').css('border-color', 'red');
            $('#accountNumber').focus();
            $('#errorAccountNumber').css('display', 'block');
            $('#errorAccountNumber').html("Minimum of  8 characters are allowed.");
            $('#btnSubmit2').prop('disabled', true);
            console.log("min 8");
            
        }else { 
            var response = grecaptcha.getResponse();

            if(response.length > 0){
                console.log("reponse len: " + response.length);
                $('#accountNumber').css('border-color', '#d2d6de');
                $('#errorAccountNumber').css('display', 'none');
                $('#btnSubmit2').removeAttr('disabled');
            }else{
                $('#accountNumber').css('border-color', 'red');
                $('#accountNumber').focus();
                $('#errorAccountNumber').css('display', 'block');
                $('#errorAccountNumber').html("Please finish reCaptcha");
                $('#btnSubmit2').prop('disabled', true);
                console.log("reCaptcha Inc: " + response.length);
            }

        }
        
    });
        
        
    $('#mobile').on('keyup', function () {
        var regex = /^[+0-9-]*$/;
             
        if($('#mobile').val() === ""){
		 
            $('#mobile').css('border-color', 'red');
            $('#mobile').focus();
            $('#errorMobileNumber').css('display', 'block');
            $('#errorMobileNumber').html("Invalid Format");
            $('#btnSubmit1').prop('disabled', true);
        }else if(!regex.test($('#mobile').val())){
            $('#mobile').css('border-color', 'red');
            $('#mobile').focus();
            $('#errorMobileNumber').css('display', 'block');
            $('#errorMobileNumber').html("Only numeric characters are allowed.");
            $('#btnSubmit1').prop('disabled', true);
        }else if($('#mobile').val().length !==11){
            $('#mobile').css('border-color', 'red');
            $('#mobile').focus();
            $('#errorMobileNumber').css('display', 'block');
            $('#errorMobileNumber').html("Mobile number should be 11 characters only.");
            $('#btnSubmit1').prop('disabled', true);
        }else { 
            $('#mobile').css('border-color', '#d2d6de');
            $('#errorMobileNumber').css('display', 'none');
            $('#btnSubmit1').removeAttr('disabled');
	  }
	});
        
        
       $('#select1').on('change', function () { 
            if($('#select1').val() === "financialAccount"){
                document.getElementById("divFinancial").style.display ="block";
                document.getElementById("divMobile").style.display ="none";

            }

            if($('#select1').val() === "mobileNumber"){
                document.getElementById("divFinancial").style.display ="none";
                document.getElementById("divMobile").style.display ="block";

            }
       	}); 
        if($('#select1').val() === "financialAccount"){
            document.getElementById("divFinancial").style.display ="block";
            document.getElementById("divMobile").style.display ="none";

        }

        if($('#select1').val() === "mobileNumber"){
            document.getElementById("divFinancial").style.display ="none";
            document.getElementById("divMobile").style.display ="block";
        }
        
        $('.modal').modal({
            dismissible: true, // Modal can be dismissed by clicking outside of the modal
            opacity: .5 // Opacity of modal background
           
            
        });
      

</script> 

 
</html>
