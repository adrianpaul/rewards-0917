<%-- 
    Document   : verificationCode
    Created on : 05 31, 19, 2:11:55 PM
    Author     : Adrian Paul
--%>

<%@page import="java.net.URISyntaxException"%>
<%@page import="HTTPManagement.HTTPManagerDirectory"%>
<%@page import="Others.Constants"%>
<%@page import="java.net.URI"%>
<!DOCTYPE html>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
   
        
      <%
             String folderName = Constants.folderName;
           if(session.getAttribute("successAdd") == null || !session.getAttribute("successAdd").toString().equalsIgnoreCase("Type1")){
                response.sendRedirect("index.jsp");
            }
     
          session.setAttribute("isExisted", "");
     
         
    
      %>
        <META HTTP-EQUIV="refresh" CONTENT="<%=session.getMaxInactiveInterval() %>; URL=sessionexpired.jsp"/>
        
        <title><%=Constants.appName%> - Successfully Added - Mobile</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="../assets/globeIcon.ico" />
         
        <link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.3/css/bootstrap.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.1.1/css/mdb.min.css">
	<link rel="stylesheet"  href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="application/javascript"  href="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/material.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/ripples.min.js">
	<link rel="text/html"  href="https://codepen.io/triss90/pen/VWWMWN">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.2.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <!-- Global site tag (gtag.js) - Google Analytics --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-HN82HHLXQ9"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-HN82HHLXQ9'); </script>
  <script>
        if (location.protocol != 'https:') { 
            location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
        }
    </script>          
        
</head>



<body>


    <div class="row" >
        <div class="col s12 col m12 " style="text-align: center;">
            <img class="responsive-img"  src="../assets/illustrations/1x/congratsmdpi1.png"/>  
            <h4 style="color:#63acd9;margin-top: -30px;"> Congratulations!</h4>

        </div>

    </div>
    <div class="row">

        <div class="col s12 col m12" style="text-align:center;">
            <div class="col s12 offset-m3 col m6" style="text-align:center;">
                <p style="color: black;font-size: 20px;">
                      Thank you so much for being a loyal Globe myBusiness subscriber. Please expect your treat to be sent in your email within the day. We hope you enjoy!
                </p>
                
                
               
            </div>
            <div class="col s12  offset-m5 col m2 ">
                    <a href="https://mybusinessacademy.ph/" class="waves-light btn" style="width: 100%;color:white;background-color: #009fdf;" >CLOSE TAB</a> 

            </div> 

        </div>    

    </div> 

    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=cea87e7d-969e-47a5-b787-61b8ff7dadfa"> </script>
    <!-- End of globemybusiness Zendesk Widget script -->   
</body>
   
</html>
