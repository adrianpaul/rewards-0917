<%-- 
    Document   : verificationCode
    Created on : 05 31, 19, 2:11:55 PM
    Author     : Adrian Paul
--%>

<%@page import="Others.Constants"%>
<!DOCTYPE html>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
        <%
            if(session.getAttribute("successAdd").toString().equalsIgnoreCase("false")){
                 session.setAttribute("isExisted", "");
            }
        %>
          <META HTTP-EQUIV="refresh" CONTENT="<%=session.getMaxInactiveInterval() %>; URL=/0917/sessionexpired.jsp"/>
      
     <title><%=Constants.appName%> - Already Claimed</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="assets/globeIcon.ico" />
         
        <link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.3/css/bootstrap.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.1.1/css/mdb.min.css">
	<link rel="stylesheet"  href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="application/javascript"  href="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/material.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/ripples.min.js">
	<link rel="text/html"  href="https://codepen.io/triss90/pen/VWWMWN">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.2.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <!-- Global site tag (gtag.js) - Google Analytics --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-HN82HHLXQ9"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-HN82HHLXQ9'); </script>
   
</head>



<body>
    <div class="row" >
         <div class="col s12 col m12" style="text-align: center;padding-top: 20px;">
             <img class="responsive-img" src="assets/front-banner.png"/>   
         </div>

     </div>
     <div class="row">
         <div class="col s12 col m12">
            <div class="col s12 offset-m3  col m6" style="text-align:center;">
                 <p style="color: black;font-size: 20px;">
                     You have already claim your rewards.
                 </p>

            </div>  
            <div class="col s12  offset-m5 col m2">
                <a href="index.jsp" class="waves-light btn" style="width: 100%;color:white;background-color: #009fdf;" >BACK TO HOME</a> 
            </div> 
  

         </div>  
     </div> 

    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=cea87e7d-969e-47a5-b787-61b8ff7dadfa"> </script>
    <!-- End of globemybusiness Zendesk Widget script -->   
</body>
   
</html>