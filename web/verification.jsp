



<%@page import="Others.Constants"%>
<!DOCTYPE html>


<html>
<head>
            <%
            if(session.getAttribute("isExisted") == null  || session.getAttribute("isExisted").toString().equalsIgnoreCase("false") || session.getAttribute("type").equals("")){
                response.sendRedirect("index.jsp");
            }
            
            String emailAddress = (String) session.getAttribute("emailAddress");
            String mobileNumber = (String) session.getAttribute("mobileNumber");
            String otpMobile = (String) session.getAttribute("mobile");
//            System.out.println("otpMobile: "+otpMobile);
            String type = (String) session.getAttribute("type");
            String verificationCode = (String) session.getAttribute("verificationCode");
            String codeMatches = (String) session.getAttribute("codeMatches");
            String errorType = (String) session.getAttribute("errorType");
            String userType = (String) session.getAttribute("userType");
            String isVerified = (String) session.getAttribute("isVerified");
            String tries = (String) session.getAttribute("tries");
            System.out.println("errorType: "+errorType);
            System.out.println("tries "+tries);
            System.out.println("type: "+type);
            
        %>
  <META HTTP-EQUIV="refresh" CONTENT="<%=session.getMaxInactiveInterval() %>; URL=/0917/sessionexpired.jsp"/>
      
       <title><%=Constants.appName%> - Verify Through OTP</title>
	
	<link rel="icon" type="image/png" href="assets/globeIcon.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
       
         
        <link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.3/css/bootstrap.min.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.1.1/css/mdb.min.css">
	<link rel="stylesheet"  href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="application/javascript"  href="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/material.min.js">
	<link rel="application/javascript"  href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/ripples.min.js">
	<link rel="text/html"  href="https://codepen.io/triss90/pen/VWWMWN">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.2.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <!-- Global site tag (gtag.js) - Google Analytics --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-HN82HHLXQ9"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-HN82HHLXQ9'); </script>

        
<script>
    function callSuccess(){
        var errorVerificationCode  = "false";
        var mobileNumberRegex = /^[+0-9-]*$/;
			
        if(document.getElementById("verificationCode").value ===""){
                document.getElementById("errorVerificationCode").style.display="block";
                document.getElementById("errorVerificationCode").innerHTML="Enter Verification Code";
                document.getElementById("verificationCode").style.borderColor="red";
                document.getElementById("verificationCode").focus();
                return false;
        }else if(!mobileNumberRegex.test(document.getElementById("verificationCode").value)){
                document.getElementById("errorVerificationCode").style.display="block";
                document.getElementById("errorVerificationCode").innerHTML="Only numeric characters allowed";
                document.getElementById("verificationCode").style.borderColor="red";
                document.getElementById("verificationCode").focus();
                return false;
        }else if(document.getElementById("verificationCode").value.length<4){
                document.getElementById("errorVerificationCode").style.display="block";
                document.getElementById("errorVerificationCode").innerHTML="Verification Code length  is 4.";
                document.getElementById("verificationCode").style.borderColor="red";
                document.getElementById("verificationCode").focus();
                return false;
        }else{
                document.getElementById("errorMobileNumber").style.display="none";
                document.getElementById("verificationCode").style.borderColor="LightGray";
               
        }
          
    
        
    }
</script>   

</head>



<body>
    
  
        
    <div class="row" >
        <div class="col s12 col m12 col l12" style="text-align: center;padding-top: 20px;">
            <img class="responsive-img" src="assets/illustrations/1x/phonenumbermdpi1.png"/>   
        </div>

    </div>
    <div class="row" >
        <div class="col s12 col m12 col l12" style="text-align:center;">

        <h4 class="h4-responsive"  style="font-size: 25px;">Enter the 4-digit OTP</h4>

        <% if (session.getAttribute("type") != null  && session.getAttribute("errorType") != null) {
            if (type.equals("sms") && errorType.equals("")){%>
                <h5 style="font-size: 18px;">We sent a 4-digit verification code to your mobile number
                <b><%=mobileNumber%></b></h5>
                <form action="ResendVerificationCode" method="POST">
                    <button style="background: none!important;border: none;padding: 0!important;font-family: arial, sans-serif;color: #069;text-decoration: underline;cursor: pointer;">Resend Code</button>
                </form>
            <%}

            if (type.equals("resendSMS") && errorType.equals("")){%>
                <h5 style="font-size: 18px;">We resend  a  new 4-digit verification code to your mobile number
                    <b><%=mobileNumber%></b></h5>
                <form action="ResendVerificationCode" method="POST">
                    <button style="background: none!important;border: none;padding: 0!important;font-family: arial, sans-serif;color: #069;text-decoration: underline;cursor: pointer;">Resend Code</button>
                </form>    
            <%}
             if (type.equals("sentSMS") && errorType.equals("")){%>
                <h5  style="font-size: 20px;">We already sent a 4-digit verification code to your mobile number
                 <b><%=mobileNumber%></b></h5>
                <form action="ResendVerificationCode" method="POST">
                    <button style="background: none!important;border: none;padding: 0!important;font-family: arial, sans-serif;color: #069;text-decoration: underline;cursor: pointer;">Resend Code</button>
                </form> 
            <%}  
             if (type.equals("verified") && errorType.equals("")){%>
                <h5  style="color:green;font-size: 20px;font-weight: bold;">You are already a Verified Account!</h5>
            <%}  

            %>     
        <% } %>
            <% if (session.getAttribute("isVerified") != null) {
                if (!isVerified.equals("true")){%>

                    <% if(session.getAttribute("codeMatches") != null && session.getAttribute("tries") != null) {
                        if (codeMatches.equals("false")) {%>
                                <%if (tries.equals("2")) {%>
                                    <p style="font-size: 20px;color:red;">Your input does not match to the Verification Code we sent to you. You only have <%=tries%> tries to validate</p>
                                    <form action="ResendVerificationCode" method="POST">
                                        <button style="background: none!important;border: none;padding: 0!important;font-family: arial, sans-serif;color: #069;text-decoration: underline;cursor: pointer;">Resend Code</button>
                                    </form>
                                <% }else if (tries.equals("1")) {%>
                                    <p style="font-size: 20px;color:red;">Your input does not match to the Verification Code we sent to you. You only have <%=tries%> tries to validate</p>
                                    <form action="ResendVerificationCode" method="POST">
                                        <button style="background: none!important;border: none;padding: 0!important;font-family: arial, sans-serif;color: #069;text-decoration: underline;cursor: pointer;">Resend Code</button>
                                    </form>
                                <% }else {%>
                                    <h5 style="font-size: 20px; color:red;">The 4-digit verification code we have sent to your mobile number <b><%=mobileNumber%></b> has expired. 3 attempts only.
                                    </h5>
                                    <form action="ResendVerificationCode" method="POST">
                                        <button style="background: none!important;border: none;padding: 0!important;font-family: arial, sans-serif;color: #069;text-decoration: underline;cursor: pointer;">Resend Code</button>
                                    </form>


                                <% } %>

                       <% } %>
                <% }
            }
        }%>





        </div>
    </div>    

    <div class="row" >
        <div class="col s12 col m12 col l12">


            <form action="VerificationCode" method="POST">   
                <div class="md-form col s12 offset-l3 col l6">
                        <input type="text" required id="verificationCode" name="code" class="form-control" maxlength="4">
                        <label for="verificationCode" class="">Your answer</label>
                        <span id="errorVerificationCode" style="color:red;display:none;margin-left:12px;font-size:15px;">Enter Account Number</span>
                </div>
                <div class="col s12 offset-m3 m6 offset-l5 l2">

                    <button id="btnSubmit" class="waves-light btn"  onClick="return callSuccess();" style="width: 100%;color:white;background-color: #3385ff;" disabled="">NEXT</button> 
                </div>
            </form>      



        </div>
    </div>    
                

    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=cea87e7d-969e-47a5-b787-61b8ff7dadfa"> </script>
    <!-- End of globemybusiness Zendesk Widget script -->   
</body>
<script>
    $('#verificationCode').on('keyup', function () {
		var regex = /^[+0-9]*$/;
             
        if($('#verificationCode').val() === ""){
		 
            $('#verificationCode').css('border-color', 'red');
            $('#verificationCode').focus();
            $('#errorVerificationCode').css('display', 'block');
            $('#errorVerificationCode').html("Enter Verification Code");
            $('#btnSubmit').prop('disabled', true);
		
        }else if(!regex.test($('#verificationCode').val())){
            $('#verificationCode').css('border-color', 'red');
            $('#verificationCode').focus();
            $('#errorVerificationCode').css('display', 'block');
            $('#errorVerificationCode').html("Only numeric characters allowed.");
             $('#btnSubmit').prop('disabled', true);
        }else if($('#verificationCode').val().length<4){
            $('#verificationCode').css('border-color', 'red');
            $('#mobileNumber').focus();
            $('#errorVerificationCode').css('display', 'block');
            $('#errorVerificationCode').html("Verification Code length  is 4.");
             $('#btnSubmit').prop('disabled', true);
        }else { 
            $('#verificationCode').css('border-color', '#d2d6de');
            $('#errorVerificationCode').css('display', 'none');
            $('#btnSubmit').removeAttr('disabled');
	  }
	});
     

</script>    

</html>
