/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author Adrian Paul
 */
public class FinancialAccount {
    
    String accountIdFk;
    String id;
    String financialAccountId;
        
    public FinancialAccount(String accountIdFk,String id,String financialAccountId) {
        this.accountIdFk = accountIdFk;
        this.id = id;
        this.financialAccountId = financialAccountId;
  
    }

    public FinancialAccount() {
       
    }
    
    public String getAccountIdFk() {
        return accountIdFk;
    }

    public void setAccountIdFk(String accountIdFk) {
        this.accountIdFk = accountIdFk;
    }
        
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getFinancialAccountId() {
        return financialAccountId;
    }

    public void setFinancialAccountId(String financialAccountId) {
        this.financialAccountId = financialAccountId;
    }

    
   
}
