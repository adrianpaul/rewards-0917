/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author sgmwpsupport
 */
public class CityTown {
    private String id;
    private String name;
    private String shortCode;
    private String areaCode;
    private String provinceIdFk;

    public CityTown() {
    }

    public CityTown(String id, String name, String shortCode, String areaCode, String provinceIdFk) {
        this.id = id;
        this.name = name;
        this.shortCode = shortCode;
        this.areaCode = areaCode;
        this.provinceIdFk = provinceIdFk;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getProvinceIdFk() {
        return provinceIdFk;
    }

    public void setProvinceIdFk(String provinceIdFk) {
        this.provinceIdFk = provinceIdFk;
    }
}
