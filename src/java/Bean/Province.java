/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author sgmwpsupport
 */
public class Province {
    private String id;
    private String name;
    private String shortCode;
    private String regionIdFk;

    public Province() {
    }

    public Province(String id, String name, String shortCode, String regionIdFk) {
        this.id = id;
        this.name = name;
        this.shortCode = shortCode;
        this.regionIdFk = regionIdFk;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getRegionIdFk() {
        return regionIdFk;
    }

    public void setRegionIdFk(String regionIdFk) {
        this.regionIdFk = regionIdFk;
    }

}
