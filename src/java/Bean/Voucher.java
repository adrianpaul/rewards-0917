/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author Adrian Paul
 */
public class Voucher {
    String id;
    String category;
    String code;
    String counter;
    String description;
    String endDate; 
    String byGroup;
    String isActive;
    String lastUpdate;
    String name;
    String startDate;
    String amount;
    String mobileNumber;
    String email;
    String accountContactIdFk;
    String voucherCode;
    String status;
        
    public Voucher() {
     
    }

    public Voucher(String id, String category, String code, String counter, String description,
                  String endDate, String byGroup, String isActive, String lastUpdate, String name,
                  String startDate, String amount) {
       this.id = id;
       this.category = category;
       this.code = code;
       this.counter = counter;
       this.description = description;
       this.endDate = endDate;
       this.byGroup = byGroup;
       this.isActive = isActive;
       this.lastUpdate = lastUpdate;
       this.name = name;
       this.startDate = startDate;
       this.amount = amount;
    }
    
    
     public Voucher(String id, String accountContactIdFk, String email, String mobileNumber, String voucherCode, String status) {
       this.id = id;
       this.accountContactIdFk = accountContactIdFk;
       this.email = email;
       this.mobileNumber = mobileNumber;
       this.voucherCode = voucherCode;
       this.status = status;
    }
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    
      
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    
        
    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }
    
            
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
    
    public String getByGroup() {
        return byGroup;
    }

    public void setByGroup(String byGroup) {
        this.byGroup = byGroup;
    }
    
    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
    
    
    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }      
    
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }      
    
    
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }  
   
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }  
    
    
    public String getAccountContactIdFk() {
        return accountContactIdFk;
    }

    public void setAccountContactIdFk(String accountContactIdFk) {
        this.accountContactIdFk = accountContactIdFk;
    }  
    
    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = accountContactIdFk;
    }  
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }  
}
