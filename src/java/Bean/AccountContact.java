 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author sgmwpsupport
 */
public class AccountContact implements Serializable{
    private String id;
    private String firstName;
    private String lastName;
    private String middleName;
    private String nickName;
    private String email;
    private String mobileNumber;
    private String telNumber;
  
    private String lastUpdate;  
    private String sex;
    private String civilStatus;
    private String spouseName;
    private String positionInTheCompany;
    private String tin;
    private String mothersMaidenName;
    private String spouseContactNum;
    private String numberOfChildren;
    private String education;
    private String lastSchoolAttended;
    private String accountContactRoleIdFk;
    private String status;
    private String accountIdFk;
    private String birthdate;
    private Date dateBirthDate;
    private String imageLocation;
    private String imageName;
    private String type;
    private String accountContactIdFk;
    private String department;
    private String age;
    private String inChargeOf;
    private String companyName;
    private String companyAddress;
    private String numberOfEmployees;
    private String industry;
    private String companyAnniversary;
    private String branch;
    private String topicsInterestedIn;
    private String gppChecked;
    private String gtcChecked;
    private String businessImprovement;
    private String financialAccountIdFk;
    private String isUpdated;
    private String rewardType;
    private String metaData;
    private String denomId;
    private String otpNumber;
    private String kycCampaignIdFk;
    
    
                    
    public AccountContact() {
    }

   
  
    
    

    public AccountContact(String id, String firstName, String lastName, String middleName, String nickName, String email, String mobileNumber,
                          String telNumber, String sex, String spouseName, String positionInTheCompany, String tin, String mothersMaidenName,
                          String spouseContactNum, String numberOfChildren, String education, String lastSchoolAttended, String accountContactRoleIdFk,
                          String status, String accountIdFk, String birthdate, String imageLocation, String imageName, String type, String lastUpdate , String age, String department, 
                          String companyAnniversary, String inChargeOf, String branch, String companyAddress, String companyName, String  industry, String numberOfEmployees
                          ,String topicsInterestedIn, String gppChecked, String gtcChecked, String businessImprovement, String financialAccountIdFk, String isUpdated,
                          String rewardType, String metaData, String kycCampaignIdFk) {
          
       
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.nickName = nickName;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.telNumber = telNumber;
        this.sex = sex;
        this.spouseName = spouseName;
        this.positionInTheCompany = positionInTheCompany;
        this.tin = tin;
        this.mothersMaidenName = mothersMaidenName;
        this.spouseContactNum = spouseContactNum;
        this.numberOfChildren = numberOfChildren;
        this.education = education;
        this.lastSchoolAttended = lastSchoolAttended;
        this.accountContactRoleIdFk = accountContactRoleIdFk;
        this.status = status;
        this.accountIdFk = accountIdFk;
        this.birthdate = birthdate;
        this.imageLocation = imageLocation;
        this.imageName = imageName;
        this.type = type;
        this.age = age;
        this.department = department;
        this.companyAnniversary =companyAnniversary;
        this.inChargeOf = inChargeOf;
        this.branch = branch;
        this.companyAddress = companyAddress;
        this.companyName = companyName;
        this.industry = industry;
        this.numberOfEmployees =  numberOfEmployees;
        this.topicsInterestedIn = topicsInterestedIn;
        this.gppChecked = gppChecked;
        this.gtcChecked = gtcChecked;
        this.businessImprovement = businessImprovement;
        this.financialAccountIdFk = financialAccountIdFk;
        this.isUpdated = isUpdated;
        this.rewardType = rewardType;
        this.metaData = metaData;
        this.kycCampaignIdFk = kycCampaignIdFk;
        
    }

        public AccountContact(String accountContactIdFk,String accountContactRoleIdFk, String accountIdFk, String firstName, String lastName,String email, String mobileNumber, 
            String sex,String positionInTheCompany, String birthdate,String department, String type,String  companyAnniversary, 
            String inChargeOf, String branch,String companyName, String industry, String numberOfEmployees
            ,String topicsInterestedIn, String gppChecked, String businessImprovement, String financialAccountIdFk, String isUpdated, String rewardType, String metaData
            ,String denomId, String otpNumber, String kycCampaignIdFk) {
       
        this.accountContactIdFk = accountContactIdFk;
        this.accountContactRoleIdFk = accountContactRoleIdFk;
        this.accountIdFk = accountIdFk;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.sex = sex;
        this.positionInTheCompany = positionInTheCompany;
        this.birthdate = birthdate;
        this.department = department;
        this.type = type;
        this.companyAnniversary =companyAnniversary;
        this.inChargeOf = inChargeOf;
        this.branch = branch;
        this.companyName = companyName;
        this.industry = industry;
        this.numberOfEmployees =  numberOfEmployees;
        this.topicsInterestedIn = topicsInterestedIn;
        this.gppChecked = gppChecked;
        this.businessImprovement = businessImprovement;
        this.financialAccountIdFk = financialAccountIdFk;
        this.isUpdated = isUpdated;
        this.rewardType = rewardType;
        this.metaData = metaData;
        this.denomId = denomId;
        this.otpNumber = otpNumber;
        this.kycCampaignIdFk = kycCampaignIdFk;
    }



    public AccountContact(String accountContactIdFk,String accountContactRoleIdFk, String accountIdFk, String firstName, String lastName,
            String email, String mobileNumber, String telNum, String sex,String positionInTheCompany,String birthdate, String types,
            String department ,String inChargeOf,String topicsInterestedIn, String gppChecked, String financialAccountIdFk, String isUpdated, 
            String rewardType, String metaData, String otpNumber, String kycCampaignIdFk) {
       
        this.accountContactIdFk = accountContactIdFk;
        this.accountContactRoleIdFk = accountContactRoleIdFk;
        this.accountIdFk = accountIdFk;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.telNumber = telNum;
        this.sex = sex;
        this.positionInTheCompany = positionInTheCompany;
        this.birthdate = birthdate;
        this.type = types;
        this.department = department;
        this.gppChecked = gppChecked;
        this.topicsInterestedIn = topicsInterestedIn;
        this.inChargeOf = inChargeOf;
        this.financialAccountIdFk = financialAccountIdFk;
        this.isUpdated = isUpdated;
        this.rewardType = rewardType;
        this.metaData = metaData;
        this.otpNumber = otpNumber;
        this.kycCampaignIdFk = kycCampaignIdFk;
    }

    
    
    public String getFinancialAccountIdFk() {
        return financialAccountIdFk;
    }

    public void setFinancialAccountIdFk(String financialAccountIdFk) {
        this.id = financialAccountIdFk;
    }
    
    
    public String getIsUpdated() {
        return isUpdated;
    }

    public void setIsUpdated(String isUpdated) {
        this.id = isUpdated;
    }
    
    public String getAccountContactIdFk() {
        return accountContactIdFk;
    }

    public void setAccountContactIdFk(String accountContactIdFk) {
        this.id = accountContactIdFk;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getNickname() {
           return nickName;
       }

    public void setNickname(String nickName) {
        this.nickName = nickName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
   
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

  

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }
    
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
    
    public String getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(String civilStatus) {
        this.civilStatus = civilStatus;
    }
    
    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }
    
    public String getPositionInTheCompany() {
        return positionInTheCompany;
    }

    public void setPositionInTheCompany(String positionInTheCompany) {
        this.positionInTheCompany = positionInTheCompany;
    }
    
    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }
    
    public String getMothersMaidenName() {
        return mothersMaidenName;
    }

    public void setMothersMaidenName(String mothersMaidenName) {
        this.mothersMaidenName = mothersMaidenName;
    }
    
    public String getSpouseContactNum() {
        return spouseContactNum;
    }

    public void setSpouseContactNum(String spouseContactNum) {
        this.spouseContactNum = spouseContactNum;
    }
    
    public String getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(String numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }
    
    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }
    
    public String getLastSchoolAttended() {
        return lastSchoolAttended;
    }

    public void setLastSchoolAttended(String lastSchoolAttended) {
        this.lastSchoolAttended = lastSchoolAttended;
    }
    
    public String getAccountContactRoleIdFk() {
        return accountContactRoleIdFk;
    }

    public void setAccountContactRoleIdFk(String accountContactRoleIdFk) {
        this.accountContactRoleIdFk = accountContactRoleIdFk;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getAccountIdFk() {
        return accountIdFk;
    }

    public void setAccountIdFk(String accountIdFk) {
        this.accountIdFk = accountIdFk;
    }
    
    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
    
    public String getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(String imageLocation) {
        this.imageLocation = imageLocation;
    }
    
    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
    
    
        
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
    
    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
    
    public String getInChargeOf() {
        return inChargeOf;
    }

    public void setInChargeOf(String inChargeOf) {
        this.inChargeOf = inChargeOf;
    }
  
    
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    
    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }
    
    public String getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(String numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }
    
    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }
    
    public String getCompanyAnniversary() {
        return companyAnniversary;
    }

    public void setCompanyAnniversary(String companyAnniversary) {
        this.companyAnniversary = companyAnniversary;
    }
    
    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
    
    public String getTopicsInterestedIn() {
        return topicsInterestedIn;
    }

    public void setTopicsInterestedIn(String topicsInterestedIn) {
        this.topicsInterestedIn = topicsInterestedIn;
    }
    
    public String getGppChecked() {
        return gppChecked;
    }

    public void setGppChecked(String gppChecked) {
        this.gppChecked = gppChecked;
    }
    
    public String getGtcChecked() {
        return gtcChecked;
    }

    public void setGtcChecked(String gtcChecked) {
        this.gppChecked = gtcChecked;
    }
    
    public String getBusinessImprovement() {
        return businessImprovement;
    }

    public void setBusinessImprovement(String businessImprovement) {
        this.businessImprovement = businessImprovement;
    }
     public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }
      
    public String getMetadata() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }
    
    public String getDenomId() {
        return denomId;
    }

    public void setDenomId(String denomId) {
        this.denomId = denomId;
    }
    
     public String getOtpNumber() {
        return otpNumber;
    }

    public void setOtpNumber(String otpNumber) {
        this.otpNumber = otpNumber;
    }
    
    
    public String getKycCampaignIdFk() {
        return kycCampaignIdFk;
    }

    public void setKycCampaignIdFk(String kycCampaignIdFk) {
        this.kycCampaignIdFk = kycCampaignIdFk;
    }
}
