/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author Adrian Paul
 */
public class Accounts {
    String companyName;
          
    public Accounts(String companyName) {
        this.companyName = companyName;
     
    }

    public Accounts() {
       
    }
    
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
   
}
