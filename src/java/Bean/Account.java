/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

/**
 *
 * @author Adrian Paul
 */
public class Account {
    
    String accountIdFk;
    String email;
    String mobileNumber;
    String telNumber;
    String accountContactIdFk;
        
    public Account(String accountIdFk,String email,String mobileNumber,String telNumber, String accountContactIdFk) {
        this.accountIdFk = accountIdFk;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.telNumber = telNumber;
        this.accountContactIdFk = accountContactIdFk;
    }

    public Account() {
       
    }
    
    public String getAccountIdFk() {
        return accountIdFk;
    }

    public void setAccountIdFk(String accountIdFk) {
        this.accountIdFk = accountIdFk;
    }
        
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    
    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }
    
     public String getAccountContactIdFk() {
        return accountContactIdFk;
    }

    public void setAccountContactIdFk(String accountContactIdFk) {
        this.accountContactIdFk = accountContactIdFk;
    }
    
}
