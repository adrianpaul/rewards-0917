/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.AccountContact;
import Bean.Voucher;
import HTTPManagement.HTTPManagerDirectory;
import Others.Constants;
import Parser.AccountContactParser;
import Parser.VoucherParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import java.util.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;
import static javax.xml.transform.OutputKeys.ENCODING;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 *
 * @author Adrian Paul
 */
@WebServlet(name = "AddAccount", urlPatterns = {"/AddAccount"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 1,maxFileSize = 1024 * 1024 * 5,maxRequestSize = 1024 * 1024 * 5)
public class AddAccount extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
              
            String contentType = request.getContentType();
            
            /*try {
                List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                for (FileItem item : items) {
                    if (item.isFormField()) {
                        // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                        String fieldName = item.getFieldName();
                        String fieldValue = item.getString();
                        System.out.println("fieldName:" +fieldName);
                        System.out.println("fieldValue:" +fieldValue);
                        
                        // ... (do your job here)
                    } else {
                        // Process form file field (input type="file").
                        String fieldName = item.getFieldName();
                        String fileName = FilenameUtils.getName(item.getName());
                        InputStream fileContent = item.getInputStream();
                        System.out.println("fieldName:" +fieldName);
                        System.out.println("fileName:" +fileName);
                    }
                    
                } 
                
            } catch (FileUploadException ex) {
                Logger.getLogger(UpdateAccounts.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            
            
            
            
            String firstName = request.getParameter("firstName")+"";
            String lastName = request.getParameter("lastName")+"";
            String accountContactId = request.getParameter("accountContactId")+"";
            String middleName = request.getParameter("middleName")+"";
            String nickName = request.getParameter("nickName")+"";
            String emailAdd = request.getParameter("emailAdd")+"";
            String mobileNum = request.getParameter("mobileNumber")+"";
            String telNum = request.getParameter("telephoneNumber")+"";
            String sex = request.getParameter("sex")+"";
            String spouseName = request.getParameter("spouseName")+"";
            String positionInTheCompany = request.getParameter("positionInTheCompany")+"";
            String tin = request.getParameter("tin")+"";
            String mothersMaidenName = request.getParameter("mothersMaidenName")+"";
            String spouseContactNum = request.getParameter("spouseContactNumber")+"";
            String lastSchoolAttended = request.getParameter("lastSchoolAttended")+"";
            String numberOfChildren = request.getParameter("numberOfChildren")+"";
            String education = request.getParameter("education")+"";
            String status = request.getParameter("status")+"";
            String accountIdFk = request.getParameter("accountId")+"";
            String imageLocation = request.getParameter("idPath")+"";
          
            String civilStatus = request.getParameter("civilStatus")+"";
            String birthday = request.getParameter("birthday")+"";
            String accountContactRoleIdFk = request.getParameter("accountContactRoleIdFk")+"";
           
         /*  Part filePart = request.getPart("idImage");
            String fileName = filePart.getSubmittedFileName(); 
            System.out.println("fileName: " + fileName);
            Path p = Paths.get(fileName); //creates a Path object
            String filePath = p.getFileName().toString();
            System.out.println("filePath: " +filePath);
            String imageName = fileName;*/
            
            String uploadPath = getServletContext().getRealPath("") + File.separator;
//            System.out.println("uploadPath: " +uploadPath);
//          
//          
//            System.out.println("accountContactId:  " + accountContactId);
//            System.out.println("firstName:  " + firstName);
//            System.out.println("lastName:  " + lastName);
//            System.out.println("middleName:  " + middleName);
//            System.out.println("nickName:  " + nickName);
//            System.out.println("emailAdd:  " + emailAdd);
//            System.out.println("mobileNum:  " + mobileNum);
//            System.out.println("telNum:  " + telNum);
//            System.out.println("sex:  " + sex);
//            System.out.println("civilStatus:  " + civilStatus);
//            System.out.println("spouseName:  " + spouseName);
//            System.out.println("positionInTheCompany:  " + positionInTheCompany);
//            System.out.println("tin:  " + tin);
//            System.out.println("mothersMaidenName:  " + mothersMaidenName);
//            System.out.println("spouseContactNum:  " + spouseContactNum);
//            System.out.println("numberOfChildren:  " + numberOfChildren);
//            System.out.println("education:  " + education);
//            System.out.println("lastSchoolAttended:  " + lastSchoolAttended);
//            System.out.println("status:  " + "ACTIVE");
//            System.out.println("accountIdFk:  " + accountIdFk);
//            System.out.println("accountContactRoleIdFk: " +accountContactRoleIdFk);
//            System.out.println("birthdate:  " + birthday);
//           // System.out.println("imageLocation:  " + imageLocation);
//            System.out.println("imageName:  " + imageName);
            String types = "1";
            String endPoint = Constants.serverURL;
            String email = Constants.email;
            String password = Constants.password;
            HTTPManagerDirectory tp = new HTTPManagerDirectory();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dateFormats = new SimpleDateFormat("MMM dd, yyy");
            
          

            
            if(birthday.equalsIgnoreCase("") || birthday.equalsIgnoreCase("-")){
                birthday ="1900-01-01";
            }else{
                Date bdate = dateFormats.parse(birthday);
                birthday =  dateFormat.format(bdate);
            }
            
            System.out.println("bdate: " +birthday);
            String finalURLEmail ="";
            String finalURLNumber ="";
            
            URI uriSample = null;
            int countMatch = 0;   
            ArrayList<AccountContact> allAccountContactEmail = new ArrayList<AccountContact>();
            AccountContactParser accountContactParserEmail = new AccountContactParser();
            
            ArrayList<AccountContact> allAccountContactNumber = new ArrayList<AccountContact>();
            AccountContactParser accountContactParserNumber = new AccountContactParser();
            
            
            String getKycAccountByInputEmail = endPoint + "account/getKycAccountByInput?input="+emailAdd;
            try {
                uriSample = new URI(getKycAccountByInputEmail);
                finalURLEmail = uriSample.toURL().toString();

            } catch (URISyntaxException ex) {
                finalURLEmail = endPoint + "account/getKycAccountByInput?input="+emailAdd;
            }


            String allAccountcontactResponse = tp.executeGetRequest(finalURLEmail, "GET", email, password);
            allAccountContactEmail = accountContactParserEmail.parseAllAccountContact(allAccountcontactResponse);
//            System.out.println("allAccountContact.size(): " +allAccountContactEmail.size());
            if(allAccountContactEmail.size()>0){
                countMatch++;
            }
            
            String getKycAccountByInputNumber = endPoint + "account/getKycAccountByInput?input="+mobileNum;
            try {
                uriSample = new URI(getKycAccountByInputNumber );
                finalURLNumber = uriSample.toURL().toString();

            } catch (URISyntaxException ex) {
                finalURLNumber = endPoint + "account/getKycAccountByInput?input="+mobileNum;
            }


            String allAccountContactResponse = tp.executeGetRequest(finalURLNumber, "GET", email, password);
            allAccountContactNumber = accountContactParserEmail.parseAllAccountContact(allAccountContactResponse);
//            System.out.println("allAccountContact.size(): " +allAccountContactNumber.size());
            if(allAccountContactNumber.size()>0){
                countMatch++;
            }
                
                
          /*if(countMatch<=0){
                AccountContact accountContact = new AccountContact("0", firstName, lastName, middleName,  nickName,  emailAdd,  mobileNum.replace("+63", "0"),
                           telNum,  sex,  civilStatus,  spouseName,  positionInTheCompany,  tin,  mothersMaidenName,
                           spouseContactNum,  numberOfChildren,  education,  lastSchoolAttended,  "0",
                           status,    "0",  birthday, types);
                        
                String defaultDate = "1900-01-01";

                 if(birthday.equalsIgnoreCase("") || birthday.equalsIgnoreCase("-")){
                     accountContact.setBirthdate(defaultDate);
                 }else{
                     accountContact.setBirthdate(birthday);
                 }
                 if(status.equalsIgnoreCase("")){
                    accountContact.setStatus("-");
                 }else{
                    accountContact.setStatus(status);
                 }
                String addUrl = endPoint + "account/addKYCContact";
                tp.executeAddKYC(addUrl, accountContact, "POST", email, password);


//                System.out.println("imageLocation: " + imageLocation);
                String finalUrl="";
                String uploadImage = endPoint + "account/uploadImages?email="+emailAdd +"&&mobileNumber=" +mobileNum;


                try {
                    uriSample = new URI(uploadImage);
                    finalUrl = uriSample.toURL().toString();

                 } catch (URISyntaxException ex) {
                     finalUrl = endPoint + "account/uploadImages?email="+emailAdd +"&&mobileNumber=" +mobileNum;          
                 }
//                System.out.println("imageLocation: " + imageLocation);
                tp.executeUploadImage(finalUrl, imageLocation, "POST", email, password);


                response.sendRedirect("successAdd.jsp");    
          }else{
                 response.sendRedirect("alreadyExist.jsp");
          } */     
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(UpdateAccounts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(UpdateAccounts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
