/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.Account;
import HTTPManagement.HTTPManagerDirectory;
import Others.Constants;
import Parser.AccountParser;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;

/**
 *
 * @author Adrian Paul
 */
@WebServlet(name = "ResendVerificationCode", urlPatterns = {"/ResendVerificationCode"})
public class ResendVerificationCode extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException, JSONException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            int number = new Random().nextInt(9000) + 1000;
            String code = Integer.toString(number);
            HttpSession session = request.getSession();
            
            URI uriSample = null;
            String finalUrl ="";
            String finalUrls ="";
            String endPoint = Constants.serverURL;
            String email = Constants.email;
            String password = Constants.password;
            String emailAddress = (String) session.getAttribute("emailAddress");
            String mobileNumber = (String) session.getAttribute("mobileNumber");
            String type = (String) session.getAttribute("type");
            HTTPManagerDirectory tp = new HTTPManagerDirectory();
            System.out.println("type: " +type);
            String url ="";
            String sendVia ="SMS";
            String getOTP = endPoint + "account/sendOTPviaSms/?mobileNumber="+mobileNumber+"&code="+code;
            System.out.println("getOTP: " + getOTP);

            try {
                uriSample = new URI(getOTP);
                finalUrl = uriSample.toURL().toString();

            } catch (URISyntaxException ex) {
                finalUrl = endPoint + "account/sendOTPviaSms/?mobileNumber="+mobileNumber+"&code="+code;
            }

            tp.sendOTP(finalUrl, "POST", email, password);
            session.setAttribute("verificationCode", code);
            System.out.println("verificationCode: "+code);
            session.setAttribute("type", "resendSMS");
            session.setAttribute("errorType", "");
            session.setAttribute("codeMatches", "true");
            session.setAttribute("countError", "0");
            session.setAttribute("tries", "3");
            String saveOTP = endPoint + "account/OTPtransaction/?input="+mobileNumber+"&sendVia="+sendVia+"&status=RESEND_CODE";
            System.out.println("saveOTP url: " +saveOTP);
            try {
                uriSample = new URI(saveOTP);
                 url = uriSample.toURL().toString();

             } catch (URISyntaxException ex) {
                 url = endPoint + endPoint + "account/OTPtransaction/?input="+mobileNumber+"&sendVia="+sendVia+"&status=RESEND_CODE";
             }
            tp.executeGetRequest(url, "POST", email, password);

            response.sendRedirect("verification.jsp");


       
            
          
            
            
            
            
            
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(ResendVerificationCode.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(ResendVerificationCode.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(ResendVerificationCode.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(ResendVerificationCode.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
