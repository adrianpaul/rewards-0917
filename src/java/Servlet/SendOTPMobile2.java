/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.Account;
import Bean.AccountContact;
import HTTPManagement.HTTPManagerDirectory;
import Others.Constants;
import Parser.AccountContactParser;
import Parser.AccountParser;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;

/**
 *
 * @author Adrian Paul
 */
@WebServlet(name = "SendOTPMobile2", urlPatterns = {"/SendOTPMobile2"})
public class SendOTPMobile2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException, JSONException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
             int number = new Random().nextInt(9000) + 1000;
            String code = Integer.toString(number);
            HttpSession session = request.getSession();
            
            URI uriSample = null;
            String finalUrl ="";
            String endPoint = Constants.serverURL;
            String email = Constants.email;
            String password = Constants.password;
            String folderName = Constants.folderName;
            
            
            String mobile =  request.getParameter("mobile")+"";
            System.out.println("mobile: "+mobile);
            String errorType = (String) session.getAttribute("errorType");
            String type = (String) session.getAttribute("type");
            String mobileNumber = (String) session.getAttribute("mobileNumber");
            String verificationCode = (String) session.getAttribute("verificationCode");
            
            String finalUrls ="";
           
            ArrayList<Account> allAccount = new ArrayList<Account>();
            AccountParser accountParser = new AccountParser();
            String getAllAccount = endPoint + "account/getAccountContactByInput/?input="+mobile;
            System.out.println("getAllAccount: "+getAllAccount);
            String sendVia  = "SMS";
            String isVerified = (String) session.getAttribute("isVerified");
           
            
            
            HTTPManagerDirectory tp = new HTTPManagerDirectory();
            try {
                uriSample = new URI(getAllAccount);
                finalUrls = uriSample.toURL().toString();

            } catch (URISyntaxException ex) {
                finalUrls = endPoint + "account/getAccountContactByInput/?input="+mobile;
            }
 

            String allAccountResponse = tp.executeGetRequest(finalUrls, "GET", email, password);
            String accountId ="";
            System.out.println("RESPONSE: "+allAccountResponse);
            allAccount = accountParser.parseAllAccount(allAccountResponse);
      
            int mobileCount =0;
            
            if(allAccount.size()>0){
                for(int i=0; i<allAccount.size();i++){
                    accountId = allAccount.get(i).getAccountIdFk();
                    mobileCount++;
                }
            }
            int countError = 0;
            if(!errorType.equals("") || type.equalsIgnoreCase("resendSMS")){
                if(!mobile.equalsIgnoreCase(mobileNumber)){
                    countError=0;
                }else{
                    countError=1;
                }
            }
            if(mobile.equalsIgnoreCase(mobileNumber) && !verificationCode.equals("")){
                countError=1;
            }
            
            if(mobile.equalsIgnoreCase(mobileNumber) && isVerified.equals("true")){
                countError=2;
            }
            
            System.out.println("countError: "+countError);
            if(countError != 0){
               
                if(countError == 2){
                    session.setAttribute("isVerified", "true");
                    response.sendRedirect(folderName+"verified.jsp");
                }else{
                    
                    session.setAttribute("isVerified", "false");
                    session.setAttribute("type", "sentSMS");
                    response.sendRedirect(folderName+"verification.jsp");
                }
                
            }else{
                
                String finalURL ="";
                    
                ArrayList<AccountContact> allAccountContact = new ArrayList<AccountContact>();
                AccountContactParser accountContactParser = new AccountContactParser();
                String getKycAccountByInput = endPoint + "account/getKycAccountByInput?input="+mobile;
                System.out.println("getKycAccountByInput: "+getKycAccountByInput);         
                    
                try {
                    uriSample = new URI(getKycAccountByInput);
                    finalURL = uriSample.toURL().toString();

                } catch (URISyntaxException ex) {
                    finalURL = endPoint + "account/getKycAccountByInput?input="+mobile;
                }

                int countMatch = 0;
                String allAccountmobileResponse = tp.executeGetRequest(finalURL, "GET", email, password);
                allAccountContact = accountContactParser.parseAllAccountContact(allAccountmobileResponse);
                System.out.println("allAccountContact.size(): " +allAccountContact.size());
                for(int i=0;i<allAccountContact.size();i++){
                    if(allAccountContact.get(i).getType().equalsIgnoreCase("2")){
                        System.out.println("NAME: " +allAccountContact.get(i).getFirstName() );
                        countMatch++;
                    }   
                }
            
                if(countMatch>0){
                     session.setAttribute("isExisted", "true");
                     response.sendRedirect(folderName+"alreadyClaimedMobile.jsp");
                     session.setAttribute("formType", "2");
                     System.out.println("already claimed");
                }else{
                    String urlRedeem ="";
            
                    String getRedeem = endPoint + "account/countByAccount?accountIdFk="+accountId;
                    System.out.println("getKycAccountByInputMobile: "+getRedeem);

                    try {
                        uriSample = new URI(getRedeem);
                        urlRedeem = uriSample.toURL().toString();


                    } catch (URISyntaxException ex) {
                        urlRedeem = endPoint + "account/countByAccount?accountIdFk="+accountId;
                    }

                    String redeemResponse = tp.executeGetRequest(urlRedeem, "GET", email, password);
                    System.out.println("redeemResponse: "+ redeemResponse);

                    if(Integer.parseInt(redeemResponse)>=3){
                        response.sendRedirect(folderName+"redeem.jsp");   
                    }else{
                        if(mobileCount>0){
                         String url="";
                         String getOTP = endPoint + "account/sendOTPviaSms/?mobileNumber="+mobile+"&code="+code;
                         System.out.println("getOTP: " + getOTP);

                        try {
                             uriSample = new URI(getOTP);
                             finalUrl = uriSample.toURL().toString();

                         } catch (URISyntaxException ex) {
                             finalUrl = endPoint + "account/sendOTPviaSms/?mobileNumber="+mobile+"&code="+code;
                         }

                        //tp.sendOTP(finalUrl, "POST", email, password);
                        session.setAttribute("isExisted", "true");
                        session.setAttribute("accountId", accountId);
                        session.setAttribute("verificationCode", code);
                        session.setAttribute("type", "sms");
                        session.setAttribute("userType", "update");
                        session.setAttribute("mobileNumber", mobile);
                        session.setAttribute("mobile", mobile);
                        
                        
                        
                        session.setAttribute("countError", "0");
                        session.setAttribute("tries", "3");
                        session.setAttribute("isVerified", "false");
                        session.setAttribute("formType", "2");
                        

                        String saveOTP = endPoint + "account/OTPtransaction/?input="+mobile+"&sendVia="+sendVia+"&status=SENT_OTP";

                        try {
                            uriSample = new URI(saveOTP);
                             url = uriSample.toURL().toString();

                         } catch (URISyntaxException ex) {
                             url = endPoint + endPoint + "account/OTPtransaction/?input="+mobile+"&sendVia="+sendVia+"&status=SENT_OTP";
                         }
                         tp.executeGetRequest(url, "POST", email, password);
                        response.sendRedirect(folderName+"verification.jsp");

                    }else{
                        System.out.println("NO RESPONSE");
                        session.setAttribute("isExisted", "false");
                        session.setAttribute("errorType", "mobileNumber");
                        session.setAttribute("mobile", "");
                        session.setAttribute("accountId", "");
                        response.sendRedirect(folderName+"enterMobile.jsp");
                    }
                    }
                    
                    
                
                }
                    
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(SendOTPMobile2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(SendOTPMobile2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(SendOTPMobile2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(SendOTPMobile2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
