/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.AccountContact;
import Bean.Voucher;
import HTTPManagement.HTTPManagerDirectory;
import Others.Constants;
import Parser.AccountContactParser;
import Parser.VoucherParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import java.util.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;
import static javax.xml.transform.OutputKeys.ENCODING;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 *
 * @author Adrian Paul
 */
@WebServlet(name = "AboutYourself2", urlPatterns = {"/AboutYourself2"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 1,maxFileSize = 1024 * 1024 * 5,maxRequestSize = 1024 * 1024 * 5)

public class AboutYourself2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException, JSONException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            URI uriSample = null;
            String endPoint = Constants.serverURL;
            String email = Constants.email;
            String password = Constants.password;
            HTTPManagerDirectory tp = new HTTPManagerDirectory();
            String folderName = Constants.folderName;

            int countMatch = 0;
            
            String firstName = request.getParameter("firstName")+"";
            String lastName = request.getParameter("lastName")+"";
            String accountContactId = request.getParameter("accountContactId")+"";
            String emailAdd = request.getParameter("emailAdd")+"";
            String mobileNum = request.getParameter("mobileNumber")+"";
            String otpNumber =(String) session.getAttribute("mobile")+"";
            String kycCampaignIdFk =(String) session.getAttribute("kycCampaignIdFk")+"";
             
            
            
            String positionInTheCompany = request.getParameter("accountContactRole")+"";
            String financialAccountIdFk = request.getParameter("financialAccountIdFk")+"";
            String day = request.getParameter("day")+"";
            String month = request.getParameter("month")+"";
            String year = request.getParameter("year")+"";
            
            String birthday = day+"-"+month+"-"+year;
            String accountContactRoleIdFk = request.getParameter("positionInTheCompany")+"";
            String department = request.getParameter("department")+"";
            String accountId = request.getParameter("accountId")+"";
            String interest = request.getParameter("interests")+"";
            String inCharge = request.getParameter("inCharge")+"";
            String gender = request.getParameter("gender")+"";
            String contactNumber =  request.getParameter("contactNumber");
          
            String types = (String) session.getAttribute("formType")+"";
            String gppChecked = (String) session.getAttribute("isCheckedPrivacy")+"";
           
          
            String voucherCode = "";
            String sendVoucherURL =""; 
            JSONObject metadata = new JSONObject();
            metadata.put("alternateContact", contactNumber);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(metadata);
            String metaData = jsonArray.toString();
          
           
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dateFormats = new SimpleDateFormat("MM-dd-yyyy");

            if(birthday.equalsIgnoreCase("") || birthday.equalsIgnoreCase("-")){
                birthday ="1900-01-01";
            }else{
                Date bdate = dateFormats.parse(birthday);
                birthday =  dateFormat.format(bdate);
            }
            System.out.println("birthday: "+birthday);
            if(inCharge.equalsIgnoreCase("I use the postpaid line")){
                session.setAttribute("useTheLine", "yes");
                
                 if(types.equalsIgnoreCase("1")){
                    ArrayList<AccountContact> allAccountContactFinancialAccountIdFk = new ArrayList<AccountContact>();
                    AccountContactParser accountContactParserFinancialAccountIdFk= new AccountContactParser();
                    ArrayList<Voucher> allVoucher = new ArrayList<Voucher>();
                    VoucherParser voucherParser = new VoucherParser();
                    String finalUrlFinancialAccountIdFk ="";

                    String getKycAccountByInputFinancialAccountIdFk = endPoint + "account/getKycAccountByFinancialAccId?financialAccId="+financialAccountIdFk;
                    try {
                        uriSample = new URI(getKycAccountByInputFinancialAccountIdFk);
                        finalUrlFinancialAccountIdFk= uriSample.toURL().toString();

                    } catch (URISyntaxException ex) {
                        finalUrlFinancialAccountIdFk = endPoint + "account/getKycAccountByFinancialAccId?financialAccId="+financialAccountIdFk;
                    }

                    String kycResonseFinancialAccountIdFk= tp.executeGetRequest(finalUrlFinancialAccountIdFk, "GET", email, password);
                    allAccountContactFinancialAccountIdFk = accountContactParserFinancialAccountIdFk.parseAllAccountContact(kycResonseFinancialAccountIdFk);
                    for(int i=0;i<allAccountContactFinancialAccountIdFk.size();i++){

                        if(allAccountContactFinancialAccountIdFk.get(i).getType().equalsIgnoreCase(types)){
                             countMatch++;
                        }   

                    }

                  
                   if(countMatch<=0){
                       String rewardType ="2";
                        AccountContact accountContact = new AccountContact(accountContactId, accountContactRoleIdFk, accountId, firstName, lastName,
                                emailAdd,  mobileNum,  contactNumber,  gender, positionInTheCompany, birthday, types, department, inCharge
                                ,interest,gppChecked,financialAccountIdFk,"1",rewardType, metaData, otpNumber,kycCampaignIdFk);



                        String defaultDate = "1900-01-01";

                         if(birthday.equalsIgnoreCase("") || birthday.equalsIgnoreCase("-")){
                             accountContact.setBirthdate(defaultDate);
                         }else{
                             accountContact.setBirthdate(birthday);
                         }




                        String addUrl = endPoint + "account/addKYCContact";
                        tp.executeAddUseTheLine(addUrl, accountContact, "POST", email, password);
                        String getVoucherUrl = "";
                        String getVoucher = endPoint + "account/getVoucher?isActive=true";
                        try {
                            uriSample = new URI(getVoucher);
                            getVoucherUrl = uriSample.toURL().toString();

                         } catch (URISyntaxException ex) {
                             getVoucherUrl = endPoint + "account/getVoucher?isActive=true";
                         }
                        String getVoucherResponse = tp.executeGetRequest(getVoucherUrl, "GET", email, password);
                        allVoucher = voucherParser.parseAllVoucher(getVoucherResponse);

                        for(int i=0;i<allVoucher.size(); i++){
                            if(Integer.parseInt(allVoucher.get(i).getCounter()) > 0){
                                voucherCode = allVoucher.get(i).getCode();
                            }
                        }
                        if(!voucherCode.equalsIgnoreCase("0") || !voucherCode.equalsIgnoreCase("")){
                            String sendVoucher =  endPoint + "account/sendVoucher?email="+emailAdd+"&mobile="+mobileNum+"&voucherCode="+voucherCode;
                            try {
                                uriSample = new URI(sendVoucher);
                                sendVoucherURL = uriSample.toURL().toString();

                             } catch (URISyntaxException ex) {
                                 sendVoucherURL = endPoint + "account/sendVoucher?email="+emailAdd+"&mobile="+mobileNum+"&voucherCode="+voucherCode;
                            }
                            tp.executeGetRequest(sendVoucherURL, "POST", email, password);
                            Voucher voucher = new Voucher("0",accountContactId,emailAdd,mobileNum,voucherCode,"DELIVERED");
                            String voucherUrl = endPoint + "account/voucher/saveHistory";
                            tp.executeAddVoucherHistory(voucherUrl, voucher, "POST", email, password);


                        }else{
                             Voucher voucher = new Voucher("0",accountContactId,emailAdd,mobileNum,voucherCode,"FAILED");
                            String voucherUrl = endPoint + "account/voucher/saveHistory";
                            tp.executeAddVoucherHistory(voucherUrl, voucher, "POST", email, password);
                        }





                        response.sendRedirect(folderName+"successAdd.jsp");  
                        session.setAttribute("successAdd", "Type2");
                   }else{
                        session.setAttribute("isExisted", "true");
                        session.setAttribute("successAdd", "false");
                        response.sendRedirect(folderName+"alreadyClaimedMobile.jsp");   
                   }
                
                
                }//type is equals to fa
                 
                if(types.equalsIgnoreCase("2")){
                    ArrayList<Voucher> allVoucher = new ArrayList<Voucher>();
                    VoucherParser voucherParser = new VoucherParser();

                    ArrayList<AccountContact> allAccountContactMobile = new ArrayList<AccountContact>();
                    AccountContactParser accountContactParserMobile = new AccountContactParser();

                    String finalUrlMobile ="";

                    String getKycAccountByInputMobile = endPoint + "account/getKycAccountByInput?input="+mobileNum;
                 
                    try {
                        uriSample = new URI(getKycAccountByInputMobile);
                        finalUrlMobile = uriSample.toURL().toString();


                    } catch (URISyntaxException ex) {
                        finalUrlMobile = endPoint + "account/getKycAccountByInput?input="+mobileNum;
                    }

                    String kycResonseMobile = tp.executeGetRequest(finalUrlMobile, "GET", email, password);
                    allAccountContactMobile = accountContactParserMobile.parseAllAccountContact(kycResonseMobile);

                    for(int i=0;i<allAccountContactMobile.size();i++){
                        if(allAccountContactMobile.get(i).getType().equalsIgnoreCase(types)){
                                 countMatch++;

                        }  
                    }

                 
                    String kycResonseFinancialAccountIdFk= tp.executeGetRequest(finalUrlMobile, "GET", email, password);
                    allAccountContactMobile = accountContactParserMobile.parseAllAccountContact(kycResonseFinancialAccountIdFk);
                    for(int i=0;i<allAccountContactMobile.size();i++){

                        if(allAccountContactMobile.get(i).getType().equalsIgnoreCase(types)){
                             countMatch++;
                        }   

                    }

                   if(countMatch<=0){
                       String rewardType ="2";
                        AccountContact accountContact = new AccountContact(accountContactId, accountContactRoleIdFk, accountId, firstName, lastName,
                                emailAdd,  mobileNum,  contactNumber,  gender, positionInTheCompany, birthday, types, department, inCharge
                                ,interest,gppChecked,financialAccountIdFk,"1",rewardType, metaData, otpNumber,kycCampaignIdFk);



                        String defaultDate = "1900-01-01";

                         if(birthday.equalsIgnoreCase("") || birthday.equalsIgnoreCase("-")){
                             accountContact.setBirthdate(defaultDate);
                         }else{
                             accountContact.setBirthdate(birthday);
                         }




                        String addUrl = endPoint + "account/addKYCContact";
                        tp.executeAddUseTheLine(addUrl, accountContact, "POST", email, password);

                        String getVoucherUrl = "";
                        String getVoucher = endPoint + "account/getVoucher?isActive=true";
                    
                        try {
                            uriSample = new URI(getVoucher);
                            getVoucherUrl = uriSample.toURL().toString();

                         } catch (URISyntaxException ex) {
                             getVoucherUrl = endPoint + "account/getVoucher?isActive=true";
                         }
                        String getVoucherResponse = tp.executeGetRequest(getVoucherUrl, "GET", email, password);
                        allVoucher = voucherParser.parseAllVoucher(getVoucherResponse);

                        for(int i=0;i<allVoucher.size(); i++){
                            if(Integer.parseInt(allVoucher.get(i).getCounter()) > 0){
                                voucherCode = allVoucher.get(i).getCode();
                            }
                        }
                        if(!voucherCode.equalsIgnoreCase("0") || !voucherCode.equalsIgnoreCase("")){
                            String sendVoucher =  endPoint + "account/sendVoucher?email="+emailAdd+"&mobile="+mobileNum+"&voucherCode="+voucherCode;
                            try {
                                uriSample = new URI(sendVoucher);
                                sendVoucherURL = uriSample.toURL().toString();

                             } catch (URISyntaxException ex) {
                                 sendVoucherURL = endPoint + "account/sendVoucher?email="+emailAdd+"&mobile="+mobileNum+"&voucherCode="+voucherCode;
                            }
                            tp.executeGetRequest(sendVoucherURL, "POST", email, password);
                            Voucher voucher = new Voucher("0",accountContactId,emailAdd,mobileNum,voucherCode,"DELIVERED");
                            String voucherUrl = endPoint + "account/voucher/saveHistory";
                            tp.executeAddVoucherHistory(voucherUrl, voucher, "POST", email, password);


                        }else{
                             Voucher voucher = new Voucher("0",accountContactId,emailAdd,mobileNum,voucherCode,"FAILED");
                            String voucherUrl = endPoint + "account/voucher/saveHistory";
                            tp.executeAddVoucherHistory(voucherUrl, voucher, "POST", email, password);
                        }





                        response.sendRedirect(folderName+"successAdd.jsp");  
                        session.setAttribute("successAdd", "Type2");
                   }else{
                        session.setAttribute("isExisted", "true");
                        session.setAttribute("successAdd", "false");
                        response.sendRedirect(folderName+"alreadyClaimedMobile.jsp");   
                   }
                }
               
                
                
                
                
                
            }else{
                session.setAttribute("useTheLine", "no");
                session.setAttribute("accountId",accountId);
                session.setAttribute("accountContactIdFk",accountContactId);
                session.setAttribute("firstName",firstName);
                session.setAttribute("lastName",lastName);
                session.setAttribute("emailAdd",emailAdd);
                session.setAttribute("mobileNum",mobileNum);
                session.setAttribute("contactNumber",contactNumber);
                session.setAttribute("positionInTheCompany",positionInTheCompany);
                session.setAttribute("financialAccountIdFk",financialAccountIdFk);
                session.setAttribute("birthday",birthday);
                session.setAttribute("accountContactRoleIdFk",accountContactRoleIdFk);
                session.setAttribute("department",department);
                session.setAttribute("interest",interest);
                session.setAttribute("inCharge",inCharge);
                session.setAttribute("sex",gender);
                response.sendRedirect(folderName+"aboutYourCompany.jsp");
            
            }
            
           
                         
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(UpdateAccounts.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(AboutYourself2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(UpdateAccounts.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(AboutYourself2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
