/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.Account;
import Bean.AccountContact;
import HTTPManagement.HTTPManagerDirectory;
import Others.Constants;
import Parser.AccountParser;
import Parser.AccountContactParser;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Adrian Paul
 */
@WebServlet(name = "VerificationCode2", urlPatterns = {"/VerificationCode2"})
public class VerificationCode2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
           
            HttpSession session = request.getSession();
            String code = request.getParameter("code")+"";
            System.out.println("code: "+code);
            String verificationCode = (String) session.getAttribute("verificationCode");
            String userType = (String) session.getAttribute("userType")+"";
            String errorCount = (String) session.getAttribute("countError")+"";
            String triesCount = (String) session.getAttribute("tries")+"";
            URI uriSample = null;
            String finalUrl ="";
            String finalUrls ="";
            String endPoint = Constants.serverURL;
            String email = Constants.email;
            String password = Constants.password;
            String folderName = Constants.folderName;
            
            String emailAddress = (String) session.getAttribute("emailAddress");
            String mobileNumber = (String) session.getAttribute("mobileNumber");
            String accountId = (String) session.getAttribute("accountId");
            System.out.println("accountId: " + accountId);
            
            
            String type = (String) session.getAttribute("type");
            int tries = Integer.parseInt(triesCount);
            
            
            HTTPManagerDirectory tp = new HTTPManagerDirectory();;
          
            String firstName ="";
            String lastName ="";
            String middleName ="";
            String nickName ="";
            String mobileNum ="";
            String telNum ="";
            String emailAdd ="";
            String sex ="";
            String civilStatus ="";
            String spouseName ="";
            String positionInTheCompany ="";
            String tin ="";
            String mothersMaidenName ="";
            String spouseContactNum ="";
            String numberOfChildren ="";
            String education ="";
            String lastSchoolAttended ="";
            String accountContactRoleIdFk ="";
            String status ="";
            String accountIdFk ="";
            String birthdate ="";
            String imageLocation ="";
            String imageName ="";
            String accountContactId ="";
            String age ="";
            String department ="";
            
            
            int countError = Integer.parseInt(errorCount);
            System.out.println("code: "+code);
            if(code.equalsIgnoreCase(verificationCode) && countError<3){
               

                String url ="";
                String urlGetInfo ="";
                String urlAdd ="";
                String types ="3";
                String sendVia ="SMS";
                String saveOTP = endPoint + "account/OTPtransaction/?input="+mobileNumber+"&sendVia="+sendVia+"&status=SUCCESS";
                System.out.println("saveOTP url: " +saveOTP);
                try {
                    uriSample = new URI(saveOTP);
                     url = uriSample.toURL().toString();

                 } catch (URISyntaxException ex) {
                     url = endPoint + endPoint + "account/OTPtransaction/?input="+mobileNumber+"&sendVia="+sendVia+"&status=SUCCESS";
                 }
                if(!mobileNumber.equals("") || session.getAttribute("mobileNumber") == null ){
                    response.sendRedirect(folderName+"aboutYourself.jsp");
                    session.setAttribute("codeMatches", "true");
                    session.setAttribute("formType","2");
                } 
                
                session.setAttribute("countError", "0");
                session.setAttribute("errorType", "");
            }else{
                countError++;
                tries-=1;
                
                String url ="";
                String sendVia ="SMS";
                String saveOTP = endPoint + "account/OTPtransaction/?input="+mobileNumber+"&sendVia="+sendVia+"&status=FAILED";
                System.out.println("saveOTP url: " +saveOTP);
                try {
                    uriSample = new URI(saveOTP);
                     url = uriSample.toURL().toString();

                 } catch (URISyntaxException ex) {
                     url = endPoint + endPoint + "account/OTPtransaction/?input="+mobileNumber+"&sendVia="+sendVia+"&status=FAILED";
                 }
                tp.executeGetRequest(url, "POST", email, password);
               
                session.setAttribute("codeMatches", "false");
                session.setAttribute("errorType", "attempt");
                session.setAttribute("countError", Integer.toString(countError));
                session.setAttribute("tries", Integer.toString(tries));
                if(tries<=0){
                    session.setAttribute("tries", "0");
                }
                response.sendRedirect(folderName+"verification.jsp");
                if(countError>=3){
                    session.setAttribute("errorType", "attemptThrice");
               }
                
            }
            System.out.println("CountError: " + countError);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(VerificationCode2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(VerificationCode2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
