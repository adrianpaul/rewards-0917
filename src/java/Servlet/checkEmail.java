/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.Account;
import HTTPManagement.HTTPManagerDirectory;
import Others.Constants;
import Parser.AccountParser;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;

/**
 *
 * @author Adrian Paul
 */
@WebServlet(name = "checkEmail", urlPatterns = {"/checkEmail"})
public class checkEmail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            
            String financialAccountId = (String) session.getAttribute("financialAccountId");
            String emailAddress = request.getParameter("emailAdd");
             URI uriSample = null;
            
            String endPoint = Constants.serverURL;
            String email = Constants.email;
            String password = Constants.password;
            
            
            HTTPManagerDirectory tp = new HTTPManagerDirectory();
            ArrayList<Account> allAccount = new ArrayList<Account>();
            AccountParser accountParser = new AccountParser();
            String getAllAccount = endPoint + "account/getAccountContactByFinancialAccId/?financialAccId="+financialAccountId;
            String finalUrls ="";
            uriSample = null;
            try {
                uriSample = new URI(getAllAccount);
                finalUrls = uriSample.toURL().toString();

            } catch (URISyntaxException ex) {
                finalUrls = endPoint + "account/getAccountContactByFinancialAccId/?financialAccId="+financialAccountId;
            }


            String allAccountResponse = tp.executeGetRequest(finalUrls, "GET", email, password);

            System.out.println("RESPONSE CHECK EMAIL: "+allAccountResponse);
            allAccount = accountParser.parseAllAccount(allAccountResponse);
            int emailCount = 0;
            for(int i=0; i<allAccount.size();i++){
                String emailAdd = allAccount.get(i).getEmail();
                if(emailAddress.equalsIgnoreCase(emailAdd)){
                    emailCount++;
                }
            }
            if(emailCount>0){
                response.sendRedirect("send.jsp");
            }else{
                response.sendRedirect("enterEmail.jsp");
            }
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(checkEmail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(checkEmail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(checkEmail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(checkEmail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
