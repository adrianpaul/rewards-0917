/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.AccountContact;
import HTTPManagement.HTTPManagerDirectory;
import Others.Constants;
import Parser.AccountContactParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import java.util.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;
import static javax.xml.transform.OutputKeys.ENCODING;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
/**
 *
 * @author Adrian Paul
 */
@WebServlet(name = "UpdateAccounts", urlPatterns = {"/UpdateAccounts"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 1,maxFileSize = 1024 * 1024 * 5,maxRequestSize = 1024 * 1024 * 5)

public class UpdateAccounts extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
              
          
            
            
            String firstName = request.getParameter("firstName")+"";
            String lastName = request.getParameter("lastName")+"";
            String accountContactId = request.getParameter("accountContactId")+"";
            String middleName = request.getParameter("middleName")+"";
            String nickName = request.getParameter("nickName")+"";
            String emailAdd = request.getParameter("emailAdd")+"";
            String mobileNum = request.getParameter("mobileNumber")+"";
            String telNum = request.getParameter("telephoneNumber")+"";
            String sex = request.getParameter("sex")+"";
            String spouseName = request.getParameter("spouseName")+"";
            String positionInTheCompany = request.getParameter("accountContactRole")+"";
            String tin = request.getParameter("tin")+"";
            String mothersMaidenName = request.getParameter("mothersMaidenName")+"";
            String spouseContactNum = request.getParameter("spouseContactNumber")+"";
            String lastSchoolAttended = request.getParameter("lastSchoolAttended")+"";
            String numberOfChildren = request.getParameter("numberOfChildren")+"";
            String education = request.getParameter("education")+"";
            String status = request.getParameter("status")+"";
            String financialAccountIdFk = request.getParameter("financialAccountIdFk")+"";
            String imageLocation = request.getParameter("idPath")+"";
            String civilStatus = request.getParameter("civilStatus")+"";
            String birthday = request.getParameter("birthday")+"";
            String accountContactRoleIdFk = request.getParameter("positionInTheCompany")+"";
            String age = request.getParameter("age")+"";
            String department = request.getParameter("department")+"";
            String accountId = request.getParameter("accountId")+"";
            String types = "1";
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dateFormats = new SimpleDateFormat("MMM dd, yyy");
            
          

            
            if(birthday.equalsIgnoreCase("") || birthday.equalsIgnoreCase("-")){
                birthday ="1900-01-01";
            }else{
                Date bdate = dateFormats.parse(birthday);
                birthday =  dateFormat.format(bdate);
            }
            session.setAttribute("accountId",accountId);
            session.setAttribute("accountContactIdFk",accountContactId);
            session.setAttribute("firstName",firstName);
            session.setAttribute("lastName",lastName);
            session.setAttribute("middleName",middleName);
            session.setAttribute("emailAdd",emailAdd);
            session.setAttribute("mobileNum",mobileNum);
            session.setAttribute("telNum",telNum);
            session.setAttribute("sex",sex);
            session.setAttribute("spouseName",spouseName);
            session.setAttribute("positionInTheCompany",positionInTheCompany);
            session.setAttribute("mothersMaidenName",mothersMaidenName);
            session.setAttribute("spouseContactNum",spouseContactNum);
            session.setAttribute("lastSchoolAttended",lastSchoolAttended);
            session.setAttribute("numberOfChildren",numberOfChildren);
            session.setAttribute("education",education);
            session.setAttribute("status",status);
            session.setAttribute("financialAccountIdFk",financialAccountIdFk);
            session.setAttribute("imageLocation",imageLocation);
            session.setAttribute("civilStatus",civilStatus);
            session.setAttribute("birthday",birthday);
            session.setAttribute("accountContactRoleIdFk",accountContactRoleIdFk);
            session.setAttribute("age",age);
            session.setAttribute("department",department);
            session.setAttribute("tin",tin);
            session.setAttribute("nickName",nickName);
            session.setAttribute("type",types);
            response.sendRedirect("inCharge.jsp");
            
                         
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(UpdateAccounts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(UpdateAccounts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
