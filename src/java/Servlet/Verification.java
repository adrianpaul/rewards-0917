/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.Account;
import Bean.AccountContact;
import HTTPManagement.HTTPManagerDirectory;
import Others.Constants;
import Parser.AccountContactParser;
import Parser.AccountParser;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;

/**
 *
 * @author Adrian Paul
 */
@WebServlet(name = "Verification", urlPatterns = {"/Verification"})
public class Verification extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String accountNumber = request.getParameter("accountNumber")+"";
            System.out.println("accountNumber: "+accountNumber);
            HttpSession session = request.getSession();
            URI uriSample = null;
            
            String endPoint = Constants.serverURL;
            String email = Constants.email;
            String password = Constants.password;
            
            
            HTTPManagerDirectory tp = new HTTPManagerDirectory();
            
    
            int countMatch = 0;   
            ArrayList<AccountContact> allAccountContactEmail = new ArrayList<AccountContact>();
            AccountContactParser accountContactParserEmail = new AccountContactParser();
            String finalUrlEmail ="";
         
            String getKycAccountByInputEmail = endPoint + "account/getKycAccountByFinancialAccId?financialAccId="+accountNumber;
            System.out.println("getKycAccountByInputEmail: "+getKycAccountByInputEmail);
            try {
                uriSample = new URI(getKycAccountByInputEmail);
                finalUrlEmail = uriSample.toURL().toString();

            } catch (URISyntaxException ex) {
                finalUrlEmail = endPoint + "account/getKycAccountByFinancialAccId?financialAccId="+accountNumber;
            }

            String allAccountcontactResponse = tp.executeGetRequest(finalUrlEmail, "GET", email, password);
            allAccountContactEmail = accountContactParserEmail.parseAllAccountContact(allAccountcontactResponse);
            System.out.println("allAccountContactEmail.size(): "+allAccountContactEmail.size());
            
            for(int i=0;i<allAccountContactEmail.size();i++){
                if(allAccountContactEmail.get(i).getType().equalsIgnoreCase("1")){
                    System.out.println("NAME: " +allAccountContactEmail.get(i).getFirstName() );
                    countMatch++;
                }   
            }
            
            if(countMatch>0){
                session.setAttribute("isExisted", "true");
                response.sendRedirect("alreadyClaimed.jsp"); 
                
            }else{
                ArrayList<Account> allAccount = new ArrayList<Account>();
                AccountParser accountParser = new AccountParser();
                String getAllAccount = endPoint + "account/getAccountContactByFinancialAccId/?financialAccId="+accountNumber;
                String finalUrls ="";
                uriSample = null;
                try {
                    uriSample = new URI(getAllAccount);
                    finalUrls = uriSample.toURL().toString();

                } catch (URISyntaxException ex) {
                    finalUrls = endPoint + "account/getAccountContactByFinancialAccId/?financialAccId="+accountNumber;
                }


                String allAccountResponse = tp.executeGetRequest(finalUrls, "GET", email, password);
                System.out.println("RESPONSE: "+allAccountResponse);
                allAccount = accountParser.parseAllAccount(allAccountResponse);
                System.out.println("allAccount.size(): " +allAccount.size());
                if(allAccount.size()>0){
                    
                    session.setAttribute("isExisted", "true");
                    session.setAttribute("formType", "1");
                    session.setAttribute("financialAccountIdFk", accountNumber);
                    response.sendRedirect("updateForm1.jsp");
                }else{
                   System.out.println("NO RESPONSE");
                    session.setAttribute("isExisted", "false");
                    
                    response.sendRedirect("accountNumber.jsp"); 
                }
            }
           
         
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(Verification.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Verification.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(Verification.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Verification.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
