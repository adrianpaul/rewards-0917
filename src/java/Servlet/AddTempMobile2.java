/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.AccountContact;
import Bean.Voucher;
import HTTPManagement.HTTPManagerDirectory;
import Others.Constants;
import Parser.AccountContactParser;
import Parser.VoucherParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import java.util.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;
import static javax.xml.transform.OutputKeys.ENCODING;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 *
 * @author Adrian Paul
 */
@WebServlet(name = "AddTempMobile2", urlPatterns = {"/AddTempMobile2"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 1,maxFileSize = 1024 * 1024 * 5,maxRequestSize = 1024 * 1024 * 5)

public class AddTempMobile2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException, JSONException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            
               String folderName = Constants.folderName;
          
            String firstName = (String) session.getAttribute("firstName");
            String lastName = (String) session.getAttribute("lastName");
            String accountContactIdFk = (String) session.getAttribute("accountContactIdFk");
            String mobileNum = (String) session.getAttribute("mobileNum");
            String sex = (String) session.getAttribute("sex");
            String positionInTheCompany = (String) session.getAttribute("positionInTheCompany");
            String financialAccountIdFk = (String) session.getAttribute("financialAccountIdFk");
            String birthday = (String) session.getAttribute("birthday");
            String accountContactRoleIdFk = (String) session.getAttribute("accountContactRoleIdFk");
            String department = (String) session.getAttribute("department");
            String inCharge = (String) session.getAttribute("inCharge");
            String gppChecked = (String) session.getAttribute("isCheckedPrivacy");
            String interest = (String) session.getAttribute("interest");
            String accountId = (String) session.getAttribute("accountId");
            String contactNumber = (String) session.getAttribute("contactNumber");
            String companyName = (String) session.getAttribute("companyName");
            String numberOfEmployees = (String) session.getAttribute("numberOfEmployees");
            String industry = (String) session.getAttribute("industry");
            String branch = (String) session.getAttribute("branch");
            String companyAnniversary = (String) session.getAttribute("companyAnniversary");
            String billingContactPerson = (String) session.getAttribute("billingContactPerson");
            String billingContactEmails = (String) session.getAttribute("billingContactEmail");
            String city = (String) session.getAttribute("city");
            String province = (String) session.getAttribute("province");
            String middleName = (String) session.getAttribute("middleName");
            String emailAdd = (String) session.getAttribute("emailAdd");
            String otpNumber = (String) session.getAttribute("mobile")+"";
            String kycCampaignIdFk = (String) session.getAttribute("kycCampaignIdFk")+"";
             
            
            System.out.println("accountID: "+accountId);
           
            String rewardType = "1";
       
            System.out.println("otpNumber: "+otpNumber);
            System.out.println("companyAnniversary1: "+companyAnniversary);
            String type = (String) session.getAttribute("formType");
            System.out.println("type: "+type);
            String reward = request.getParameter("reward")+"";
            System.out.println("reward: "+reward);
            String businessImprovement =(String) session.getAttribute("improves")+"";
            String useTheLine = (String) session.getAttribute("useTheLine");
            
            String voucherCode = "";
       
         
            JSONObject metadata = new JSONObject();
            metadata.put("alternateContact", contactNumber);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(metadata);
            String metaData = jsonArray.toString();
//            System.out.println("metaData: "+metaData);
            
            URI uriSample = null;
            String endPoint = Constants.serverURL;
            String email = Constants.email;
            String password = Constants.password;
            HTTPManagerDirectory tp = new HTTPManagerDirectory();

            
            int countMatch = 0;   
            
            ArrayList<Voucher> allVoucher = new ArrayList<Voucher>();
            VoucherParser voucherParser = new VoucherParser();
           
            ArrayList<AccountContact> allAccountContactMobile = new ArrayList<AccountContact>();
            AccountContactParser accountContactParserMobile = new AccountContactParser();
            
            String finalUrlMobile ="";
            
            String getKycAccountByInputMobile = endPoint + "account/getKycAccountByInput?input="+mobileNum;
            System.out.println("getKycAccountByInputMobile: "+getKycAccountByInputMobile);
           
            try {
                uriSample = new URI(getKycAccountByInputMobile);
                finalUrlMobile = uriSample.toURL().toString();
                

            } catch (URISyntaxException ex) {
                finalUrlMobile = endPoint + "account/getKycAccountByInput?input="+mobileNum;
            }

            String kycResonseMobile = tp.executeGetRequest(finalUrlMobile, "GET", email, password);
            allAccountContactMobile = accountContactParserMobile.parseAllAccountContact(kycResonseMobile);
        
            for(int i=0;i<allAccountContactMobile.size();i++){
                if(allAccountContactMobile.get(i).getType().equalsIgnoreCase(type)){
                         countMatch++;
                    
                }  
            }
            
            System.out.println("countMatch: "+countMatch);
            String urlRedeem ="";
            
            String getRedeem = endPoint + "account/countByAccount?accountIdFk="+accountId;
            System.out.println("getKycAccountByInputMobile: "+getRedeem);
           
            try {
                uriSample = new URI(getRedeem);
                urlRedeem = uriSample.toURL().toString();
                

            } catch (URISyntaxException ex) {
                urlRedeem = endPoint + "account/countByAccount?accountIdFk="+accountId;
            }

            String redeemResponse = tp.executeGetRequest(urlRedeem, "GET", email, password);
            System.out.println("redeemResponse: "+ redeemResponse);
            
            if(Integer.parseInt(redeemResponse)>=3){
                response.sendRedirect(folderName+"redeem.jsp");   
            }else{
                if(countMatch<=0){
                    AccountContact accountContact = new AccountContact(accountContactIdFk, accountContactRoleIdFk, accountId, firstName, lastName,  emailAdd,  mobileNum,
                                 sex,  positionInTheCompany,  birthday, department, type, companyAnniversary, inCharge , branch,
                            companyName, industry,numberOfEmployees,interest,gppChecked, businessImprovement,financialAccountIdFk,"1", rewardType,metaData,reward,otpNumber,kycCampaignIdFk);



                    String defaultDate = "1900-01-01";

                     if(birthday.equalsIgnoreCase("") || birthday.equalsIgnoreCase("-")){
                         accountContact.setBirthdate(defaultDate);
                     }else{
                         accountContact.setBirthdate(birthday);
                     }
                 


                    String addUrl = endPoint + "account/addKYCContact";
                    tp.executeAddKYC(addUrl, accountContact, "POST", email, password);


                   /* String finalUrl="";

                    String uploadImage = endPoint + "account/uploadImages?email="+emailAdd +"&mobileNumber=" +mobileNum+"&type=1";


                    try {
                        uriSample = new URI(uploadImage);
                        finalUrl = uriSample.toURL().toString();

                     } catch (URISyntaxException ex) {
                         finalUrl = endPoint + "account/uploadImages?email="+emailAdd +"&mobileNumber=" +mobileNum+"&type=1";          
                     }
                    tp.executeUploadImage(finalUrl, imageLocation, "POST", email, password);*/

                    String sendVoucherURL =""; 

                    String sendVoucher = endPoint + "account/sendVoucherByDenomination?denomination="+reward+"&recipientMobile="+mobileNum+"&recipientEmail="+emailAdd+"&type="+type+"&quantity=1";
                    System.out.println("sendVoucher: "+sendVoucher);
                    try {
                        uriSample = new URI(sendVoucher);
                        sendVoucherURL = uriSample.toURL().toString();

                     } catch (URISyntaxException ex) {
                         sendVoucherURL = endPoint + "account/sendVoucherByDenomination?denomination="+reward+"&recipientMobile="+mobileNum+"&recipientEmail="+emailAdd+"&type="+type+"&quantity=1";                    
                     }
                    tp.executeGetRequest(sendVoucherURL, "POST", email, password);


                    response.sendRedirect(folderName+"successAddMobile.jsp");  
                    session.setAttribute("successAdd", "Type1");


                }else{
                    session.setAttribute("isExisted", "true");
                    session.setAttribute("successAdd", "false");
                    response.sendRedirect(folderName+"alreadyClaimedMobile.jsp");   
                }
            }
           
            
        
         
            
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                processRequest(request, response);
            } catch (JSONException ex) {
                Logger.getLogger(AddTempMobile.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ParseException ex) {
            Logger.getLogger(UpdateAccounts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                processRequest(request, response);
            } catch (JSONException ex) {
                Logger.getLogger(AddTempMobile.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ParseException ex) {
            Logger.getLogger(UpdateAccounts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
