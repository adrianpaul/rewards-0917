/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Bean.AccountContact;
import HTTPManagement.HTTPManagerDirectory;
import Others.Constants;
import Parser.AccountContactParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import java.util.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;
import static javax.xml.transform.OutputKeys.ENCODING;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
/**
 *
 * @author Adrian Paul
 */
@WebServlet(name = "AboutYourCompany", urlPatterns = {"/AboutYourCompany"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 1,maxFileSize = 1024 * 1024 * 5,maxRequestSize = 1024 * 1024 * 5)

public class AboutYourCompany extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
              
          
            
            
            String companyName = request.getParameter("companyName")+"";
            String numberOfEmployees = request.getParameter("numberOfEmployees")+"";
            String industry = request.getParameter("industry")+"";
            String othersIndustry = request.getParameter("othersIndustry")+"";
            
            String branch = request.getParameter("branch")+"";
//            System.out.println("Branch: "+ branch);
            String companyAnniversary = "";
            String month = request.getParameter("month")+"";
            String year = request.getParameter("year")+"";
            
            String billingContactPerson = request.getParameter("billingContactPerson")+"";
            String billingContactEmail = request.getParameter("billingContactEmail")+"";
            String city = request.getParameter("city")+"";
            String province = request.getParameter("province")+"";
//            System.out.println("Others Industry: "+othersIndustry);
            
    
            String types = "2";
           SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dateFormats = new SimpleDateFormat("MM-yyyy");
            
            if(month.equalsIgnoreCase("") || year.equalsIgnoreCase("") || month.equalsIgnoreCase("null") || year.equalsIgnoreCase("null")){
                 companyAnniversary ="01-1900";
            }else{
                companyAnniversary = month+"-"+year;
            }

        
            
            if(industry.equalsIgnoreCase("Others")){
                if(!othersIndustry.equalsIgnoreCase("")){
                   industry = othersIndustry;
                }
            }
            
            session.setAttribute("companyName",companyName);
            session.setAttribute("numberOfEmployees",numberOfEmployees);
            session.setAttribute("industry",industry);
            session.setAttribute("branch",branch);
            session.setAttribute("companyAnniversary",companyAnniversary);
            session.setAttribute("billingContactPerson",billingContactPerson);
            session.setAttribute("billingContactEmail",billingContactEmail);
            session.setAttribute("city",city);
            session.setAttribute("province",province);
            
            response.sendRedirect("serve.jsp");
            
                         
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(UpdateAccounts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(UpdateAccounts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
