/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

import Bean.CityTown;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author sgmwpsupport
 */
public class CityTownParser {

    public CityTown cityTownParseFeed(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            CityTown ct = new CityTown();
            if (obj.has("id")) {
                if (!obj.isNull("id")) {
                    ct.setId(obj.getInt("id") + "");
                } else {
                    ct.setId("-");
                }
            }

            if (obj.has("name")) {
                if (!obj.isNull("name")) {
                    ct.setName(obj.getString("name") + "");
                } else {
                    ct.setName("-");
                }
            }

            if (obj.has("shortCode")) {
                if (!obj.isNull("shortCode")) {
                    ct.setShortCode(obj.getString("shortCode") + "");
                } else {
                    ct.setShortCode("-");
                }
            }

            if (obj.has("areaCode")) {
                if (!obj.isNull("areaCode")) {
                    ct.setAreaCode(obj.getString("areaCode") + "");
                } else {
                    ct.setAreaCode("-");
                }
            }

            if (obj.has("provinceIdFk")) {
                if (!obj.isNull("provinceIdFk")) {
                    ct.setProvinceIdFk(obj.getInt("provinceIdFk") + "");
                } else {
                    ct.setProvinceIdFk("-");
                }
            }

            return ct;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<CityTown> parseAllCityTowns(String response) {
        try {
            JSONArray ar = new JSONArray(response);
            ArrayList<CityTown> citytowns = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {
                JSONObject obj = ar.getJSONObject(i);
                CityTown citytown = new CityTown();

                if (obj.has("id")) {
                    if (!obj.isNull("id")) {
                        citytown.setId(obj.getInt("id") + "");
                    } else {
                        citytown.setId("-");
                    }
                }

                if (obj.has("name")) {
                    if (!obj.isNull("name")) {
                        citytown.setName(obj.getString("name") + "");
                    } else {
                        citytown.setName("-");
                    }
                }

                if (obj.has("shortCode")) {
                    if (!obj.isNull("shortCode")) {
                        citytown.setShortCode(obj.getString("shortCode") + "");
                    } else {
                        citytown.setShortCode("-");
                    }
                }

                if (obj.has("areaCode")) {
                    if (!obj.isNull("areaCode")) {
                        citytown.setShortCode(obj.getString("areaCode") + "");
                    } else {
                        citytown.setShortCode("-");
                    }
                }

                if (obj.has("provinceIdFk")) {
                    if (!obj.isNull("provinceIdFk")) {
                        citytown.setProvinceIdFk(obj.getInt("provinceIdFk") + "");
                    } else {
                        citytown.setProvinceIdFk("-");
                    }
                }

                citytowns.add(citytown);
            }
            return citytowns;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

}
