/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

import Bean.AccountContact;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author sgmwpsupport
 */
public class AccountContactParser {

    public AccountContact userParseFeed(String response) {

        try {
//            System.out.print(response);

            JSONObject obj = new JSONObject(response);
            AccountContact user = new AccountContact();
            
            if (obj.has("id")) {
                if (!obj.isNull("id")) {
                    user.setId(obj.getInt("id") + "");
                } else {
                    user.setId("-");
                }
            }
            
            if (obj.has("firstName")) {
                if (!obj.isNull("firstName")) {
                    user.setFirstName(obj.getString("firstName") + "");
                } else {
                    user.setFirstName("-");
                }
            }
            
            if (obj.has("middleName")) {
                if (!obj.isNull("middleName")) {
                    user.setMiddleName(obj.getString("middleName") + "");
                } else {
                    user.setMiddleName("-");
                }
            }
            
            if (obj.has("lastName")) {
                if (!obj.isNull("lastName")) {
                    user.setLastName(obj.getString("lastName") + "");
                } else {
                    user.setLastName("-");
                }
            }
            
            if (obj.has("nickName")) {
                if (!obj.isNull("nickName")) {
                    user.setNickname(obj.getString("nickName") + "");
                } else {
                    user.setNickname("-");
                }
            }
            
            if (obj.has("email")) {
                if (!obj.isNull("email")) {
                    user.setEmail(obj.getString("email") + "");
                } else {
                    user.setEmail("-");
                }
            }
            
            if (obj.has("mobileNumber")) {
                if (!obj.isNull("mobileNumber")) {
                    user.setMobileNumber(obj.getString("mobileNumber") + "");
                } else {
                    user.setMobileNumber("-");
                }
            }
            
            if (obj.has("telNumber")) {
                if (!obj.isNull("telNumber")) {
                    user.setTelNumber(obj.getString("telNumber") + "");
                } else {
                    user.setTelNumber("-");
                }
            }
            
            if (obj.has("sex")) {
                if (!obj.isNull("sex")) {
                    user.setSex(obj.getString("sex") + "");
                } else {
                    user.setSex("-");
                }
            }
            
            if (obj.has("civilStatus")) {
                if (!obj.isNull("civilStatus")) {
                    user.setCivilStatus(obj.getString("civilStatus") + "");
                } else {
                    user.setCivilStatus("-");
                }
            }
            
            if (obj.has("spouseName")) {
                if (!obj.isNull("spouseName")) {
                    user.setSpouseName(obj.getString("spouseName") + "");
                } else {
                    user.setSpouseName("-");
                }
            }
            
            if (obj.has("positionInTheCompany")) {
                if (!obj.isNull("positionInTheCompany")) {
                    user.setPositionInTheCompany(obj.getString("positionInTheCompany") + "");
                } else {
                    user.setPositionInTheCompany("-");
                }
            }
            
            if (obj.has("tin")) {
                if (!obj.isNull("tin")) {
                    user.setTin(obj.getString("tin") + "");
                } else {
                    user.setTin("-");
                }
            }
            
            if (obj.has("mothersMaidenName")) {
                if (!obj.isNull("mothersMaidenName")) {
                    user.setMothersMaidenName(obj.getString("mothersMaidenName") + "");
                } else {
                    user.setMothersMaidenName("-");
                }
            }
            
            if (obj.has("spouseContactNum")) {
                if (!obj.isNull("spouseContactNum")) {
                    user.setSpouseContactNum(obj.getString("spouseContactNum") + "");
                } else {
                    user.setSpouseContactNum("-");
                }
            }
            
            if (obj.has("numberOfChildren")) {
                if (!obj.isNull("numberOfChildren")) {
                    user.setNumberOfChildren(obj.getString("numberOfChildren") + "");
                } else {
                    user.setNumberOfChildren("-");
                }
            }
            
            if (obj.has("education")) {
                if (!obj.isNull("education")) {
                    user.setEducation(obj.getString("education") + "");
                } else {
                    user.setEducation("-");
                }
            }
            
            if (obj.has("lastdSchoolAttended")) {
                if (!obj.isNull("lastdSchoolAttended")) {
                    user.setLastSchoolAttended(obj.getString("lastdSchoolAttended") + "");
                } else {
                    user.setLastSchoolAttended("-");
                }
            }

            if (obj.has("accountContactRoleIdFk")) {
                if (!obj.isNull("accountContactRoleIdFk")) {
                    user.setAccountContactRoleIdFk(obj.getInt("accountContactRoleIdFk") + "");
                } else {
                    user.setAccountContactRoleIdFk("-");
                }
            }
            
            if (obj.has("status")) {
                if (!obj.isNull("status")) {
                    user.setStatus(obj.getString("status") + "");
                } else {
                    user.setStatus("-");
                }
            }
            
            if (obj.has("accountIdFk")) {
                if (!obj.isNull("accountIdFk")) {
                    user.setAccountIdFk(obj.getInt("accountIdFk") + "");
                } else {
                    user.setAccountIdFk("-");
                }
            }
            
            if (obj.has("birthdate")) {
                if (!obj.isNull("birthdate")) {
                    user.setBirthdate(LongToDate_Gregorian(obj.getLong("birthdate")) + "");
                } else {
                    user.setBirthdate("-");
                }
            }
             
            if (obj.has("imageLocation")) {
                if (!obj.isNull("imageLocation")) {
                    user.setImageLocation(obj.getString("imageLocation") + "");
                } else {
                    user.setImageLocation("-");
                }
            }
            
            if (obj.has("imageName")) {
                if (!obj.isNull("imageName")) {
                    user.setImageName(obj.getString("imageName") + "");
                } else {
                    user.setImageName("-");
                }
            }
            
            if (obj.has("type")) {
                if (!obj.isNull("type")) {
                    user.setType(obj.getString("type") + "");
                } else {
                    user.setType("-");
                }
            }
            
            if (obj.has("department")) {
                if (!obj.isNull("department")) {
                    user.setDepartment(obj.getString("department") + "");
                } else {
                    user.setDepartment("-");
                }
            }
            
            
            if (obj.has("age")) {
                if (!obj.isNull("age")) {
                    user.setAge(obj.getString("age") + "");
                } else {
                    user.setAge("-");
                }
            }
            
            if (obj.has("age")) {
                if (!obj.isNull("age")) {
                    user.setAge(obj.getString("age") + "");
                } else {
                    user.setAge("-");
                }
            }
            
                
            if (obj.has("inChargeOf")) {
                if (!obj.isNull("inChargeOf")) {
                    user.setInChargeOf(obj.getString("inChargeOf") + "");
                } else {
                    user.setInChargeOf("-");
                }
            }
            
            if (obj.has("companyName")) {
                if (!obj.isNull("companyName")) {
                    user.setCompanyName(obj.getString("companyName") + "");
                } else {
                    user.setCompanyName("-");
                }
            }
            
            if (obj.has("companyAddress")) {
                if (!obj.isNull("companyAddress")) {
                    user.setCompanyAddress(obj.getString("companyAddress") + "");
                } else {
                    user.setCompanyAddress("-");
                }
            }
            
            if (obj.has("numberOfEmployees")) {
                if (!obj.isNull("numberOfEmployees")) {
                    user.setNumberOfEmployees(obj.getString("numberOfEmployees") + "");
                } else {
                    user.setNumberOfEmployees("-");
                }
            }
            
            if (obj.has("industry")) {
                if (!obj.isNull("industry")) {
                    user.setIndustry(obj.getString("industry") + "");
                } else {
                    user.setIndustry("-");
                }
            }
            
            if (obj.has("companyAnniv")) {
                if (!obj.isNull("companyAnniv")) {
                    user.setCompanyAnniversary(obj.getString("companyAnniv") + "");
                } else {
                    user.setCompanyAnniversary("-");
                }
            }
 
            if (obj.has("multibranch")) {
                if (!obj.isNull("multibranch")) {
                    user.setBranch(obj.getString("multibranch") + "");
                } else {
                    user.setBranch("-");
                }
            }
           
            
             if (obj.has("rewardType")) {
                if (!obj.isNull("rewardType")) {
                    user.setRewardType(obj.getInt("rewardType") + "");
                } else {
                    user.setRewardType("-");
                }
            }
            
             
                
             if (obj.has("metaData")) {
                if (!obj.isNull("metaData")) {
                    user.setMetaData(obj.getString("metaData") + "");
                } else {
                    user.setMetaData("-");
                }
            }
            
            
            
            return user;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public boolean isJSONValid(String test) {
    try {
        JSONObject jsonObject = new JSONObject(test);
    } catch (Exception ex) {
        try {
            JSONArray jsonArray = new JSONArray(test);
        } catch (Exception ex1) {
            return false;
        }
    }
    return true;
}

    public AccountContact userParseFeedForUpdate(String response) {
        
        if(isJSONValid(response)){
            
            try {

                JSONObject obj = new JSONObject(response);
                AccountContact user = new AccountContact();

                if (obj.has("firtsName")) {
                if (!obj.isNull("firtsName")) {
                    user.setFirstName(obj.getString("firtsName") + "");
                } else {
                    user.setFirstName("-");
                }
            }
            
            if (obj.has("middleName")) {
                if (!obj.isNull("middleName")) {
                    user.setMiddleName(obj.getString("middleName") + "");
                } else {
                    user.setMiddleName("-");
                }
            }
            
            if (obj.has("lastName")) {
                if (!obj.isNull("lastName")) {
                    user.setLastName(obj.getString("lastName") + "");
                } else {
                    user.setLastName("-");
                }
            }
            
            if (obj.has("nickName")) {
                if (!obj.isNull("nickName")) {
                    user.setNickname(obj.getString("nickName") + "");
                } else {
                    user.setNickname("-");
                }
            }
            
            if (obj.has("email")) {
                if (!obj.isNull("email")) {
                    user.setEmail(obj.getString("email") + "");
                } else {
                    user.setEmail("-");
                }
            }
            
            if (obj.has("mobileNumber")) {
                if (!obj.isNull("mobileNumber")) {
                    user.setMobileNumber(obj.getString("mobileNumber") + "");
                } else {
                    user.setMobileNumber("-");
                }
            }
            
            if (obj.has("telNumber")) {
                if (!obj.isNull("telNumber")) {
                    user.setTelNumber(obj.getString("telNumber") + "");
                } else {
                    user.setTelNumber("-");
                }
            }
            
            if (obj.has("sex")) {
                if (!obj.isNull("sex")) {
                    user.setSex(obj.getString("sex") + "");
                } else {
                    user.setSex("-");
                }
            }
            
            if (obj.has("civilStatus")) {
                if (!obj.isNull("civilStatus")) {
                    user.setCivilStatus(obj.getString("civilStatus") + "");
                } else {
                    user.setCivilStatus("-");
                }
            }
            
            if (obj.has("spouseName")) {
                if (!obj.isNull("spouseName")) {
                    user.setSpouseName(obj.getString("spouseName") + "");
                } else {
                    user.setSpouseName("-");
                }
            }
            
            if (obj.has("positionInTheCompany")) {
                if (!obj.isNull("positionInTheCompany")) {
                    user.setPositionInTheCompany(obj.getString("positionInTheCompany") + "");
                } else {
                    user.setPositionInTheCompany("-");
                }
            }
            
            if (obj.has("tin")) {
                if (!obj.isNull("tin")) {
                    user.setTin(obj.getString("tin") + "");
                } else {
                    user.setTin("-");
                }
            }
            
            if (obj.has("mothersMaidenName")) {
                if (!obj.isNull("mothersMaidenName")) {
                    user.setMothersMaidenName(obj.getString("mothersMaidenName") + "");
                } else {
                    user.setMothersMaidenName("-");
                }
            }
            
            if (obj.has("spouseContactNum")) {
                if (!obj.isNull("spouseContactNum")) {
                    user.setSpouseContactNum(obj.getString("spouseContactNum") + "");
                } else {
                    user.setSpouseContactNum("-");
                }
            }
            
            if (obj.has("numberOfChildren")) {
                if (!obj.isNull("numberOfChildren")) {
                    user.setNumberOfChildren(obj.getString("numberOfChildren") + "");
                } else {
                    user.setNumberOfChildren("-");
                }
            }
            
            if (obj.has("education")) {
                if (!obj.isNull("education")) {
                    user.setEducation(obj.getString("education") + "");
                } else {
                    user.setEducation("-");
                }
            }
            
            if (obj.has("lastdSchoolAttended")) {
                if (!obj.isNull("lastdSchoolAttended")) {
                    user.setLastSchoolAttended(obj.getString("lastdSchoolAttended") + "");
                } else {
                    user.setLastSchoolAttended("-");
                }
            }

            if (obj.has("accountContactRoleIdFk")) {
                if (!obj.isNull("accountContactRoleIdFk")) {
                    user.setAccountContactRoleIdFk(obj.getInt("accountContactRoleIdFk") + "");
                } else {
                    user.setAccountContactRoleIdFk("-");
                }
            }
            
            if (obj.has("status")) {
                if (!obj.isNull("status")) {
                    user.setStatus(obj.getString("status") + "");
                } else {
                    user.setStatus("-");
                }
            }
            
            if (obj.has("accountIdFk")) {
                if (!obj.isNull("accountIdFk")) {
                    user.setAccountIdFk(obj.getInt("accountIdFk") + "");
                } else {
                    user.setAccountIdFk("-");
                }
            }
            
            if (obj.has("birthdate")) {
                if (!obj.isNull("birthdate")) {
                    user.setBirthdate(LongToDate_Gregorian(obj.getLong("birthdate")) + "");
                } else {
                    user.setBirthdate("-");
                }
            }
             
            if (obj.has("imageLocation")) {
                if (!obj.isNull("imageLocation")) {
                    user.setImageLocation(obj.getString("imageLocation") + "");
                } else {
                    user.setImageLocation("-");
                }
            }
            
            if (obj.has("imageName")) {
                if (!obj.isNull("imageName")) {
                    user.setImageName(obj.getString("imageName") + "");
                } else {
                    user.setImageName("-");
                }
            }
            if (obj.has("type")) {
                    if (!obj.isNull("type")) {
                        user.setType(obj.getString("type") + "");
                    } else {
                        user.setType("-");
                    }
                }
                
            if (obj.has("age")) {
                if (!obj.isNull("age")) {
                    user.setAge(obj.getString("age") + "");
                } else {
                    user.setAge("-");
                }
            }

             if (obj.has("department")) {
                if (!obj.isNull("department")) {
                    user.setDepartment(obj.getString("department") + "");
                } else {
                    user.setDepartment("-");
                }
            }

            if (obj.has("inChargeOf")) {
                if (!obj.isNull("inChargeOf")) {
                    user.setInChargeOf(obj.getString("inChargeOf") + "");
                } else {
                    user.setInChargeOf("-");
                }
            }

            if (obj.has("companyName")) {
                if (!obj.isNull("companyName")) {
                    user.setCompanyName(obj.getString("companyName") + "");
                } else {
                    user.setCompanyName("-");
                }
            }

            if (obj.has("companyAddress")) {
                if (!obj.isNull("companyAddress")) {
                    user.setCompanyAddress(obj.getString("companyAddress") + "");
                } else {
                    user.setCompanyAddress("-");
                }
            }

            if (obj.has("numberOfEmployees")) {
                if (!obj.isNull("numberOfEmployees")) {
                    user.setNumberOfEmployees(obj.getString("numberOfEmployees") + "");
                } else {
                    user.setNumberOfEmployees("-");
                }
            }

            if (obj.has("industry")) {
                if (!obj.isNull("industry")) {
                    user.setIndustry(obj.getString("industry") + "");
                } else {
                    user.setIndustry("-");
                }
            }

            if (obj.has("companyAnniv")) {
                if (!obj.isNull("companyAnniv")) {
                    user.setCompanyAnniversary(LongToDate_Gregorian(obj.getLong("companyAnniv")) + "");
                } else {
                    user.setCompanyAnniversary("-");
                }
            }

            if (obj.has("multibranch")) {
                if (!obj.isNull("multibranch")) {
                    user.setBranch(obj.getString("multibranch") + "");
                } else {
                    user.setBranch("-");
                }
            }

            if (obj.has("topicsInterestedIn")) {
                if (!obj.isNull("topicsInterestedIn")) {
                    user.setTopicsInterestedIn(obj.getString("topicsInterestedIn") + "");
                } else {
                    user.setTopicsInterestedIn("-");
                }
            }

            if (obj.has("gppChecked")) {
                if (!obj.isNull("gppChecked")) {
                    user.setGppChecked(obj.getBoolean("gppChecked") + "");
                } else {
                    user.setGppChecked("-");
                }
            }

            if (obj.has("gtcChecked")) {
                if (!obj.isNull("gtcChecked")) {
                    user.setGtcChecked(obj.getBoolean("gtcChecked") + "");
                } else {
                    user.setGtcChecked("-");
                }
            }
            
            if (obj.has("businessImprovement")) {
                if (!obj.isNull("businessImprovement")) {
                    user.setBusinessImprovement(obj.getString("businessImprovement") + "");
                } else {
                    user.setBusinessImprovement("-");
                }
            }
            if (obj.has("financialAccountIdFk")) {
                   if (!obj.isNull("financialAccountIdFk")) {
                       user.setFinancialAccountIdFk(obj.getString("financialAccountIdFk") + "");
                   } else {
                       user.setFinancialAccountIdFk("-");
                   }
               }


               if (obj.has("updated")) {
                   if (!obj.isNull("updated")) {
                       user.setIsUpdated(obj.getBoolean("updated") + "");
                   } else {
                       user.setIsUpdated("-");
                   }
               }
                
            if (obj.has("rewardType")) {
                if (!obj.isNull("rewardType")) {
                    user.setRewardType(obj.getInt("rewardType") + "");
                } else {
                    user.setRewardType("-");
                }
            }
            
             
                
            if (obj.has("metaData")) {
                if (!obj.isNull("metaData")) {
                    user.setMetaData(obj.getString("metaData") + "");
                } else {
                    user.setMetaData("-");
                }
            }
            
            if (obj.has("denomId")) {
                if (!obj.isNull("denomId")) {
                    user.setDenomId(obj.getString("denomId") + "");
                } else {
                    user.setDenomId("-");
                }
            }
             
             
                return user;
            } catch (JSONException e) {
                System.err.println(e.toString());
                e.printStackTrace();
                return null;
            }
        }else{
            return null;
        }

    }

    public String empObjToJSON(AccountContact accountContact) {
        return "";
    }

   
  public ArrayList<AccountContact> parseAllAccountContact(String response) {
        try {
            JSONArray ar = new JSONArray(response);
            ArrayList<AccountContact> accountContacts = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {
                JSONObject obj = ar.getJSONObject(i);
                AccountContact accountContact = new AccountContact();
                if (obj.has("id")) {
                    if (!obj.isNull("id")) {
                        accountContact.setId(obj.getInt("id") + "");
                    } else {
                        accountContact.setId("-");
                    }
                }
                if (obj.has("firstName")) {
                    if (!obj.isNull("firstName")) {
                        accountContact.setFirstName(obj.getString("firstName") + "");
                    } else {
                        accountContact.setFirstName("-");
                    }
                }

                if (obj.has("middleName")) {
                    if (!obj.isNull("middleName")) {
                        accountContact.setMiddleName(obj.getString("middleName") + "");
                    } else {
                        accountContact.setMiddleName("-");
                    }
                }

                if (obj.has("lastName")) {
                    if (!obj.isNull("lastName")) {
                        accountContact.setLastName(obj.getString("lastName") + "");
                    } else {
                        accountContact.setLastName("-");
                    }
                }

                if (obj.has("nickName")) {
                    if (!obj.isNull("nickName")) {
                        accountContact.setNickname(obj.getString("nickName") + "");
                    } else {
                        accountContact.setNickname("-");
                    }
                }

                if (obj.has("email")) {
                    if (!obj.isNull("email")) {
                        accountContact.setEmail(obj.getString("email") + "");
                    } else {
                        accountContact.setEmail("-");
                    }
                }

                if (obj.has("mobileNumber")) {
                    if (!obj.isNull("mobileNumber")) {
                        accountContact.setMobileNumber(obj.getString("mobileNumber") + "");
                    } else {
                        accountContact.setMobileNumber("-");
                    }
                }

                if (obj.has("telNumber")) {
                    if (!obj.isNull("telNumber")) {
                        accountContact.setTelNumber(obj.getString("telNumber") + "");
                    } else {
                        accountContact.setTelNumber("-");
                    }
                }

                if (obj.has("sex")) {
                    if (!obj.isNull("sex")) {
                        accountContact.setSex(obj.getString("sex") + "");
                    } else {
                        accountContact.setSex("-");
                    }
                }

                if (obj.has("civilStatus")) {
                    if (!obj.isNull("civilStatus")) {
                        accountContact.setCivilStatus(obj.getString("civilStatus") + "");
                    } else {
                        accountContact.setCivilStatus("-");
                    }
                }

                if (obj.has("spouseName")) {
                    if (!obj.isNull("spouseName")) {
                        accountContact.setSpouseName(obj.getString("spouseName") + "");
                    } else {
                        accountContact.setSpouseName("-");
                    }
                }

                if (obj.has("positionInTheCompany")) {
                    if (!obj.isNull("positionInTheCompany")) {
                        accountContact.setPositionInTheCompany(obj.getString("positionInTheCompany") + "");
                    } else {
                        accountContact.setPositionInTheCompany("-");
                    }
                }

                if (obj.has("tin")) {
                    if (!obj.isNull("tin")) {
                        accountContact.setTin(obj.getString("tin") + "");
                    } else {
                        accountContact.setTin("-");
                    }
                }

                if (obj.has("mothersMaidenName")) {
                    if (!obj.isNull("mothersMaidenName")) {
                        accountContact.setMothersMaidenName(obj.getString("mothersMaidenName") + "");
                    } else {
                        accountContact.setMothersMaidenName("-");
                    }
                }

                if (obj.has("spouseContactNum")) {
                    if (!obj.isNull("spouseContactNum")) {
                        accountContact.setSpouseContactNum(obj.getString("spouseContactNum") + "");
                    } else {
                        accountContact.setSpouseContactNum("-");
                    }
                }

                if (obj.has("numberOfChildren")) {
                    if (!obj.isNull("numberOfChildren")) {
                        accountContact.setNumberOfChildren(obj.getString("numberOfChildren") + "");
                    } else {
                        accountContact.setNumberOfChildren("-");
                    }
                }

                if (obj.has("education")) {
                    if (!obj.isNull("education")) {
                        accountContact.setEducation(obj.getString("education") + "");
                    } else {
                        accountContact.setEducation("-");
                    }
                }

                if (obj.has("lastdSchoolAttended")) {
                    if (!obj.isNull("lastdSchoolAttended")) {
                        accountContact.setLastSchoolAttended(obj.getString("lastdSchoolAttended") + "");
                    } else {
                        accountContact.setLastSchoolAttended("-");
                    }
                }

                if (obj.has("accountContactRoleIdFk")) {
                    if (!obj.isNull("accountContactRoleIdFk")) {
                        accountContact.setAccountContactRoleIdFk(obj.getInt("accountContactRoleIdFk") + "");
                    } else {
                        accountContact.setAccountContactRoleIdFk("-");
                    }
                }

                if (obj.has("status")) {
                    if (!obj.isNull("status")) {
                        accountContact.setStatus(obj.getString("status") + "");
                    } else {
                        accountContact.setStatus("-");
                    }
                }

                if (obj.has("accountIdFk")) {
                    if (!obj.isNull("accountIdFk")) {
                        accountContact.setAccountIdFk(obj.getInt("accountIdFk") + "");
                    } else {
                        accountContact.setAccountIdFk("-");
                    }
                }

                if (obj.has("birthdate")) {
                    if (!obj.isNull("birthdate")) {
                        accountContact.setBirthdate(LongToDate_Gregorian(obj.getLong("birthdate")) + "");
                    } else {
                        accountContact.setBirthdate("-");
                    }
                }

                if (obj.has("imageLocation")) {
                    if (!obj.isNull("imageLocation")) {
                        accountContact.setImageLocation(obj.getString("imageLocation") + "");
                    } else {
                        accountContact.setImageLocation("-");
                    }
                }

                if (obj.has("imageName")) {
                    if (!obj.isNull("imageName")) {
                        accountContact.setImageName(obj.getString("imageName") + "");
                    } else {
                        accountContact.setImageName("-");
                    }
                }
                
                if (obj.has("type")) {
                    if (!obj.isNull("type")) {
                        accountContact.setType(obj.getString("type") + "");
                    } else {
                        accountContact.setType("-");
                    }
                }
                
                if (obj.has("age")) {
                    if (!obj.isNull("age")) {
                        accountContact.setAge(obj.getString("age") + "");
                    } else {
                        accountContact.setAge("-");
                    }
                }
                
                 if (obj.has("department")) {
                    if (!obj.isNull("department")) {
                        accountContact.setDepartment(obj.getString("department") + "");
                    } else {
                        accountContact.setDepartment("-");
                    }
                }
                 
                if (obj.has("inChargeOf")) {
                    if (!obj.isNull("inChargeOf")) {
                        accountContact.setInChargeOf(obj.getString("inChargeOf") + "");
                    } else {
                        accountContact.setInChargeOf("-");
                    }
                }

                if (obj.has("companyName")) {
                    if (!obj.isNull("companyName")) {
                        accountContact.setCompanyName(obj.getString("companyName") + "");
                    } else {
                        accountContact.setCompanyName("-");
                    }
                }

                if (obj.has("companyAddress")) {
                    if (!obj.isNull("companyAddress")) {
                        accountContact.setCompanyAddress(obj.getString("companyAddress") + "");
                    } else {
                        accountContact.setCompanyAddress("-");
                    }
                }

                if (obj.has("numberOfEmployees")) {
                    if (!obj.isNull("numberOfEmployees")) {
                        accountContact.setNumberOfEmployees(obj.getString("numberOfEmployees") + "");
                    } else {
                        accountContact.setNumberOfEmployees("-");
                    }
                }

                if (obj.has("industry")) {
                    if (!obj.isNull("industry")) {
                        accountContact.setIndustry(obj.getString("industry") + "");
                    } else {
                        accountContact.setIndustry("-");
                    }
                }

                if (obj.has("companyAnniv")) {
                    if (!obj.isNull("companyAnniv")) {
                        accountContact.setCompanyAnniversary(LongToDate_Gregorian(obj.getLong("companyAnniv")) + "");
                    } else {
                        accountContact.setCompanyAnniversary("-");
                    }
                }

                if (obj.has("multibranch")) {
                    if (!obj.isNull("multibranch")) {
                        accountContact.setBranch(obj.getString("multibranch") + "");
                    } else {
                        accountContact.setBranch("-");
                    }
                }
                
                if (obj.has("topicsInterestedIn")) {
                    if (!obj.isNull("topicsInterestedIn")) {
                        accountContact.setTopicsInterestedIn(obj.getString("topicsInterestedIn") + "");
                    } else {
                        accountContact.setTopicsInterestedIn("-");
                    }
                }
             
                if (obj.has("gppChecked")) {
                    if (!obj.isNull("gppChecked")) {
                        accountContact.setGppChecked(obj.getBoolean("gppChecked") + "");
                    } else {
                        accountContact.setGppChecked("-");
                    }
                }
             
                if (obj.has("gtcChecked")) {
                    if (!obj.isNull("gtcChecked")) {
                        accountContact.setGtcChecked(obj.getBoolean("gtcChecked") + "");
                    } else {
                        accountContact.setGtcChecked("-");
                    }
                }
              
                if (obj.has("businessImprovement")) {
                    if (!obj.isNull("businessImprovement")) {
                        accountContact.setBusinessImprovement(obj.getString("businessImprovement") + "");
                    } else {
                        accountContact.setBusinessImprovement("-");
                    }
                }
                
                if (obj.has("financialAccountIdFk")) {
                    if (!obj.isNull("financialAccountIdFk")) {
                        accountContact.setFinancialAccountIdFk(obj.getString("financialAccountIdFk") + "");
                    } else {
                        accountContact.setFinancialAccountIdFk("-");
                    }
                }
                
                
                if (obj.has("updated")) {
                    if (!obj.isNull("updated")) {
                        accountContact.setIsUpdated(obj.getBoolean("updated") + "");
                    } else {
                        accountContact.setIsUpdated("-");
                    }
                }
                  if (obj.has("rewardType")) {
                    if (!obj.isNull("rewardType")) {
                        accountContact.setRewardType(obj.getInt("rewardType") + "");
                    } else {
                        accountContact.setRewardType("-");
                    }
                }



                if (obj.has("metaData")) {
                    if (!obj.isNull("metaData")) {
                        accountContact.setMetaData(obj.getString("metaData") + "");
                    } else {
                        accountContact.setMetaData("-");
                    }
                }

                if (obj.has("denomId")) {
                    if (!obj.isNull("denomId")) {
                        accountContact.setDenomId(obj.getInt("denomId") + "");
                    } else {
                        accountContact.setDenomId("-");
                    }
                }
                
                if (obj.has("otpNumber")) {
                    if (!obj.isNull("otpNumber")) {
                        accountContact.setOtpNumber(obj.getString("otpNumber") + "");
                    } else {
                        accountContact.setOtpNumber("-");
                    }
                }
                
                
                accountContacts.add(accountContact);
            }
            return accountContacts;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    public String LongtoDateString(long dateInMillis) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMillis);
        return formatter.format(calendar.getTime());
    }

    public String LongtoString(long dateInMillis) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMillis);
        return formatter.format(calendar.getTime());
    }

    public String LongtoStringWithTime(long dateInMillis) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMillis);
        return formatter.format(calendar.getTime());
    }

    public static String LongToDate_Gregorian(long dateInMillis) {
        SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMillis);
        return formatter.format(calendar.getTime());
    }
}
