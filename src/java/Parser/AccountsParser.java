/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

import Bean.Accounts;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Adrian Paul
 */
public class AccountsParser {
     public Accounts accountsParseFeed(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            Accounts ct = new Accounts();
            if (obj.has("companyName")) {
                if (!obj.isNull("companyName")) {
                    ct.setCompanyName(obj.getString("companyName") + "");
                } else {
                    ct.setCompanyName("-");
                }
            }


            return ct;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
   public ArrayList<Accounts> parseAllAccounts(String response) throws JSONException, ParseException {
        try {
            JSONArray ar = new JSONArray(response);
            ArrayList<Accounts> accounts = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {
                JSONObject obj = ar.getJSONObject(i);
                Accounts account = new Accounts();
                
                if (obj.has("companyName")) {
                    if (!obj.isNull("companyName")) {
                        account.setCompanyName(obj.getInt("companyName") + "");
                    } else {
                        account.setCompanyName("");
                    }
                }
                

                accounts.add(account);
            }
            return accounts;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
}
