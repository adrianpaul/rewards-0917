/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

import Bean.Department;
import java.text.ParseException;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Adrian Paul
 */
public class DepartmentParser {
    
     public ArrayList<Department> parseAllDepartment(String response) throws JSONException, ParseException {
        try {
            JSONArray ar = new JSONArray(response);
            ArrayList<Department> departments = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {
                JSONObject obj = ar.getJSONObject(i);
                Department department = new Department();
                
                if (obj.has("id")) {
                    if (!obj.isNull("id")) {
                        department.setID(obj.getInt("id") + "");
                    } else {
                        department.setID("-");
                    }
                }
                
               
                if (obj.has("name")) {
                    if (!obj.isNull("name")) {
                        department.setName(obj.getString("name") + "");
                    } else {
                        department.setName("-");
                    }
                }
                
                if (obj.has("description")) {
                    if (!obj.isNull("telNumber")) {
                        department.setDescription(obj.getString("description") + "");
                    } else {
                        department.setDescription("-");
                    }
                }
                
                 if (obj.has("isActive")) {
                    if (!obj.isNull("isActive")) {
                        department.setIsActive(obj.getBoolean("isActive") + "");
                    } else {
                        department.setIsActive("-");
                    }
                }

               

                departments.add(department);
            }
            return departments;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
}
