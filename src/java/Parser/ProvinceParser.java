/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

import Bean.Province;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author sgmwpsupport
 */
public class ProvinceParser {
        public Province regionParseFeed(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            Province province = new Province();
            if (obj.has("id")) {
                if (!obj.isNull("id")) {
                    province.setId(obj.getInt("id") + "");
                } else {
                    province.setId("-");
                }
            }

            if (obj.has("name")) {
                if (!obj.isNull("name")) {
                    province.setName(obj.getString("name") + "");
                } else {
                    province.setName("-");
                }
            }

            if (obj.has("shortCode")) {
                if (!obj.isNull("shortCode")) {
                    province.setShortCode(obj.getString("shortCode") + "");
                } else {
                    province.setShortCode("-");
                }
            }

            if (obj.has("regionIdFk")) {
                if (!obj.isNull("regionIdFk")) {
                    province.setShortCode(obj.getInt("regionIdFk") + "");
                } else {
                    province.setRegionIdFk("-");
                }
            }

            return province;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Province> parseAllProvinces(String response) {
        try {
            JSONArray ar = new JSONArray(response);
            ArrayList<Province> provinces = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {
                JSONObject obj = ar.getJSONObject(i);
                Province province = new Province();

                if (obj.has("id")) {
                    if (!obj.isNull("id")) {
                        province.setId(obj.getInt("id") + "");
                    } else {
                        province.setId("-");
                    }
                }

                if (obj.has("name")) {
                    if (!obj.isNull("name")) {
                        province.setName(obj.getString("name") + "");
                    } else {
                        province.setName("-");
                    }
                }

                if (obj.has("shortCode")) {
                    if (!obj.isNull("shortCode")) {
                        province.setShortCode(obj.getString("shortCode") + "");
                    } else {
                        province.setShortCode("-");
                    }
                }

                if (obj.has("regionIdFk")) {
                    if (!obj.isNull("regionIdFk")) {
                        province.setRegionIdFk(obj.getInt("regionIdFk") + "");
                    } else {
                        province.setRegionIdFk("-");
                    }
                }
                provinces.add(province);
            }
            return provinces;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
