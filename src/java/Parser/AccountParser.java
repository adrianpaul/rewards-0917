/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

import Bean.Account;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Adrian Paul
 */
public class AccountParser {
    
   public ArrayList<Account> parseAllAccount(String response) throws JSONException, ParseException {
        try {
            JSONArray ar = new JSONArray(response);
            ArrayList<Account> accounts = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {
                JSONObject obj = ar.getJSONObject(i);
                Account account = new Account();
                
                if (obj.has("accountIdFk")) {
                    if (!obj.isNull("accountIdFk")) {
                        account.setAccountIdFk(obj.getInt("accountIdFk") + "");
                    } else {
                        account.setAccountIdFk("-");
                    }
                }
                
                if (obj.has("accountContactIdFk")) {
                    if (!obj.isNull("accountContactIdFk")) {
                        account.setAccountContactIdFk(obj.getInt("accountContactIdFk") + "");
                    } else {
                        account.setAccountContactIdFk("-");
                    }
                }
                
                if (obj.has("mobileNumber")) {
                    if (!obj.isNull("mobileNumber")) {
                        account.setMobileNumber(obj.getString("mobileNumber") + "");
                    } else {
                        account.setMobileNumber("-");
                    }
                }
                
                if (obj.has("telNumber")) {
                    if (!obj.isNull("telNumber")) {
                        account.setTelNumber(obj.getString("telNumber") + "");
                    } else {
                        account.setTelNumber("-");
                    }
                }

                if (obj.has("email")) {
                    if (!obj.isNull("email")) {
                        account.setEmail(obj.getString("email") + "");
                    } else {
                        account.setEmail("-");
                    }
                }

                accounts.add(account);
            }
            return accounts;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
}
