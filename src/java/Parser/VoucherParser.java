/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

import Bean.Voucher;
import static Parser.AccountContactParser.LongToDate_Gregorian;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Adrian Paul
 */
public class VoucherParser {
    
     public ArrayList<Voucher> parseAllVoucher(String response) throws JSONException, ParseException {
        try {
            JSONArray ar = new JSONArray(response);
            ArrayList<Voucher> industries = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {
                JSONObject obj = ar.getJSONObject(i);
                Voucher industry = new Voucher();
                
                if (obj.has("id")) {
                    if (!obj.isNull("id")) {
                        industry.setId(obj.getInt("id") + "");
                    } else {
                        industry.setId("-");
                    }
                }
                
               
                if (obj.has("name")) {
                    if (!obj.isNull("name")) {
                        industry.setName(obj.getString("name") + "");
                    } else {
                        industry.setName("-");
                    }
                }
                
                if (obj.has("description")) {
                    if (!obj.isNull("description")) {
                        industry.setDescription(obj.getString("description") + "");
                    } else {
                        industry.setDescription("-");
                    }
                }
                
                if (obj.has("category")) {
                    if (!obj.isNull("category")) {
                        industry.setCategory(obj.getString("category") + "");
                    } else {
                        industry.setCategory("-");
                    }
                }
                
                if (obj.has("code")) {
                    if (!obj.isNull("code")) {
                        industry.setCode(obj.getString("code") + "");
                    } else {
                        industry.setCode("-");
                    }
                }
                
                if (obj.has("counter")) {
                    if (!obj.isNull("counter")) {
                        industry.setCounter(obj.getInt("counter") + "");
                    } else {
                        industry.setCounter("-");
                    }
                }
          
                if (obj.has("startDate")) {
                    if (!obj.isNull("startDate")) {
                        industry.setStartDate(LongToDate_Gregorian(obj.getLong("startDate")) + "");
                    } else {
                        industry.setStartDate("-");
                    }
                }
               
                if (obj.has("endDate")) {
                    if (!obj.isNull("endDate")) {
                        industry.setEndDate(LongToDate_Gregorian(obj.getLong("endDate")) + "");
                    } else {
                        industry.setEndDate("-");
                    }
                }
               
                if (obj.has("isActive")) {
                    if (!obj.isNull("isActive")) {
                        industry.setIsActive(obj.getInt("isActive") + "");
                    } else {
                        industry.setIsActive("-");
                    }
                }
                
                
                if (obj.has("byGroup")) {
                    if (!obj.isNull("byGroup")) {
                        industry.setByGroup(obj.getString("byGroup") + "");
                    } else {
                        industry.setByGroup("-");
                    }
                }
                
                if (obj.has("amount")) {
                    if (!obj.isNull("amount")) {
                        industry.setAmount(obj.getString("amount") + "");
                    } else {
                        industry.setAmount("-");
                    }
                }
                
                if (obj.has("status")) {
                    if (!obj.isNull("status")) {
                        industry.setStatus(obj.getString("status") + "");
                    } else {
                        industry.setStatus("-");
                    }
                }
                
                if (obj.has("voucherCode")) {
                    if (!obj.isNull("voucherCode")) {
                        industry.setVoucherCode(obj.getString("voucherCode") + "");
                    } else {
                        industry.setVoucherCode("-");
                    }
                }
                industries.add(industry);
            }
            return industries;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public String LongtoDateString(long dateInMillis) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMillis);
        return formatter.format(calendar.getTime());
    }

    public String LongtoString(long dateInMillis) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMillis);
        return formatter.format(calendar.getTime());
    }

    public String LongtoStringWithTime(long dateInMillis) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMillis);
        return formatter.format(calendar.getTime());
    }

    public static String LongToDate_Gregorian(long dateInMillis) {
        SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMillis);
        return formatter.format(calendar.getTime());
    }
}
