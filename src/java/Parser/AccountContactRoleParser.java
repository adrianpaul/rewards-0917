/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

import Bean.AccountContactRole;
import java.text.ParseException;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Adrian Paul
 */
public class AccountContactRoleParser {
    
     public ArrayList<AccountContactRole> parseAllAccountContactRole(String response) throws JSONException, ParseException {
        try {
            JSONArray ar = new JSONArray(response);
            ArrayList<AccountContactRole> accounts = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {
                JSONObject obj = ar.getJSONObject(i);
                AccountContactRole account = new AccountContactRole();
                
                if (obj.has("id")) {
                    if (!obj.isNull("id")) {
                        account.setID(obj.getInt("id") + "");
                    } else {
                        account.setID("-");
                    }
                }
                
               
                if (obj.has("name")) {
                    if (!obj.isNull("name")) {
                        account.setName(obj.getString("name") + "");
                    } else {
                        account.setName("-");
                    }
                }
                
                if (obj.has("description")) {
                    if (!obj.isNull("telNumber")) {
                        account.setDescription(obj.getString("description") + "");
                    } else {
                        account.setDescription("-");
                    }
                }

               

                accounts.add(account);
            }
            return accounts;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
}
