/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

import Bean.FinancialAccount;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Adrian Paul
 */
public class FinancialAccountParser {
    
   public ArrayList<FinancialAccount> parseAllFinancialAccount(String response) throws JSONException, ParseException {
        try {
            JSONArray ar = new JSONArray(response);
            ArrayList<FinancialAccount> accounts = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {
                JSONObject obj = ar.getJSONObject(i);
                FinancialAccount account = new FinancialAccount();
                
                if (obj.has("accountIdFk")) {
                    if (!obj.isNull("accountIdFk")) {
                        account.setAccountIdFk(obj.getInt("accountIdFk") + "");
                    } else {
                        account.setAccountIdFk("-");
                    }
                }
                
                if (obj.has("id")) {
                    if (!obj.isNull("id")) {
                        account.setId(obj.getInt("id") + "");
                    } else {
                        account.setId("-");
                    }
                }
                
                if (obj.has("financialAccountId")) {
                    if (!obj.isNull("financialAccountId")) {
                        account.setFinancialAccountId(obj.getString("financialAccountId") + "");
                    } else {
                        account.setFinancialAccountId("-");
                    }
                }
                
                

                accounts.add(account);
            }
            return accounts;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
}
