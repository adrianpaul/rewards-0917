/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

import Bean.Industry;
import java.text.ParseException;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Adrian Paul
 */
public class IndustryParser {
    
     public ArrayList<Industry> parseAllIndustry(String response) throws JSONException, ParseException {
        try {
            JSONArray ar = new JSONArray(response);
            ArrayList<Industry> industries = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {
                JSONObject obj = ar.getJSONObject(i);
                Industry industry = new Industry();
                
                if (obj.has("id")) {
                    if (!obj.isNull("id")) {
                        industry.setID(obj.getInt("id") + "");
                    } else {
                        industry.setID("-");
                    }
                }
                
               
                if (obj.has("name")) {
                    if (!obj.isNull("name")) {
                        industry.setName(obj.getString("name") + "");
                    } else {
                        industry.setName("-");
                    }
                }
                
                if (obj.has("description")) {
                    if (!obj.isNull("description")) {
                        industry.setDescription(obj.getString("description") + "");
                    } else {
                        industry.setDescription("-");
                    }
                }
                
          

               

                industries.add(industry);
            }
            return industries;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
}
