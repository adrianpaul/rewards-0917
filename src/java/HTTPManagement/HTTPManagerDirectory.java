/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HTTPManagement;

import Bean.AccountContact;
import Bean.Voucher;
import Others.Constants;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author sgmwpsupport
 */
public class HTTPManagerDirectory {

  
    public String executeGetRequest(String targetURL, String method, String email, String pw) throws MalformedURLException, IOException {
        HttpsTrustManager.allowAllSSL();
        final String user = email;
        final String password = pw;

        Base64.Encoder encoder = Base64.getEncoder();
        URL url = new URL(targetURL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStream is = null;

        try {
            // Create connection
            connection.setReadTimeout(50000);
            connection.setConnectTimeout(50000);
            connection.setRequestMethod(method);
            connection.setDoInput(true);
            String credentials = user + ":" + password;

            connection.setRequestProperty("Content-Type",
                    "application/json;");
            connection.setRequestProperty("Content-Language", "en-US");

            String base64EncodedCredentials = encoder.encodeToString(credentials.getBytes());
            connection.setRequestProperty("Authorization", "Basic " + base64EncodedCredentials);

            connection.connect();

            // Get Response  
            Integer responsecode = connection.getResponseCode();

            is = connection.getInputStream();

            String contentAsString = convertInputStreamToString(is);

//            System.out.println(targetURL + " || " + responsecode);
            if (responsecode != 200 || "".equals(contentAsString)) {
                connection.disconnect();
                return "";
            } else {
                return contentAsString;
            }
        } catch (Exception e) {
            connection.disconnect();
            e.printStackTrace();
            return "";
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }
    
    
   public void sendOTP(String targetURL, String method, String email, String password) throws IOException, ParseException {
        HttpsTrustManager.allowAllSSL();
        
        String user = email;
        String pass = password;
        
        URL url = new URL(targetURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        
        InputStream is = null;
        String response = "";
        
        try {
            JSONObject savePipeline = new JSONObject();

            savePipeline.put("id", 0);
            savePipeline.put("email", email + "");
//   
            String body = savePipeline.toString();
//            System.out.println("Body: " + body);

            conn.setReadTimeout(40000);
            conn.setConnectTimeout(40000);
            conn.setRequestMethod(method);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            String credentials = user + ":" + pass;

            Base64.Encoder encoder = Base64.getEncoder();
            String base64EncodedCredentials = encoder.encodeToString(credentials.getBytes()); //base64 encoding for accessing URLs with authentication
            conn.setRequestProperty("Authorization", "Basic " + base64EncodedCredentials);

            String urlParameters = body;
            byte[] postData = urlParameters.getBytes(Charset.forName("UTF-8"));
            int postDataLength = postData.length;

            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setUseCaches(false);
            
            
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write(postData);
            }

            int responseCode = conn.getResponseCode();
            
//            System.out.println(userAuth.getEmployeeIdFk()+ " " + responseCode);
            
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            }
//            System.out.println(response + " " + responseCode);

        } catch (JSONException | NumberFormatException | IOException e) {
//            System.out.println("FAILED" + " " + response + conn.getContent().toString());
        }
    }
   
   
    public int executeAddAccountContact(String targetURL, AccountContact accountContact, String method, String email, String pw) throws IOException, ParseException {
        HttpsTrustManager.allowAllSSL();

        String user = email;
        String pass = pw;

        URL url = new URL(targetURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        InputStream is = null;
        String response = "";

        JSONObject userJson = new JSONObject();

        try {
            
            userJson.put("accountContactIdFk", accountContact.getAccountContactIdFk().toUpperCase() + "");
            userJson.put("firstName", accountContact.getFirstName().toUpperCase() + "");
            userJson.put("lastName", accountContact.getLastName().toUpperCase() + "");
            userJson.put("middleName", accountContact.getMiddleName().toUpperCase() + "");
            userJson.put("nickName", accountContact.getNickname().toUpperCase() + "");
            userJson.put("email", accountContact.getEmail() + "");
            userJson.put("mobileNumber", accountContact.getMobileNumber() + "");
            userJson.put("telNumber", accountContact.getTelNumber()+ "");
            userJson.put("sex", accountContact.getSex()+ "");
            userJson.put("civilStatus", accountContact.getCivilStatus()+ "");
            userJson.put("spouseName", accountContact.getSpouseName()+ "");
            userJson.put("positionInTheCompany", accountContact.getPositionInTheCompany()+ "");
            userJson.put("tin", accountContact.getTin()+ "");
            userJson.put("mothersMaidenName", accountContact.getMothersMaidenName()+ "");
            userJson.put("spouseContactNum", accountContact.getSpouseContactNum()+ "");
            userJson.put("numberOfChildren", accountContact.getNumberOfChildren()+ "");
            userJson.put("education", accountContact.getEducation()+ "");
            userJson.put("lastSchoolAttended", accountContact.getLastSchoolAttended()+ "");
            userJson.put("accountContactRoleIdFk", Integer.parseInt(accountContact.getAccountContactRoleIdFk()));
            userJson.put("status", accountContact.getStatus()+ "");
            userJson.put("accountIdFk", Integer.parseInt(accountContact.getAccountIdFk()));
            userJson.put("birthdate", HTMLDatetoCerebroDate(accountContact.getBirthdate()));
            userJson.put("imageLocation", accountContact.getImageLocation()+ "");
            userJson.put("imageName", accountContact.getImageName()+ "");
            
            
            
        } catch (JSONException ex) {
            Logger.getLogger(HTTPManagerDirectory.class.getName()).log(Level.SEVERE, null, ex);
        }

        String body = userJson.toString();
        System.out.println("JSON BODY: " +  body);
        conn.setReadTimeout(40000);
        conn.setConnectTimeout(40000);
        conn.setRequestMethod(method);
        conn.setDoInput(true);
        conn.setDoOutput(true);

        String credentials = user + ":" + pass;

        Base64.Encoder encoder = Base64.getEncoder();
        String base64EncodedCredentials = encoder.encodeToString(credentials.getBytes()); //base64 encoding for accessing URLs with authentication
        conn.setRequestProperty("Authorization", "Basic " + base64EncodedCredentials);

        String urlParameters = body;
        byte[] postData = urlParameters.getBytes(Charset.forName("UTF-8"));
        int postDataLength = postData.length;
        
        System.out.println("postDataLength: "+postDataLength);
        conn.setDoOutput(true);
        conn.setInstanceFollowRedirects(false);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("charset", "utf-8");
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setUseCaches(false);

        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(postData);
        }

        int responseCode = conn.getResponseCode();

        if (responseCode == HttpsURLConnection.HTTP_OK) {
            String line;
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = br.readLine()) != null) {
                response += line;
            }

        } else {
        }
//        System.out.println(response + " " + responseCode);
        return responseCode;
    }
    
    public void executeAddKYC(String targetURL, AccountContact accountContact, String method, String email, String password) throws IOException, ParseException {
        HttpsTrustManager.allowAllSSL();

        String user = email;
        String pass = password;

        URL url = new URL(targetURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        InputStream is = null;
        String response = "";

        try {
            JSONObject userJson = new JSONObject();
            userJson.put("accountContactIdFk", accountContact.getAccountContactIdFk() + "");
            userJson.put("accountContactRoleIdFk", Integer.parseInt(accountContact.getAccountContactRoleIdFk()));
            userJson.put("accountIdFk", Integer.parseInt(accountContact.getAccountIdFk()));
            userJson.put("firstName", accountContact.getFirstName().toUpperCase() + "");
            userJson.put("lastName", accountContact.getLastName().toUpperCase() + "");
            userJson.put("email", accountContact.getEmail() + "");
            userJson.put("mobileNumber", accountContact.getMobileNumber() + "");
            userJson.put("sex", accountContact.getSex()+ "");
            userJson.put("positionInTheCompany", accountContact.getPositionInTheCompany()+ "");
            userJson.put("birthdate", HTMLDatetoCerebroDate(accountContact.getBirthdate()));
            userJson.put("department", accountContact.getDepartment()+ "");
            userJson.put("type", accountContact.getType()+ "");
            userJson.put("companyAnniv",  HTMLCompanyAniversaryDatetoCerebroDate(accountContact.getCompanyAnniversary())+ "");
            //userJson.put("age", accountContact.getAge()+ "");
            userJson.put("inChargeOf", accountContact.getInChargeOf()+ "");
            userJson.put("multibranch", accountContact.getBranch()+ "");
            userJson.put("companyName", accountContact.getCompanyName()+ "");
            //userJson.put("companyAddress", accountContact.getCompanyAddress()+ "");
            userJson.put("industry", accountContact.getIndustry()+ "");
            userJson.put("numberOfEmployee", accountContact.getNumberOfEmployees()+ "");
            userJson.put("topicsInterestedIn", accountContact.getTopicsInterestedIn()+ "");
            userJson.put("gppChecked", Boolean.valueOf(accountContact.getGppChecked())+ "");
            userJson.put("businessImprovement",accountContact.getBusinessImprovement()+ "");
            userJson.put("financialAccountIdFk",accountContact.getFinancialAccountIdFk()+ "");
            userJson.put("updated",Boolean.valueOf(accountContact.getIsUpdated())+ "");
            userJson.put("rewardType",accountContact.getRewardType()+ "");
            userJson.put("metaData",accountContact.getMetadata()+ "");
            userJson.put("denomId",accountContact.getDenomId()+ "");
            userJson.put("otpNumber", accountContact.getOtpNumber()+ "");
            userJson.put("kycCampaignIdFk", accountContact.getKycCampaignIdFk()+ "");
            
           
            
            
     
            

            String body = userJson.toString();
            System.out.println("body: " + body);
            conn.setReadTimeout(40000);
            conn.setConnectTimeout(40000);
            conn.setRequestMethod(method);
            conn.setDoInput(true);
            conn.setDoOutput(true);

//            System.out.println("----");
            String credentials = user + ":" + pass;
//            System.out.println(credentials);

            Base64.Encoder encoder = Base64.getEncoder();
            String base64EncodedCredentials = encoder.encodeToString(credentials.getBytes()); //base64 encoding for accessing URLs with authentication
            conn.setRequestProperty("Authorization", "Basic " + base64EncodedCredentials);

            String urlParameters = body;
            byte[] postData = urlParameters.getBytes(Charset.forName("UTF-8"));
            int postDataLength = postData.length;
            System.out.println("urlParameters: " + urlParameters);
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setUseCaches(false);
            System.out.println("postDataLength: " + postDataLength);
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write(postData);
            }

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            }
          
            System.out.println("adding KYC: " +response + " " + responseCode);

        } catch (JSONException | NumberFormatException | IOException e) {
            System.out.println("FAILED" + e.toString() + " " + response + conn.getContent().toString());
        }
    }

    
    public void executeAddUseTheLine(String targetURL, AccountContact accountContact, String method, String email, String password) throws IOException, ParseException {
        HttpsTrustManager.allowAllSSL();

        String user = email;
        String pass = password;

        URL url = new URL(targetURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        InputStream is = null;
        String response = "";

        try {
            JSONObject userJson = new JSONObject();
            userJson.put("accountContactIdFk", accountContact.getAccountContactIdFk() + "");
            userJson.put("firstName", accountContact.getFirstName().toUpperCase() + "");
            userJson.put("lastName", accountContact.getLastName().toUpperCase() + "");
            userJson.put("email", accountContact.getEmail() + "");
            userJson.put("mobileNumber", accountContact.getMobileNumber() + "");
            userJson.put("sex", accountContact.getSex()+ "");
            userJson.put("positionInTheCompany", accountContact.getPositionInTheCompany()+ "");
            userJson.put("accountContactRoleIdFk", Integer.parseInt(accountContact.getAccountContactRoleIdFk()));
            userJson.put("accountIdFk", Integer.parseInt(accountContact.getAccountIdFk()));
            userJson.put("birthdate", HTMLDatetoCerebroDate(accountContact.getBirthdate()));
            userJson.put("type", accountContact.getType()+ "");
            userJson.put("department", accountContact.getDepartment()+ "");
            userJson.put("inChargeOf", accountContact.getInChargeOf()+ "");
            userJson.put("topicsInterestedIn", accountContact.getTopicsInterestedIn()+ "");
            userJson.put("gppChecked", Boolean.valueOf(accountContact.getGppChecked())+ "");
            userJson.put("financialAccountIdFk",accountContact.getFinancialAccountIdFk()+ "");
            userJson.put("updated",Boolean.valueOf(accountContact.getIsUpdated())+ "");
            userJson.put("rewardType", accountContact.getRewardType()+ "");
            userJson.put("metaData", accountContact.getMetadata()+ "");
            userJson.put("otpNumber", accountContact.getOtpNumber()+ "");
            userJson.put("kycCampaignIdFk", accountContact.getKycCampaignIdFk()+ "");
            
            
            
          
     
            

            String body = userJson.toString();
            System.out.println("body: " + body);
            conn.setReadTimeout(40000);
            conn.setConnectTimeout(40000);
            conn.setRequestMethod(method);
            conn.setDoInput(true);
            conn.setDoOutput(true);

//            System.out.println("----");
            String credentials = user + ":" + pass;
//            System.out.println(credentials);

            Base64.Encoder encoder = Base64.getEncoder();
            String base64EncodedCredentials = encoder.encodeToString(credentials.getBytes()); //base64 encoding for accessing URLs with authentication
            conn.setRequestProperty("Authorization", "Basic " + base64EncodedCredentials);

            String urlParameters = body;
            byte[] postData = urlParameters.getBytes(Charset.forName("UTF-8"));
            int postDataLength = postData.length;
            System.out.println("urlParameters: " + urlParameters);
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setUseCaches(false);
            System.out.println("postDataLength: " + postDataLength);
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write(postData);
            }

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            }
          
            System.out.println("adding KYC: " +response + " " + responseCode);

        } catch (JSONException | NumberFormatException | IOException e) {
            System.out.println("FAILED" + e.toString() + " " + response + conn.getContent().toString());
        }
    }
    
    public void executeAddVoucherHistory(String targetURL, Voucher voucher, String method, String email, String password) throws IOException, ParseException {
        HttpsTrustManager.allowAllSSL();

        String user = email;
        String pass = password;

        URL url = new URL(targetURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        InputStream is = null;
        String response = "";

        try {
            JSONObject userJson = new JSONObject();
            
          
            userJson.put("id", Integer.parseInt(voucher.getId()));
            userJson.put("accountContactIdFk", Integer.parseInt(voucher.getId()));
            userJson.put("email", voucher.getEmail()+ "");
            userJson.put("mobileNumber", voucher.getMobileNumber()+ "");
            userJson.put("voucherCode", voucher.getVoucherCode());
            userJson.put("status", voucher.getStatus());
           
            

            String body = userJson.toString();
            conn.setReadTimeout(40000);
            conn.setConnectTimeout(40000);
            conn.setRequestMethod(method);
            conn.setDoInput(true);
            conn.setDoOutput(true);

//            System.out.println("----");
            String credentials = user + ":" + pass;
//            System.out.println(credentials);

            Base64.Encoder encoder = Base64.getEncoder();
            String base64EncodedCredentials = encoder.encodeToString(credentials.getBytes()); //base64 encoding for accessing URLs with authentication
            conn.setRequestProperty("Authorization", "Basic " + base64EncodedCredentials);

            String urlParameters = body;
            byte[] postData = urlParameters.getBytes(Charset.forName("UTF-8"));
            int postDataLength = postData.length;
            System.out.println("urlParameters: " + urlParameters);
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setUseCaches(false);
            System.out.println("postDataLength: " + postDataLength);
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write(postData);
            }

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            }
          
            System.out.println("Adding Voucher: " +response + " " + responseCode);

        } catch (JSONException | NumberFormatException | IOException e) {
            System.out.println("FAILED" + e.toString() + " " + response + conn.getContent().toString());
        }
    }
    
    
     public void executeUploadImage(String targetURL, String imageLocation, String method, String email, String password) throws IOException, ParseException {
        HttpsTrustManager.allowAllSSL();

        String user = email;
        String pass = password;

        URL url = new URL(targetURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        InputStream is = null;
        String response = "";

        try {
            String body = imageLocation;
            conn.setReadTimeout(40000);
            conn.setConnectTimeout(40000);
            conn.setRequestMethod(method);
            conn.setDoInput(true);
            conn.setDoOutput(true);

//            System.out.println("----");
            String credentials = user + ":" + pass;
//            System.out.println(credentials);

            Base64.Encoder encoder = Base64.getEncoder();
            String base64EncodedCredentials = encoder.encodeToString(credentials.getBytes()); //base64 encoding for accessing URLs with authentication
            conn.setRequestProperty("Authorization", "Basic " + base64EncodedCredentials);

            String urlParameters = body;
            byte[] postData = urlParameters.getBytes(Charset.forName("UTF-8"));
            int postDataLength = postData.length;
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setUseCaches(false);
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write(postData);
            }

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            }
          
            System.out.println("Uploading Image: " +response + " " + responseCode);

        } catch (NumberFormatException | IOException e) {
            System.out.println("FAILED" + e.toString() + " " + response + conn.getContent().toString());
        }
    }
    
     
   private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }
        inputStream.close();
        return result;
    }
    
    public static String HTMLDatetoCerebroDate(String htmlDate) throws ParseException {
        DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd");
        fromFormat.setLenient(false);
        DateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
        toFormat.setLenient(false);
        Date date = fromFormat.parse(htmlDate);
        String cerebroDate = toFormat.format(date);
        return cerebroDate;
    }

     public static String HTMLCompanyAniversaryDatetoCerebroDate(String htmlDate) throws ParseException {
        DateFormat fromFormat = new SimpleDateFormat("MM-yyyy");
        fromFormat.setLenient(false);
        DateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
        toFormat.setLenient(false);
        Date date = fromFormat.parse(htmlDate);
        String cerebroDate = toFormat.format(date);
        return cerebroDate;
    }


  



  
}
